#ifndef myadd_h
#define myadd_h

#include <wx/wx.h>
#include <wx/spinctrl.h>
#include <wx/datectrl.h>

#include "Exel.h"
#include "id.h"
#include "Myuuid.h"
#include "StaffOkAdd.h"
#include "DepartmentOkAdd.h"
#include "EquipOkAdd.h"
#include "CustomerOkAdd.h"
#include "ContractOkAdd.h"
#include "SubcontrOkAdd.h"
#include "WGroupOkAdd.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

struct AddInfo
{
    int number_col;
    wxString get_sql_file;
};

class MyAdd : public wxDialog
{
public:
    MyAdd(wxString get_add_main, Exel* add_table_main, wxString name_table_main, BD* bd_main, bool* is_ok);
    MyAdd(Exel* add_table_main, BD* bd_main, bool* is_ok, wxAuiNotebook *nb);

    bool FillDiaflog();
    void GetFillingVector();
    bool GetSelectFromSQL(wxArrayString* result, wxString name_sql_file);
    void AddNameCols();
    void FillingLine();
    void FirstFillingLineContract();
    void FillingLineContract();
    void FillingLineSubcontr();
    void GetSizeMyAdd();
    void GetSizeMyAddContract();

    void AddNewLine(wxCommandEvent &event);
    void AddNewLineContract(wxCommandEvent &event);
    void DeleteNewLine(wxCommandEvent &event);
    void DeleteNewLineContract(wxCommandEvent &event);
    void DeleteNewLineSubcontr();
    void CheckProject(wxCommandEvent &event);

    void OnOk(wxCommandEvent &event);
    void SetFillingVector();
    bool ChooseAdd();

    wxString get_add;
    wxString set_add;
    Exel* add_table;
    Exel* contract_table;
    wxString name_table;
    BD* bd;

    wxButton* button_ok;
    wxButton* button_plus;
    wxButton* button_minus;
    wxButton* button_cancel;

    wxBoxSizer* vsizer;
    wxBoxSizer* sizer_name_cols;
    wxBoxSizer* sizer_buttons; 
    wxScrolledWindow* scrolled;
    wxGridSizer* grid_sizer;

    wxArrayString read_result;
    wxArrayInt number_rows;
    std::vector<AddInfo> vector_add;
    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxDatePickerCtrl*> vector_date;
    std::vector<wxSpinCtrl*> vector_spin;

    std::vector<wxComboBox*> vector_combobox_contract;
    std::vector<wxComboBox*> vector_combobox_subcontr;

    std::vector<wxArrayString> combobox_result;
    std::vector<wxString> vector_set;
};

#endif