#include "EquipOkAdd.h"

EquipOkAdd::EquipOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    old_vector_combobox = vector_combobox_main;

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }

    result.Add(wxT("Организация"));

    ChangeNameOnUUID();

    int number_uuid = old_vector_combobox.size() / 2;
    GetAndCheckUuid(table, 0, vector_uuid, number_uuid);

    int size_vector_org = vector_org.size() / 2;
    int size_new_vector = new_vector_combobox.size() / 2;

    std::vector<wxString> vector_uuid_org;
    std::vector<wxString> vector_uuid_new;

    int index_vector_uuid = 0;

    while(index_vector_uuid < size_vector_org)
    {
        vector_uuid_org.push_back(vector_uuid[index_vector_uuid]);
        index_vector_uuid++;
    }

    for(int i = 0; i < size_new_vector; i++)
    {
        vector_uuid_new.push_back(vector_uuid[index_vector_uuid]);
        index_vector_uuid++;
    }

    int count_rows = table->GetNumberRows() + 1;

    for(int i = 0; i < vector_org.size(); i+=2)
    {
        wxString number_equip = wxT("[");
        number_equip << count_rows;
        number_equip += wxT("]");
        number_equip += vector_org[i]->GetValue();
        vector_org[i]->SetValue(number_equip);
        count_rows++;
    }

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    PGresult* res1 = bd->request("BEGIN;");
    if(PQresultStatus(res1) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        ErrorSQL(str);
        res1 = bd->request("ROLLBACK;");
        PQclear(res1);
        return;
    }

    if(size_vector_org > 0)
    {
        if(!InsertSQL(vector_uuid_org, insert_uuid, vector_org, 3, set_sql[1], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
    }

    if(size_new_vector > 0)
    {
        if(!InsertSQL(vector_uuid_new, insert_uuid, new_vector_combobox, 3, set_sql[2], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
    }

    PGresult* res2 = bd->request("COMMIT;");
    if(PQresultStatus(res2) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res2), wxConvUTF8);
        ErrorSQL(str);
        res2 = bd->request("ROLLBACK;");
        PQclear(res2);
    }

    PQclear(res1);
    PQclear(res2);
}

void EquipOkAdd::ChangeNameOnUUID()
{
    int count_in_one_block = result.GetCount() / 2;
    int count_vector_combobox = old_vector_combobox.size();
    for(int i = 1; i < count_vector_combobox; i+=2)
    {
        wxString name_dep = old_vector_combobox[i]->GetValue();
        if(name_dep.Cmp(wxT("Организация")) == 0)
        {
            old_vector_combobox[i]->SetValue(wxT("t"));
            vector_org.push_back(old_vector_combobox[i-1]);
            vector_org.push_back(old_vector_combobox[i]);
        }
        for(int index_name = 0; index_name < count_in_one_block; index_name++)
        {
            if(name_dep.Cmp(result[index_name]) == 0)
            {
                int index_uuid = index_name + count_in_one_block;
                old_vector_combobox[i]->SetValue(result[index_uuid]);
                new_vector_combobox.push_back(old_vector_combobox[i-1]);
                new_vector_combobox.push_back(old_vector_combobox[i]);
                break;
            }
        }
    }
}