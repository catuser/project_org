#include <wx/wx.h>
#include <wx/aui/auibook.h>
#include <wx/textfile.h>

#include "id.h"
#include "Exel.h"
#include "Search.h"
#include "Filter.h"
#include "Change.h"
#include "MyAdd.h"

class Personnal : public wxPanel
{
public:
    Personnal(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main);
    void PersonnalSetSizer();
    void ErrorConnectBD();
    bool ErrorRequest(PGresult* res, bool isselect);
    bool ResultRequest(wxString name_file, wxString id_column, PGresult* res, PGresult* res_eq);

    void OnStaff(bool isupdate);
    void OnDepartments(bool isupdate);
    void OnWGroup(bool isupdate);

    void OnUpdate(int number_exel, wxString file_name);
    void OnUpdateTwo(int number_exel, wxString sql1, wxString sql2);
    void OnFilter(int number_exel);
    void OnSearch(int number_exel);
    void OnAdd(int number_exel);
    void OnChange(int number_exel);
    void OnDelete(int number_exel);
    int OnDeleteSQL(wxString name_file, wxString cell_id, bool isselect);
    void OnDeleteStaff();
    bool OnDeleteStaffMainPart(wxString cell_id);
    void OnDeleteDepartments();
    bool OnDeleteDepartmentsMainPart(wxString cell_id_dep, wxString cell_id_bos, wxString name_dep);
    void OnDeleteWGroup();
    bool OnDeleteWGroupMainPart(wxString id_contract_project, wxString iscontract, wxString id_wgroup);

    bool DeleteEquipWGroup(wxString get_sql_file, wxString first_string, wxString second_string, BD* bd);

    void SetBool(bool is_read_only);

    wxScrolledWindow* table_buttons;
    wxBoxSizer* table_buttons_sizer;
    BD* bd;
    wxString error_bd;
    wxAuiNotebook* nb;

    std::vector<TableColumn> table_vector_staff;
    std::vector<TableColumn> table_vector_departments;
    std::vector<TableColumn> table_vector_work_group;

    std::vector<NumberColumn> filter_staff;
    std::vector<NumberColumn> filter_departments;
    std::vector<NumberColumn> filter_work_group;

    wxButton* staff;
    wxButton* departments;
    wxButton* work_group;

    Exel* onstaff;
    Exel* ondepartments;
    Exel* onwork_group;

    bool isonstaff;
    bool isondepartments;
    bool isonwork_group;

    bool is_read_only_personal;
};