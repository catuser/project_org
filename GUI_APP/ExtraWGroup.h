#include <wx/wx.h>
#include <wx/datectrl.h>

#include <iostream>
#include <vector>

#include "BD.h"
#include "id.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class ExtraWGroup : public wxDialog
{
public:
    ExtraWGroup(wxString id_wgroup_main, wxString id_contract_project_main, wxString name_wgroup_main, 
            wxString start_work_wgroup_main, bool off_ok_done, BD* bd_main);

    void FillFileWithSQL(wxString name_file_with_sql);
    bool FillBoss();
    bool FillStaff();
    bool FillEquip();
    bool IsContract();
    bool FillProjects();

    void OnBoss(wxCommandEvent &event);
    void OnStaff(wxCommandEvent &event);
    void OnEquip(wxCommandEvent &event);
    void OnProject(wxCommandEvent &event);

    void AddLine(wxCommandEvent &event);
    void DeleteLine(wxCommandEvent &event);

    void AddLineStaff();
    void AddLineEquip();

    void DeleteLineStaff();
    void DeleteLineEquip();

    void OnOk(wxCommandEvent &event);
    void OnDone(wxCommandEvent &event);

    bool CheckStartProject();
    bool CheckEndProject();

    bool DeleteEquip(wxString get_sql_file, wxString first_string, wxString second_string, BD* bd);

    wxString id_wgroup;
    wxString id_contract_project;
    wxString name_wgroup;
    wxString start_work_wgroup;
    BD* bd;

    bool is_ok;
    bool bool_is_contract;
    wxArrayString sql_files;

    wxArrayString all_names_boss;
    wxArrayString id_staff_wgroup;
    wxArrayString name_staff;
    wxArrayString info_use_equip;
    wxArrayString all_names_staff;
    wxArrayString all_names_equip;
    wxArrayString equip_bool;
    wxArrayString info_use_projects;

    int start_count_staff;
    int start_count_equip;

    std::vector<int> vector_index_ok;

    wxScrolledWindow* scrolled_boss;
    wxScrolledWindow* scrolled_staff;
    wxScrolledWindow* scrolled_equip;
    wxScrolledWindow* scrolled_projects;

    wxBoxSizer* sizer_main;
    wxBoxSizer* sizer_first_buttons;
    wxBoxSizer* sizer_second_buttons;
    wxBoxSizer* sizer_third_buttons;

    wxGridSizer* grid_sizer_boss;
    wxGridSizer* grid_sizer_staff;
    wxGridSizer* grid_sizer_equip;
    wxGridSizer* grid_sizer_projects;

    wxButton* button_boss;
    wxButton* button_staff;
    wxButton* button_equip;
    wxButton* button_projects;

    wxButton* button_plus;
    wxButton* button_minus;

    wxButton* button_ok;
    wxButton* button_done;
    wxButton* button_cancel;

    wxComboBox* box_boss;
    std::vector<wxComboBox*> vector_staff;
    std::vector<wxComboBox*> vector_equip;
    std::vector<wxTextCtrl*> vector_projects;

    std::vector<wxComboBox*> vector_equip_bool;

    std::vector<wxTextCtrl*> vector_start_equip;
    std::vector<wxDatePickerCtrl*> vector_start_project;

    std::vector<wxTextCtrl*> vector_end_equip;
    std::vector<wxDatePickerCtrl*> vector_end_project;
};