#include <wx/wx.h>
#include <wx/aui/auibook.h>
#include <wx/textfile.h>

#include "id.h"
#include "Exel.h"
#include "Search.h"
#include "Filter.h"
#include "Change.h"
#include "MyAdd.h"

class Equipment : public wxPanel
{
public:
    Equipment(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main);
    void EquipmentSetSizer();
    void ErrorConnectBD();
    bool ErrorRequest(PGresult* res, bool isselect);

    void OnEquipmentTable(bool isupdate);
    void OnEquipmentUsed(bool isupdate);
    void OnEquipmentEfficiency(bool isupdate);

    void OnUpdate(int number_exel, wxString file_name);
    void OnFilter(int number_exel);
    void OnSearch(int number_exel);
    void OnAdd(int number_exel);
    void OnChange(int number_exel);
    void OnDelete(int number_exel);
    int OnDeleteSQL(wxString name_file, wxString cell_id, bool isselect);
    void OnDeleteEqTable();
    bool OnDeleteEqTableMainPart(wxString id_eq);

    void SetBool(bool is_read_only);

    wxScrolledWindow* table_buttons;
    wxBoxSizer* table_buttons_sizer;
    BD* bd;
    wxString error_bd;
    wxAuiNotebook* nb;

    std::vector<TableColumn> table_vector_equipment_table;
    std::vector<TableColumn> table_vector_equipment_used;
    std::vector<TableColumn> table_vector_equipment_efficiency;

    std::vector<NumberColumn> filter_equipment_table;
    std::vector<NumberColumn> filter_equipment_used;
    std::vector<NumberColumn> filter_equipment_efficiency;

    wxButton* equipment_table;
    wxButton* equipment_used;
    wxButton* equipment_efficiency;

    Exel* onequipment_table;
    Exel* onequipment_used;
    Exel* onequipment_efficiency;

    bool isonequipment_table;
    bool isonequipment_used;
    bool isonequipment_efficiency;

    bool is_read_only_equip;
};