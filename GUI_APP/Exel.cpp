#include "Exel.h"

Exel::Exel(wxAuiNotebook *nb) : wxGrid(nb, wxID_ANY, wxDefaultPosition)
{
    //Нельзя выделять блоки мышкой
    EnableEditing(false);


    Connect(wxEVT_GRID_COL_SORT, wxGridEventHandler(Exel::OnSort));
    Connect(wxEVT_GRID_CELL_LEFT_DCLICK, wxGridEventHandler(Exel::ExtraMenu));
}


//Создание и Заполнение Таблицы 
void Exel::GetTable(BD* bd_main, const char* sql, bool isupdate)
{
    bd = bd_main;
    //Отправка запроса
    PGresult* res = bd->request(sql);
    //Проверка на ошибку
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        //Если есть ошибка (от БД), То он ее выведет на экран отдельным сообщением
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res);
        return;
    }
    else
    {
        //Если нет ошибки, то тогда создаю таблицу  и заполняю ее
        int amount_rows = PQntuples(res);
        int amount_cols = PQnfields(res);

        //Проверяю, не обновление ли это таблицы
        if(!isupdate)
        {
            //Если нет, то тогда создаю таблицу
            CreateGrid(amount_rows, amount_cols);
            //Заполняю столбцы
            for (int i = 0; i < amount_cols; i++)
            {
                wxString name_col = wxString(PQfname(res, i), wxConvUTF8);
                SetColLabelValue(i, name_col);
            }
        }
        else
        {
            //Если обновление, то тогда удаляю старые строки и
            //вставляю новые, которые буду заполнять
            int now_amount_rows = GetNumberRows();
            DeleteRows(0, now_amount_rows, false);
            InsertRows(0, amount_rows, false);
        }
        

        //Заполнение строк
        for (int i = 0; i < amount_rows; i++)
        {
            for (int j = 0; j < amount_cols; j++)
            {
                wxString name_cell = wxString(PQgetvalue(res, i, j), wxConvUTF8);
                SetCellValue(i, j, name_cell);
            }
        }

        //Размещение названия колонок  выбор шрифта
        SetRowLabelAlignment(wxALIGN_RIGHT, wxALIGN_CENTRE);
        SetLabelFont(wxFont(9, wxFONTFAMILY_DEFAULT, 
            wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

        //Автонастройка размера ячеек
        AutoSizeColumns();
        AutoSizeRows();

        PQclear(res);
    }
}

//Аналогично как и для заполнения обычной таблицы, только тут отправляются два запроса
//и на полученных данных собирается таблица
void Exel::GetTableDepartments(BD* bd, const char* sql, bool isupdate)
{
    std::string sql_str = sql;
    auto pos = sql_str.find("\n");
    std::string s1;
    std::string s2;
    if (pos != std::string::npos)
    {
        s1 = sql_str.substr(0,pos);
        s2 = sql_str.substr(pos+1);
    }
    const char* sql1 = s1.c_str();
    const char* sql2 = s2.c_str();
    PGresult* res1 = bd->request(sql1);
    PGresult* res2 = bd->request(sql2);
    if(PQresultStatus(res1) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res1);
        PQclear(res2);
        return;
    }
    if(PQresultStatus(res2) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res2), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res1);
        PQclear(res2);
        return;
    }

    int amount_rows1 = PQntuples(res1);
    int amount_rows2 = PQntuples(res2);

    if(!isupdate)
    {
        CreateGrid(amount_rows1, 7);

        for (int i = 0; i < 3; i++)
        {
            wxString name_col = wxString(PQfname(res2, i), wxConvUTF8);
            SetColLabelValue(i, name_col);
        }
        SetColLabelValue(3, wxT("Начальник"));
        SetColLabelValue(4, wxT("ФИО"));
        SetColLabelValue(5, wxT("Телефон"));
        SetColLabelValue(6, wxT("Профессия"));
    }
    else
    {
        int now_amount_rows = GetNumberRows();
        DeleteRows(0, now_amount_rows, false);
        if(amount_rows1 > amount_rows2)
            InsertRows(0, amount_rows1, false);
        else
            InsertRows(0, amount_rows2, false);
    }

    int number_row = 0;
    int number_row_depart = 0;
    wxString name_boss;
    for (int i = 0; i < amount_rows2; i++)
    {
        const char* id_department = PQgetvalue(res2, i, 0);
        const char* name_department = PQgetvalue(res2, i, 1);
        const char* id_boss = PQgetvalue(res2, i, 2);
        while(strcmp(name_department, PQgetvalue(res1, number_row, 0)) == 0)
        {
            wxString name_cell = wxString(id_department, wxConvUTF8);
            SetCellValue(number_row, 0, name_cell);
            name_cell = wxString(name_department, wxConvUTF8);
            SetCellValue(number_row, 1, name_cell);
            name_cell = wxString(id_boss, wxConvUTF8);
            SetCellValue(number_row, 2, name_cell);
            for (int j = 0; j < 3; j++)
            {
                name_cell = wxString(PQgetvalue(res1, number_row, j+2), wxConvUTF8);
                SetCellValue(number_row, j + 4, name_cell);
            }
            if(number_row == amount_rows1 - 1)
            {
                break;
            }
            number_row++;
        }

        for(int j = number_row_depart; j <= number_row; j++)
        {
            if(strcmp(PQgetvalue(res1, j, 1), id_boss) == 0)
            {
                name_boss = wxString(PQgetvalue(res1, j, 2), wxConvUTF8);
                for (int x = number_row_depart; x <= number_row; x++)
                {
                    number_row_depart = number_row;
                    SetCellValue(x, 3, name_boss);
                }
                break;
            }
        }
    }

    SetRowLabelAlignment(wxALIGN_RIGHT, wxALIGN_CENTRE);
    SetLabelFont(wxFont(9, wxFONTFAMILY_DEFAULT, 
        wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

    AutoSizeColumns();
    AutoSizeRows();

    PQclear(res1);
    PQclear(res2);
}

//Аналогично как и для заполнения обычной таблицы, только тут отправляются два запроса
//и на полученных данных собирается таблица
//Таблица эффективности договора и проекта
void Exel::GetTableCPEff(BD* bd, const char* sql, bool isupdate)
{
    
    std::string sql_str = sql;
    auto pos = sql_str.find("\n");
    std::string s1;
    std::string s2;
    if (pos != std::string::npos)
    {
        s1 = sql_str.substr(0,pos);
        s2 = sql_str.substr(pos+1);
    }
    const char* sql1 = s1.c_str();
    const char* sql2 = s2.c_str();
    PGresult* res1 = bd->request(sql1);
    PGresult* res2 = bd->request(sql2);
    if(PQresultStatus(res1) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res1);
        PQclear(res2);
        return;
    }
    if(PQresultStatus(res2) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res2), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res1);
        PQclear(res2);
        return;
    }
    int amount_rows;
    int amount_rows1 = PQntuples(res1);
    int amount_rows2 = PQntuples(res2);

    int amount_cols1 = PQnfields(res1);
    int amount_cols2 = PQnfields(res2);
    int amount_cols = amount_cols1 + amount_cols2;

    if(!isupdate)
    {
        if(amount_rows1 > amount_rows2)
            amount_rows = PQntuples(res1);
        else
            amount_rows = PQntuples(res2);
        CreateGrid(amount_rows, amount_cols);
        wxString name_col;
        for (int i = 0; i < amount_cols1; i++)
        {
            name_col = wxString(PQfname(res1, i), wxConvUTF8);
            SetColLabelValue(i, name_col);
        }
        for (int i = 0; i < amount_cols2; i++)
        {
            name_col = wxString(PQfname(res2, i), wxConvUTF8);
            SetColLabelValue(i + amount_cols1, name_col);
        }
    }
    else
    {
        int now_amount_rows = GetNumberRows();
        DeleteRows(0, now_amount_rows, false);
        if(amount_rows1 > amount_rows2)
            amount_rows = PQntuples(res1);
        else
            amount_rows = PQntuples(res2);
        InsertRows(0, amount_rows, false);
    }

    for (int i = 0; i < amount_rows1; i++)
    {
        for (int j = 0; j < amount_cols1; j++)
        {
            wxString name_cell = wxString(PQgetvalue(res1, i, j), wxConvUTF8);
            SetCellValue(i, j, name_cell);
        }
    }
    for (int i = 0; i < amount_rows2; i++)
    {
        for (int j = 0; j < amount_cols2; j++)
        {
            wxString name_cell = wxString(PQgetvalue(res2, i, j), wxConvUTF8);
            SetCellValue(i, j + amount_cols1, name_cell);
        }
    }

    SetRowLabelAlignment(wxALIGN_RIGHT, wxALIGN_CENTRE);
    SetLabelFont(wxFont(9, wxFONTFAMILY_DEFAULT, 
        wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

    AutoSizeColumns();
    AutoSizeRows();
    PQclear(res1);
    PQclear(res2);
}

//Функция сортировки
//Срабатывает, когда нажали на столбец
void Exel::OnSort(wxGridEvent &event)
{
    std::vector<std::pair<wxString, int>> sort_grid;
    //получаю колличество строчек
    int number_rows = GetNumberRows();
    //получаю колличество столбцов
    int number_col = GetNumberCols();
    int col = event.GetCol();
    wxString str;
    //В сортировке не участвуют те строчки, которые скрыты
    for(int row = 0; row < number_rows; row++)
    {
        if(IsRowShown(row))
        {
            str = GetCellValue(row, col);
            sort_grid.push_back(std::pair<wxString, int>(str, row));
        }
    }

    std::sort(sort_grid.begin(), sort_grid.end());
    std::vector<int> sort_pos;
    for(int i = 0; i < sort_grid.size(); i++)
    {
        sort_pos.push_back(sort_grid[i].second);
    }
    std::sort(sort_pos.begin(), sort_pos.end());
    if (selected_col == col)
    {
        selected_col = -1;
        int xl = 0;
        wxString displaced_grid[sort_grid.size()][number_col];
        for(int xr = sort_grid.size() - 1; xr >= 0; xr--)
        {
            for(int y = 0; y < number_col; y++)
            {
                displaced_grid[xl][y] = GetCellValue(sort_grid[xr].second, y);
            }
            xl++;
        }

        for(int x = 0; x < sort_grid.size(); x++)
        {
            for(int y = 0; y < number_col; y++)
            {
                SetCellValue(sort_pos[x], y, displaced_grid[x][y]);
            }
        }
    }
    else
    {
        selected_col = col;

        wxString displaced_grid[sort_grid.size()][number_col];
        for(int x = 0; x < sort_grid.size(); x++)
        {
            for(int y = 0; y < number_col; y++)
            {
                displaced_grid[x][y] = GetCellValue(sort_grid[x].second, y);
            }
        }

        for(int x = 0; x < sort_grid.size(); x++)
        {
            for(int y = 0; y < number_col; y++)
            {
                SetCellValue(sort_pos[x], y, displaced_grid[x][y]);
            }
        }
    }
    event.Skip();
}   

//Двойной клик по любой ячейки
//В зависимости от таблицы (Сотрудники или Раб Групп) открывает новое окно
//В других таблицах не будет работать
void Exel::ExtraMenu(wxGridEvent &event)
{
    int number_row = event.GetRow();
    wxString name_first_col = this->GetColLabelValue(0);
    if(name_first_col.Cmp(wxT("Number")) == 0)
    {
        wxString id_wgroup = this->GetCellValue(number_row, 1);
        wxString id_contract_project = this->GetCellValue(number_row, 2);
        wxString name_wgroup = this->GetCellValue(number_row, 3);
        wxString start_work_wgroup = this->GetCellValue(number_row, 4);
        wxString end_work_wgroup = this->GetCellValue(number_row, 5);
        bool off_ok_done;
        if(end_work_wgroup.IsNull())
        {
            off_ok_done = false;
        }
        else
        {
            off_ok_done = true;
        }
        
        ExtraWGroup* extra_wgroup = new ExtraWGroup(id_wgroup, id_contract_project, name_wgroup, start_work_wgroup, off_ok_done, bd);
        if(extra_wgroup->is_ok)
        {
            extra_wgroup->ShowModal();
        }
        extra_wgroup->Destroy();

        wxTextFile sql_file(wxT("sql/work_group/work_group.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql_string = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql_string += wxT("\n");
            sql_string += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* sql = sql_string.mb_str(wxConvUTF8);

        OnUpdateTable(bd, sql);
        return;
    }

    if(name_first_col.Cmp(wxT("idСотрудник")) == 0)
    {
        wxString id_staff = this->GetCellValue(number_row, 0);
        wxString name_staff = this->GetCellValue(number_row, 1);
        wxString profession = this->GetCellValue(number_row, 5);
        ExtraStaff* extra_staff = new ExtraStaff(id_staff, name_staff, profession, bd);
        if(extra_staff->is_ok)
        {
            extra_staff->ShowModal();
        }
        extra_staff->Destroy();
        return;
    }
    
}

//Обновление таблицы
void Exel::OnUpdateTable(BD* bd, const char* sql)
{
    //Очищение таблицы
    ClearGrid();
    //Вызов функции создания таблицы с флагом, что это обновление
    GetTable(bd, sql, true);
}

//Обновление таблицы Отделы
void Exel::OnUpdateTableDepartments(BD* bd, const char* sql)
{
    ClearGrid();
    GetTableDepartments(bd, sql, true);
}

//Обновление таблицы Эффективность Договоров и проектов
void Exel::OnUpdateTableCPEff(BD* bd, const char* sql)
{
    ClearGrid();
    GetTableCPEff(bd, sql, true);
}