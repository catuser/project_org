#ifndef workwithinput_h
#define workwithinput_h

#include <wx/wx.h>
#include <wx/textfile.h>
#include <iostream>
#include <vector>

#include "BD.h"
#include "WorkWithSQL.h"

bool CheckInput(int number_combobox, int number_combobox_result, 
        std::vector<wxArrayString> combobox_result, std::vector<wxComboBox*> vector_combobox);

bool CheckRepeatProject(int count_lines, int value_index, int plus_index, 
        std::vector<wxComboBox*> vector_combobox, wxScrolledWindow* scrolled);

bool ReplaceNameId(int count_cols, std::vector<wxComboBox*> vector_combobox, int number_col, wxString set_sql, BD* bd);

#endif