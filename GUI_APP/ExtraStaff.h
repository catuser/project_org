#include <wx/wx.h>
#include <wx/spinctrl.h>

#include <iostream>
#include <vector>

#include "BD.h"
#include "id.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class ExtraStaff : public wxDialog
{
public:
    ExtraStaff(wxString id_staff_main, wxString name_staff_main, wxString profession_main, BD* bd_main);

    bool YesTech();
    bool NotTech();
    int WhatProfession();
    void FalseSQL();
    bool TrueSQL(wxString sql, int number_profession);
    void GetSizeMyExtra();

    void AddNewLine(wxCommandEvent &event);
    void DeleteNewLine(wxCommandEvent &event);
    void OnOk(wxCommandEvent &event);

    bool NotTechOk(wxArrayString set_sql_files);
    bool TechOk(wxArrayString set_sql_files);

    wxString id_staff;
    wxString name_staff;
    wxString profession;
    BD* bd;

    bool is_ok;
    int count_id_result;
    int number_dont_service;
    wxArrayString id_result;
    wxArrayString name_result;

    wxScrolledWindow* scrolled;

    wxButton* button_ok;
    wxButton* button_plus;
    wxButton* button_minus;
    wxButton* button_cancel;

    wxBoxSizer* sizer;
    wxBoxSizer* sizer_buttons;

    wxGridSizer* grid_sizer;

    wxTextCtrl* text_name_staff;
    wxTextCtrl* enter_info;
    wxSpinCtrl* count_evidence;

    std::vector<wxComboBox*> vector_name_equip;
};