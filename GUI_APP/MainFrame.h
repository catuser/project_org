#include <wx/wx.h>
#include <wx/frame.h>
#include <wx/utils.h>
#include <wx/tglbtn.h>
#include <wx/splitter.h>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>


#include "HelpDialog.h"
#include "Exel.h"
#include "personnal.h"
#include "Equipment.h"
#include "Contract.h"

#include <utility>

class MainFrame : public wxFrame
{
public:
    MainFrame(const wxString &title);
    void ErrorConnectBD();
    BD* bd;
    bool isConnect_bd;
    wxAuiNotebook* nb;
    void OnClosePage(wxAuiNotebookEvent &event);
    void OnQuit(wxCommandEvent &event);

    void OnHelp(wxCommandEvent &event);
    void OnAboutProgram(wxCommandEvent &event);

    void OnMenuSplit(wxCommandEvent& evt);
    void OnUnsplit(wxSplitterEvent& evt);

    void OnReadOnly(wxCommandEvent &event);

    void bdbuttons();
    void free_table_buttons_sizer();
    void OnPersonnel(wxCommandEvent &event);
    void OnEquipment(wxCommandEvent &event);
    void OnContract(wxCommandEvent &event);

    Personnal* personnal;
    void OnStaffM(wxCommandEvent &event);
    void OnDepartmentsM(wxCommandEvent &event);
    void OnWGroupM(wxCommandEvent &event);

    Equipment* equipment;
    void OnEquipmentTableM(wxCommandEvent &event);
    void OnEquipmentUsedM(wxCommandEvent &event);
    void OnEquipmentEfficiencyM(wxCommandEvent &event);

    Contract* contract;
    void OnCustomerM(wxCommandEvent &event);
    void OnContractTableM(wxCommandEvent &event);
    void OnProjectM(wxCommandEvent &event);
    void OnContractProjectM(wxCommandEvent &event);
    void OnSubcontrOrgM(wxCommandEvent &event);
    void OnCPEfficiencyM(wxCommandEvent &event);

    void OnUpdatePage(wxCommandEvent &event);
    void OnFilterPage(wxCommandEvent &event);
    void OnSearchPage(wxCommandEvent &event);
    void OnAddPage(wxCommandEvent &event);
    void OnChangePage(wxCommandEvent &event);
    void OnDeletePage(wxCommandEvent &event);

    wxScrolledWindow* table_buttons;
    wxScrolledWindow* option_buttons;
    wxBoxSizer* table_buttons_sizer;
    wxBoxSizer* option_buttons_sizer;
    wxButton* button_delete;
    wxButton* button_change;
    wxButton* button_add;

    bool isPersonal;
    bool isEquipment;
    bool isContract;
    bool isOptionButtons;

    wxSplitterWindow* spl;
    wxPanel* panel_buttons;
    wxPanel* panel2;

    wxMenuBar *menubar;
	wxMenu *file;
	wxMenu *help;
    wxMenu* split;
	wxMenuItem *quit;
    wxMenuItem *help_doc;
    wxMenuItem *about_program;

    wxString error_bd;

private:
    wxDECLARE_EVENT_TABLE();
};
