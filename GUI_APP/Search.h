#ifndef search_h
#define search_h

#include <wx/wx.h>

#include "Exel.h"
#include "id.h"
#include "WorkWithSQL.h"

class Search : public wxFrame
{
public:
    Search(wxPanel* panel_main, std::vector<TableColumn> table_colums_main, Exel* myexel);
    void OnOk(wxCommandEvent &event);
    void NextSearch(wxCommandEvent &event);
    void PreviousSearch(wxCommandEvent &event);
    void OnQuit(wxCommandEvent &event);

    void SelectBlockOfTable(bool is_select);

    Exel* search_inexel;
    std::vector<TableColumn> table_colums;
    wxBoxSizer* sizer;
    wxBoxSizer* sizer_buttons;
    wxButton* button_ok;
    wxButton* button_cancel;

    wxButton* button_next;
    wxButton* button_previous;

    wxComboBox* box_colums1;
    wxTextCtrl* enter_search;
    wxArrayString name_colums_arr;
    int number_pair;
    std::vector<std::pair<int, int> > pairs_coords;
    std::vector<std::pair<int, int> > all_pairs_coords;
};

#endif