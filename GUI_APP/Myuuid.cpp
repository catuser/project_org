#include "Myuuid.h"

std::string generateUUID()
{

    const std::string CHARS = "0123456789abcdefghijklmnopqrstuvwxyz";
    std::string uuid = std::string(36,' ');
    int rnd = 0;
    int r = 0;
  
    uuid[8] = '-';
    uuid[13] = '-';
    uuid[18] = '-';
    uuid[23] = '-';

    uuid[14] = '4';

    for(int i=0;i<36;i++){
      if (i != 8 && i != 13 && i != 18 && i != 14 && i != 23) {
        if (rnd <= 0x02) {
            rnd = 0x2000000 + (std::rand() * 0x1000000) | 0;
        }
        rnd >>= 4;
        uuid[i] = CHARS[(i == 19) ? ((rnd & 0xf) & 0x3) | 0x8 : rnd & 0xf];
      }
    }
    return uuid;
}

void GetAndCheckUuid(Exel* check_table, int number_col, std::vector<wxString> &vector_id, int number_uuid)
{
    int number_rows = check_table->GetNumberRows();
    
    while(number_uuid > 0)
    {
        bool is_ok = true;
        wxString new_uuid(generateUUID());
        for(int i = 0; i < number_rows; i++)
        {
            wxString compare_uuid = check_table->GetCellValue(i, number_col);
            if(compare_uuid.Cmp(new_uuid) == 0)
            {
                is_ok = false;
                break;
            }
        }
        if(is_ok)
        {
            vector_id.push_back(new_uuid);
            number_uuid--;
        }
    }
}