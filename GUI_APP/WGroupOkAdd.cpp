#include "WGroupOkAdd.h"

WGroupOkAdd::WGroupOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled_main)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;
    scrolled = scrolled_main;
    is_ok = true;

    wxArrayString temporary_storage_name;
    for(int i = 1; i < vector_combobox.size(); i+=2)
    {
        wxString string_temporary_storage = vector_combobox[i]->GetValue();
        temporary_storage_name.Add(string_temporary_storage);
    }

    int count_lines = vector_combobox.size() / 2;
    if(!CheckRepeatProject(count_lines, 0, 2, vector_combobox, scrolled))
    {
        ErrorSQL(wxT("Разныые рабочии группы не могут выполнять одиннаковую работу"));
        is_ok = false;
        return;
    }

    int count_cols = 2;
    int number_col = 1;
    if(!ReplaceNameId(count_cols, vector_combobox, number_col, set_sql[0], bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }

    GetAndCheckUuid(table, 0, vector_uuid_wgroup, count_lines);

    GetAndCheckUuid(table, 1, vector_uuid_conpr, count_lines);


    vector_all_uuid = vector_uuid_wgroup;

    for(int i = 0; i < vector_uuid_conpr.size(); i++)
    {
        vector_all_uuid.push_back(vector_uuid_conpr[i]);
    }

    std::vector<wxComboBox*> vector_wgroup;

    wxDateTime now = wxDateTime::Now();
    wxString start_work = now.Format(wxT("%Y")) + wxT("-") + now.Format(wxT("%m")) + wxT("-") + 
                    now.Format(wxT("%d"));

    CheckAndPlusWork(vector_wgroup, start_work);

    int count_vector_contract = vector_contract.size();
    std::vector<bool> vector_is_project_null;
    wxArrayString string_projects;
    if(count_vector_contract > 0)
    {
        for(int i = 0; i < count_vector_contract; i+=2)
        {
            wxString name_contract = vector_contract[i];
            if(!GetWhereSQL(set_sql[1], &result, name_contract, bd, true))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                is_ok = true;
                return;
            }
            select_result.push_back(result);
            result.Clear();

            if(!GetWhereSQL(set_sql[9], &string_projects, name_contract, bd, true))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                is_ok = true;
                return;
            }

            int count_vector_is_project_null = vector_is_project_null.size();
            vector_is_project_null.push_back(true);
            int count_select_result = select_result.size() - 1;
            wxArrayString start_work_project = select_result[count_select_result];
            int count_result = start_work_project.GetCount();
            for(int j = 0; j < count_result; j++)
            {
                if(!start_work_project[j].IsNull())
                {
                    vector_is_project_null[count_vector_is_project_null] = false;
                    break;
                }
            }
        }
    }
    select_result.clear();

    //Получаю названия договоров, которым нужно будет поставить, что их все проекты выполнены
    int count_vector_projects = vector_project.size();
    wxArrayString all_contract;
    std::vector<std::pair<wxString, wxString> > vector_project_contract;
    if(count_vector_projects > 0)
    {
        //получаю пары контракт - проект. То есть у каждого проекта нахожу его контракт
        for(int i = 0; i < count_vector_projects; i+=3)
        {
            wxString name_project = vector_project[i];
            std::cout << "+++++++" << std::endl;
            std::cout << set_sql[10].mb_str(wxConvUTF8) << std::endl;
            std::cout << "+++++++" << std::endl;
            if(!GetWhereSQL(set_sql[10], &all_contract, name_project, bd, true))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                is_ok = true;
                return;
            }

            for(int j = 0; j < all_contract.GetCount(); j ++)
            {
                std::pair<wxString, wxString> pair_vector;
                pair_vector.first = all_contract[j];
                pair_vector.second = name_project;
                vector_project_contract.push_back(pair_vector);
            }
        }
        
        //Добавляю в список те договора, которые нужно будет поменять
        wxArrayString all_projects;
        for(int i = 0; i < all_contract.GetCount(); i++)
        {
            //получаю список проектов, содержащихся в данном договоре
            if(!GetWhereSQL(set_sql[9], &all_projects, all_contract[i], bd, true))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                is_ok = true;
                return;
            }

            //получаю список, все ли полученные проекты договора, будут выполняться
            std::vector<bool> vector_is_get;
            for(int index_vector_pair = 0; index_vector_pair < vector_project_contract.size(); index_vector_pair++)
            {
                if(vector_project_contract[index_vector_pair].first.Cmp(all_contract[i]) == 0)
                {
                    bool is_get = false;
                    for(int j = 0; j < all_projects.GetCount(); j++)
                    {
                        if(vector_project_contract[index_vector_pair].second.Cmp(all_projects[j]) == 0)
                        {
                            is_get = true;
                            break;
                        }
                    }
                    vector_is_get.push_back(is_get);
                }
            }

            //проверяю список
            bool add_in_change = true;
            for(int j = 0; j < vector_is_get.size(); j++)
            {
                if(!vector_is_get[j])
                {
                    add_in_change = false;
                    break;
                }
            }
            //если все полученные проекты будут выполнятся, то тогда договор закидывю в массив договоров, которые нужно будет
            //пометить, что у данного договора все проекты выполнены
            if(add_in_change)
            {
                change_all_contact.Add(all_contract[i]);
            }
        }
    }

    int index_temporary_storage = 0;
    wxArrayString temporary_storage_id;
    for(int i = 1; i < vector_combobox.size(); i+=2)
    {
        wxString string_temporary_storage = vector_combobox[i]->GetValue();
        temporary_storage_id.Add(string_temporary_storage);
        vector_combobox[i]->SetValue(temporary_storage_name[index_temporary_storage]);
        index_temporary_storage++;
    }

    if(!CheckRepeatProjectWithContract(string_projects))
    {
        ErrorSQL(wxT("Данный проект будет выполняться в договоре"));
        is_ok = false;
        return;
    }

    if(!AllProjectsOfContract())
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }

    index_temporary_storage = 0;
    for(int i = 1; i < vector_combobox.size(); i+=2)
    {
        vector_combobox[i]->SetValue(temporary_storage_id[index_temporary_storage]);
        index_temporary_storage++;
    }

    PGresult* res1 = bd->request("BEGIN;");
    if(PQresultStatus(res1) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        ErrorSQL(str);
        res1 = bd->request("ROLLBACK;");
        PQclear(res1);
    }

    PQclear(res1);

    for(int i = 0; i < vector_projects_from_contract.size(); i++)
    {
        int size_projects_from_contract = vector_projects_from_contract[i].GetCount();
        wxArrayString used_name_contract = vector_name_contract[i];
        if(size_projects_from_contract == 0)
        {
            if(!UpdateSQL(set_sql[12], wxT("true"), used_name_contract[0], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }
    }

    int count_start = string_projects.GetCount();

    for(int j = 0; j < count_start; j++)
    {
        if(!UpdateSQL(set_sql[6], start_work, string_projects[j], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
    }
    string_projects.Clear();

    std::vector<wxComboBox*> vector_work;

    for(int i = 0; i < count_lines; i++)
    {
        vector_work.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, start_work) );
    }
    
    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    if(!InsertSQL(vector_uuid_conpr, insert_uuid, vector_work, 2, set_sql[2], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }

    insert_uuid.push_back(1);

    if(!InsertOnlyUUIDSQL(vector_uuid_wgroup, 1, set_sql[4], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }


    if(!InsertSQL(vector_all_uuid, insert_uuid, vector_wgroup, 5, set_sql[3], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }

    int count_vector_project = vector_project.size();
    if(count_vector_project > 0)
    {

        for(int i = 0; i < count_vector_project; i+=3)
        {
            if(!UpdateSQL(set_sql[5], vector_project[i+1], vector_project[i], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }

            if(!UpdateSQL(set_sql[6], vector_project[i+2], vector_project[i], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }

        //Ставлю в Таблице Договор в колонке isВсеПроектыВыполнены true у тех договор
        //чьи все проекты выполняются или уже были выполнены
        for(int i = 0; i < change_all_contact.GetCount(); i++)
        {
            if(!UpdateSQL(set_sql[13], wxT("true"), change_all_contact[i], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }
    }

    int index_vector_contract = 0;
    for(int i = 0; i < vector_is_project_null.size(); i++)
    {
        if(vector_is_project_null[i])
        {
            if(!UpdateSQL(set_sql[7], wxT("true"), vector_contract[index_vector_contract], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }
        else
        {
            if(!UpdateSQL(set_sql[7], wxT("false"), vector_contract[index_vector_contract], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }

            if(!UpdateSQL(set_sql[13], wxT("true"), vector_contract[index_vector_contract], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }

        if(!UpdateSQL(set_sql[8], vector_contract[index_vector_contract + 1], vector_contract[index_vector_contract], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
        index_vector_contract += 2;
    }

    PGresult* res = bd->request("COMMIT;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }

    PQclear(res);
}

void WGroupOkAdd::CheckAndPlusWork(std::vector<wxComboBox*> &vector_wgroup, wxString start_work)
{
    int j = 0;
    for(int i = 0; i < vector_combobox.size(); i+=2)
    {
        wxString full_name = vector_combobox[i]->GetValue();
        wxString first_element_name = full_name.Left(1);

        vector_wgroup.push_back(vector_combobox[i + 1]);
        vector_wgroup.push_back(vector_combobox[i + 1]);

        int size_vector_wgroup = vector_wgroup.size();
        if(first_element_name.Cmp(wxT("Д")) == 0)
        {
            vector_wgroup.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxT("true")) );
            vector_wgroup[size_vector_wgroup]->Show(false);
            vector_contract.push_back(full_name);
            vector_contract.push_back(vector_uuid_conpr[j]);
        }
        else
        {
            vector_wgroup.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxT("false")) );
            vector_wgroup[size_vector_wgroup]->Show(false);
            vector_project.push_back(full_name);
            vector_project.push_back(vector_uuid_conpr[j]);
            vector_project.push_back(start_work);
        }
        j++;
    }
}

bool WGroupOkAdd::CheckRepeatProjectWithContract(wxArrayString string_projects)
{
    int count_line_projects = string_projects.GetCount();
    int count_lines_vector = vector_combobox.size() / 2;
    for(int i = 0; i < count_line_projects; i++)
    {
        int index_vector = 0;
        wxString name_project = string_projects[i];
        for(int j = 0; j < count_lines_vector; j++)
        {
            wxString compare_name_project = vector_combobox[index_vector]->GetValue();
            if(name_project.Cmp(compare_name_project) == 0)
            {
                vector_combobox[index_vector]->SetSelection(-1, -1);
                int y_scroll = (j * 3) + 2;
                scrolled->Scroll(0, y_scroll);
                return false;
            }
            index_vector += 2;
        }
    }
    return true;
}

bool WGroupOkAdd::AllProjectsOfContract()
{
    wxArrayString array_name;
    for(int i = 0; i < vector_project.size(); i+=3)
    {
        array_name.Add(vector_project[i]);
    }

    int size_array_name = array_name.GetCount();
    for(int i = 0; i < size_array_name; i++)
    {
        wxArrayString name_contract;
        wxArrayString projects_from_contract;
        if(!GetWhereSQL(set_sql[10], &name_contract, array_name[i], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            return false;
        }

        vector_name_contract.push_back(name_contract);

        if(!GetWhereSQL(set_sql[11], &projects_from_contract, name_contract[0], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            return false;
        }

        vector_projects_from_contract.push_back(projects_from_contract);

        int size_projects_from_contract = projects_from_contract.GetCount();
        for(int j = 0; j < projects_from_contract.GetCount(); j++)
        {
            for(int index_name_contract = 0; index_name_contract < size_array_name; index_name_contract++)
            {
                if(projects_from_contract[j].Cmp(array_name[index_name_contract]) == 0)
                {
                    array_name.RemoveAt(index_name_contract, 1);
                    index_name_contract--;
                    size_array_name--;
                    size_projects_from_contract--;
                    i--;
                }
            }
        }

        if(i < 0)
            i = -1;
    }
    return true;
}