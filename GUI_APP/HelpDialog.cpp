#include "HelpDialog.h"

HelpDialog::HelpDialog(const wxString & title)
	: wxDialog(NULL, -1, title, wxDefaultPosition, wxSize(400, 300))
{
    panel = new wxPanel(this, wxID_ANY);

    vbox = new wxBoxSizer(wxVERTICAL);
    hbox1 = new wxBoxSizer(wxHORIZONTAL);
    hbox2 = new wxBoxSizer(wxHORIZONTAL);
    count_lines = 1;
    size_popup = 0;
    is_popup = false;

    NameProg = new wxStaticText(panel, wxID_ANY, wxT("Proect Org"));
    VersionProg = new wxStaticText(panel, wxID_ANY, wxT(" version: 0.0.1"));
    AboutProg = new wxStaticText(panel, wxID_ANY, wxT("Proect Org - это программа\n  для взаимодействия с БД"));
    SourceProg = new wxStaticText(panel, wxID_ANY, wxT(" Исходники: "));

    vbox->Add(NameProg, 0, wxALIGN_CENTRE | wxTOP | wxBOTTOM, 20);
    vbox->Add(AboutProg, 0, wxALIGN_CENTRE | wxTOP | wxBOTTOM, 10);

    m_hyperlink1 = new wxGenericHyperlinkCtrl(panel, wxID_ANY, wxT("sourse"),
        wxT("https://bitbucket.org/catuser/project_org/src/master/"));
    hbox1->Add(SourceProg);
    hbox1->Add(m_hyperlink1);
    vbox->Add(hbox1, 1, wxALIGN_CENTRE | wxTOP | wxBOTTOM, 40);

    Credits = new wxToggleButton(panel, window::id_bar::ID_CREDITS, wxT("Credits"));
    Quit = new wxButton(panel, window::id_bar::ID_QUIT, wxT("Quit"));
    hbox2->Add(Credits, 0, wxLEFT);
    hbox2->Add(new wxStaticText(panel, wxID_ANY, wxT("")), 1);
    hbox2->Add(VersionProg, 1,  wxALIGN_CENTRE);
    hbox2->Add(new wxStaticText(panel, wxID_ANY, wxT("")), 1);
    hbox2->Add(Quit, 0, wxRIGHT);

    vbox->Add(hbox2, 1, wxALL | wxEXPAND, 10);
    panel->SetSizer(vbox);

    wxTextFile credits_file(wxT("credits.txt"));
    credits_file.Open();
    if(!credits_file.IsOpened())
    {
        listbox = new wxListBox(panel, wxID_ANY, wxPoint(410, 310), wxSize(350, 100));
        listbox->Append(wxT("Can't find credits.txt file"));
    }
    else
    {
        listbox = new wxListBox(panel, wxID_ANY, wxPoint(410, 310), wxSize(350, 100));
        count_lines = credits_file.GetLineCount();
        wxString credits_line;
        for(credits_line = credits_file.GetFirstLine(); !credits_file.Eof(); credits_line = credits_file.GetNextLine())
	    {
		    listbox->Append(credits_line);
	    }
	    credits_file.Close();
    }

    Connect(window::id_bar::ID_QUIT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(HelpDialog::OnQuit));
    Connect(window::id_bar::ID_CREDITS, wxEVT_COMMAND_TOGGLEBUTTON_CLICKED, 
		wxCommandEventHandler(HelpDialog::OnPopupPanel));
        
    this->Center();
}

void HelpDialog::OnPopupPanel(wxCommandEvent &event)
{
    if(is_popup)
    {
        is_popup = false;
        listbox->SetPosition(wxPoint(410, 310));
    }
    else
    {
        is_popup = true;
        listbox->SetPosition(wxPoint(25, 40));
    }
    
    event.Skip();
}

void HelpDialog::OnQuit(wxCommandEvent &event)
{
    Close();
    event.Skip();
}