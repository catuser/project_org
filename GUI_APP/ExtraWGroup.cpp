#include "ExtraWGroup.h"

ExtraWGroup::ExtraWGroup(wxString id_wgroup_main, wxString id_contract_project_main, wxString name_wgroup_main, 
            wxString start_work_wgroup_main, bool off_ok_done, BD* bd_main)
    : wxDialog(NULL, -1, wxT("Дополнительная информация"), wxDefaultPosition, wxSize(1280, 350))
{
    id_wgroup = id_wgroup_main;
    id_contract_project = id_contract_project_main;
    name_wgroup = name_wgroup_main;
    bd = bd_main;
    start_work_wgroup = start_work_wgroup_main;
    is_ok = true;

    sizer_main = new wxBoxSizer(wxVERTICAL);
    sizer_first_buttons = new wxBoxSizer(wxHORIZONTAL);
    sizer_second_buttons = new wxBoxSizer(wxHORIZONTAL);
    sizer_third_buttons = new wxBoxSizer(wxHORIZONTAL);

    grid_sizer_boss = new wxGridSizer(1, 5, 10);
    grid_sizer_staff = new wxGridSizer(1, 5, 10);
    grid_sizer_equip = new wxGridSizer(3, 5, 10);
    grid_sizer_projects = new wxGridSizer(3, 5, 10);

    button_boss = new wxButton(this, window::id_buttons::ID_BOSS, wxT("Начальник"));
    button_staff = new wxButton(this, window::id_buttons::ID_STAFF, wxT("Сотрудники"));
    button_equip = new wxButton(this, window::id_buttons::ID_EQUIPMENT, wxT("Оборудование"));
    button_projects = new wxButton(this, window::id_buttons::ID_PROJECT, wxT("Проекты"));

    sizer_first_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_first_buttons->Add(button_boss, 0, wxALIGN_CENTER | wxALL, 5);
    sizer_first_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_first_buttons->Add(button_staff, 0, wxALIGN_CENTER | wxALL, 5);
    sizer_first_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_first_buttons->Add(button_equip, 0, wxALIGN_CENTER | wxALL, 5);
    sizer_first_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_first_buttons->Add(button_projects, 0, wxALIGN_CENTER | wxALL, 5);

    button_plus = new wxButton(this, window::id_buttons::ID_PLUS, wxT("+"));
    button_minus = new wxButton(this, window::id_buttons::ID_MINUS, wxT("-"));

    button_plus->Show(false);
    button_minus->Show(false);

    sizer_second_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_second_buttons->Add(button_minus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_second_buttons->Add(button_plus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_second_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);

    button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    button_done = new wxButton(this, window::id_buttons::ID_DONE, wxT("Завершить"));
    button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    sizer_third_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_third_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_third_buttons->Add(button_done, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_third_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_third_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    if(off_ok_done)
    {
        button_done->Show(false);
        button_ok->Show(false);
    }

    scrolled_boss =  new wxScrolledWindow(this); 
    scrolled_staff =  new wxScrolledWindow(this); 
    scrolled_equip =  new wxScrolledWindow(this); 
    scrolled_projects =  new wxScrolledWindow(this); 

    scrolled_staff->Show(false);
    scrolled_equip->Show(false);
    scrolled_projects->Show(false);

    FillFileWithSQL(wxT("sql/work_group/extra/get_extra.txt"));

    if(!FillBoss())
    {
        is_ok = false;
        return;
    }

    if(!FillStaff())
    {
        is_ok = false;
        return;
    }

    if(!FillEquip())
    {
        is_ok = false;
        return;
    }

    if(!IsContract())
    {
        is_ok = false;
        return;
    }

    sizer_main->Add(sizer_first_buttons, 0, wxEXPAND | wxALL, 5);
    sizer_main->Add(sizer_second_buttons, 0, wxEXPAND);
    sizer_main->Add(scrolled_boss, 1, wxEXPAND);
    sizer_main->Add(scrolled_staff, 1, wxEXPAND);
    sizer_main->Add(scrolled_equip, 1, wxEXPAND);
    sizer_main->Add(scrolled_projects, 1, wxEXPAND);
    sizer_main->Add(sizer_third_buttons, 0, wxEXPAND | wxTOP, 5);
	SetSizer(sizer_main);


    Connect(window::id_buttons::ID_BOSS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnBoss));
    Connect(window::id_buttons::ID_STAFF, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnStaff));
    Connect(window::id_buttons::ID_EQUIPMENT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnEquip));
    Connect(window::id_buttons::ID_PROJECT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnProject));

    Connect(window::id_buttons::ID_PLUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::AddLine));
    Connect(window::id_buttons::ID_MINUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::DeleteLine));

    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnOk));
    Connect(window::id_buttons::ID_DONE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraWGroup::OnDone));
}

void ExtraWGroup::FillFileWithSQL(wxString name_file_with_sql)
{
    wxTextFile sql_file(name_file_with_sql);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql_files.Add(sql);
        sql = sql_file.GetNextLine();
    }
    sql_file.Close();
}

bool ExtraWGroup::FillBoss()
{
    wxArrayString name_boss;
    if(!GetWhereSQL(sql_files[0], &name_boss, id_wgroup, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    box_boss = new wxComboBox(scrolled_boss, window::id_buttons::ID_COMBOBOX, name_boss[0], wxDefaultPosition, wxSize(300, -1));

    if(!GetSQL(sql_files[1], &all_names_boss, bd))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    box_boss->Append(all_names_boss);
    box_boss->AutoComplete(all_names_boss);

    grid_sizer_boss->Add(new wxStaticText(scrolled_boss, wxID_ANY, wxT("Начальник")), 0, wxALIGN_CENTER);
    grid_sizer_boss->Add(box_boss, 0, wxALIGN_CENTER);

    grid_sizer_boss->Add( new wxStaticText(scrolled_boss, wxID_ANY, wxT(" ")), 1);

    scrolled_boss->SetSizer(grid_sizer_boss);
    scrolled_boss->FitInside();
    scrolled_boss->SetScrollRate(10, 10); 
    return true;
}

bool ExtraWGroup::FillStaff()
{
    if(!GetWhereSQL(sql_files[2], &name_staff, id_wgroup, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    if(!GetSQL(sql_files[3], &all_names_staff, bd))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    if(!GetWhereSQL(sql_files[7], &id_staff_wgroup, id_wgroup, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    start_count_staff = name_staff.GetCount();

    grid_sizer_staff->Add(new wxStaticText(scrolled_staff, wxID_ANY, wxT("ФИО")), 0, wxALIGN_CENTER);

    for(int i = 0; i < start_count_staff; i++)
    {
        vector_staff.push_back( new wxComboBox(scrolled_staff, window::id_buttons::ID_COMBOBOX, name_staff[i], wxDefaultPosition, wxSize(300, -1)) );
        vector_staff[i]->Append(all_names_staff);
        vector_staff[i]->AutoComplete(all_names_staff);
        grid_sizer_staff->Add(vector_staff[i], 0, wxALIGN_CENTER);
    }

    grid_sizer_staff->Add( new wxStaticText(scrolled_staff, wxID_ANY, wxT(" ")), 1);

    scrolled_staff->SetSizer(grid_sizer_staff);
    scrolled_staff->FitInside();
    scrolled_staff->SetScrollRate(10, 10); 

    return true;
}

bool ExtraWGroup::FillEquip()
{
    if(!GetWhereSQL(sql_files[4], &info_use_equip, id_wgroup, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    if(!GetSQL(sql_files[5], &all_names_equip, bd))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    for(int i = 0; i < info_use_equip.GetCount(); i+=3)
    {
        if(info_use_equip[i+2].IsNull())
        {
            all_names_equip.Add(info_use_equip[i]);
        }
    }

    start_count_equip = info_use_equip.GetCount() / 3;


    grid_sizer_equip->Add(new wxStaticText(scrolled_equip, wxID_ANY, wxT("Название")), 0 , wxALIGN_CENTER);
    grid_sizer_equip->Add(new wxStaticText(scrolled_equip, wxID_ANY, wxT("Начало Использования")), 0 , wxALIGN_CENTER);
    grid_sizer_equip->Add(new wxStaticText(scrolled_equip, wxID_ANY, wxT("Конец Использования")), 0 , wxALIGN_CENTER);

    int index_vector = 0;
    equip_bool.Add(wxT("Да"));
    equip_bool.Add(wxT("Нету"));
    for(int i = 0; i < info_use_equip.GetCount(); i+=3)
    {
        bool is_null;
        if(info_use_equip[i + 2].IsNull())
        {
            is_null = true;

            vector_equip.push_back( new wxComboBox(scrolled_equip, window::id_buttons::ID_COMBOBOX, info_use_equip[i], 
                        wxDefaultPosition, wxSize(300, -1)) );
            vector_equip[index_vector]->Append(all_names_equip);
            vector_equip[index_vector]->AutoComplete(all_names_equip);
        }
        else
        {
            is_null = false;

            vector_equip.push_back( new wxComboBox(scrolled_equip, window::id_buttons::ID_COMBOBOX, info_use_equip[i], 
                        wxDefaultPosition, wxSize(300, -1), 0, NULL, 0 | wxCB_READONLY) );
            vector_equip[index_vector]->Append(info_use_equip[i]);
            wxArrayString array_info;
            array_info.Add(info_use_equip[i]);
            vector_equip[index_vector]->AutoComplete(array_info);
            vector_equip[index_vector]->SetValue(info_use_equip[i]);
        }

        grid_sizer_equip->Add(vector_equip[index_vector], 0, wxALL | wxALIGN_CENTER);


        vector_start_equip.push_back( new wxTextCtrl(scrolled_equip, wxID_ANY, info_use_equip[i + 1], wxDefaultPosition, 
            wxSize(300, -1), 0 | wxTE_READONLY) );

        grid_sizer_equip->Add(vector_start_equip[index_vector], 0, wxALL | wxALIGN_CENTER);


        if(is_null)
        {
            int size_equip_bool = vector_equip_bool.size();
            vector_equip_bool.push_back( new wxComboBox(scrolled_equip, window::id_buttons::ID_COMBOBOX, wxT("Нету"), 
                wxDefaultPosition, wxSize(300, -1), 0, NULL, 0 | wxCB_READONLY) );
            vector_equip_bool[size_equip_bool]->Append(equip_bool);
            vector_equip_bool[size_equip_bool]->AutoComplete(equip_bool);
            vector_equip_bool[size_equip_bool]->SetValue(wxT("Нету"));
            grid_sizer_equip->Add(vector_equip_bool[size_equip_bool], 0, wxALL | wxALIGN_CENTER);

            vector_index_ok.push_back(index_vector);
        }
        else
        {
            int count_vector = vector_end_equip.size();
            vector_end_equip.push_back( new wxTextCtrl(scrolled_equip, wxID_ANY, info_use_equip[i + 2], wxDefaultPosition, 
                wxSize(300, -1), 0 | wxTE_READONLY) );
    
            grid_sizer_equip->Add(vector_end_equip[count_vector], 0, wxALL | wxALIGN_CENTER);
        }

        index_vector++;
    }

    grid_sizer_equip->Add( new wxStaticText(scrolled_equip, wxID_ANY, wxT(" ")), 1);

    scrolled_equip->SetSizer(grid_sizer_equip);
    scrolled_equip->FitInside();
    scrolled_equip->SetScrollRate(10, 10); 

    return true;
}

bool ExtraWGroup::IsContract()
{
    wxString is_contract = name_wgroup.Left(1);
    if(is_contract.Cmp(wxT("Д")) == 0)
    {
        bool_is_contract = true;
        button_projects->Show(true);
        sizer_first_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
        if(!FillProjects())
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return false;
        }
        this->SetClientSize(1280, 350);
    }
    else
    {
        bool_is_contract = false;
        button_projects->Show(false);
        this->SetClientSize(960, 350);
    }
    
    return true;
}

bool ExtraWGroup::FillProjects()
{
    if(!GetWhereSQL(sql_files[6], &info_use_projects, id_contract_project, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    grid_sizer_projects->Add(new wxStaticText(scrolled_projects, wxID_ANY, wxT("Название")), 0 , wxALIGN_CENTER);
    grid_sizer_projects->Add(new wxStaticText(scrolled_projects, wxID_ANY, wxT("Начало Работ")), 0 , wxALIGN_CENTER);
    grid_sizer_projects->Add(new wxStaticText(scrolled_projects, wxID_ANY, wxT("Конец Работ")), 0 , wxALIGN_CENTER);

    int index_vector_projects = 0;
    for(int i = 0; i < info_use_projects.GetCount(); i+=3)
    {
        vector_projects.push_back( new wxTextCtrl(scrolled_projects, window::id_buttons::ID_COMBOBOX, info_use_projects[i], 
            wxDefaultPosition, wxSize(300, -1), wxTE_READONLY) );
        grid_sizer_projects->Add(vector_projects[index_vector_projects], 0 , wxALL | wxALIGN_CENTER);

        wxDateTime start_work = wxDateTime::Now();

        wxString string_start_work = info_use_projects[i + 1];
        int start_year = wxAtoi(string_start_work.Left(4));
        int start_month = wxAtoi(string_start_work.Mid(6, 2));
        int start_day = wxAtoi(string_start_work.Right(2));

        start_work.SetYear(start_year);
        start_work.SetMonth(wxDateTime::Month(start_month - 1));
        start_work.SetDay(start_day);

        vector_start_project.push_back( new wxDatePickerCtrl(scrolled_projects, wxID_ANY, start_work, wxDefaultPosition, wxSize(300, -1)) );
        grid_sizer_projects->Add(vector_start_project[index_vector_projects], 0 , wxALL | wxALIGN_CENTER);

        index_vector_projects++;

        int size_end_project = vector_end_project.size();
        if(info_use_projects[i + 2].IsNull())
        {
            vector_end_project.push_back( new wxDatePickerCtrl(scrolled_projects, wxID_ANY, start_work, wxDefaultPosition, wxSize(300, -1)) );
            grid_sizer_projects->Add(vector_end_project[size_end_project], 0 , wxALL | wxALIGN_CENTER);
        }
        else
        {
            wxDateTime end_work = wxDateTime::Now();

            wxString string_end_work = info_use_projects[i + 2];
            int end_year = wxAtoi(string_end_work.Left(4));
            int end_month = wxAtoi(string_end_work.Mid(6, 2));
            int end_day = wxAtoi(string_end_work.Right(2));

            end_work.SetYear(end_year);
            end_work.SetMonth(wxDateTime::Month(end_month - 1));
            end_work.SetDay(end_day);

            vector_end_project.push_back( new wxDatePickerCtrl(scrolled_projects, wxID_ANY, end_work, wxDefaultPosition, wxSize(300, -1)) );
            grid_sizer_projects->Add(vector_end_project[size_end_project], 0 , wxALL | wxALIGN_CENTER);
        }
    }

    grid_sizer_projects->Add( new wxStaticText(scrolled_projects, wxID_ANY, wxT(" ")), 1);

    scrolled_projects->SetSizer(grid_sizer_projects);
    scrolled_projects->FitInside();
    scrolled_projects->SetScrollRate(10, 10); 

    return true;
}

void ExtraWGroup::OnBoss(wxCommandEvent &event)
{
    scrolled_boss->Show(true);
    scrolled_staff->Show(false);
    scrolled_equip->Show(false);
    scrolled_projects->Show(false);

    button_plus->Show(false);
    button_minus->Show(false);

    sizer_main->Layout();

    event.Skip();
}

void ExtraWGroup::OnStaff(wxCommandEvent &event)
{
    scrolled_boss->Show(false);
    scrolled_staff->Show(true);
    scrolled_equip->Show(false);
    scrolled_projects->Show(false);

    button_plus->Show(true);
    button_minus->Show(true);

    sizer_main->Layout();

    event.Skip();
}

void ExtraWGroup::OnEquip(wxCommandEvent &event)
{
    scrolled_boss->Show(false);
    scrolled_staff->Show(false);
    scrolled_equip->Show(true);
    scrolled_projects->Show(false);

    button_plus->Show(true);
    button_minus->Show(true);

    sizer_main->Layout();

    event.Skip();
}

void ExtraWGroup::OnProject(wxCommandEvent &event)
{
    scrolled_boss->Show(false);
    scrolled_staff->Show(false);
    scrolled_equip->Show(false);
    scrolled_projects->Show(true);

    button_plus->Show(false);
    button_minus->Show(false);

    sizer_main->Layout();

    event.Skip();
}

void ExtraWGroup::AddLine(wxCommandEvent &event)
{
    if(scrolled_staff->IsShown())
    {
        AddLineStaff();
    }
    if(scrolled_equip->IsShown())
    {
        AddLineEquip();
    }

    event.Skip();
}

void ExtraWGroup::DeleteLine(wxCommandEvent &event)
{
    if(scrolled_staff->IsShown())
    {
        DeleteLineStaff();
    }
    if(scrolled_equip->IsShown())
    {
        DeleteLineEquip();
    }

    event.Skip();
}

void ExtraWGroup::AddLineStaff()
{
    int count_vector = vector_staff.size();
    int count_grid = grid_sizer_staff->GetItemCount() - 1;
    
    vector_staff.push_back( new wxComboBox(scrolled_staff, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
    vector_staff[count_vector]->Append(all_names_staff);
    vector_staff[count_vector]->AutoComplete(all_names_staff);


    grid_sizer_staff->Remove(count_grid);
    grid_sizer_staff->Add(vector_staff[count_vector], 0, wxALL | wxALIGN_CENTER);
    grid_sizer_staff->Add( new wxStaticText(scrolled_staff, wxID_ANY, wxT(" ")), 1);

    scrolled_staff->SetSizer(grid_sizer_staff);
    scrolled_staff->FitInside();
    scrolled_staff->SetScrollRate(10, 10); 
    
    this->Layout();
}

void ExtraWGroup::AddLineEquip()
{
    int count_vector = vector_equip.size();
    int count_vector_bool =  vector_equip_bool.size();
    int count_grid = grid_sizer_equip->GetItemCount() - 1;

    vector_index_ok.push_back(count_vector);

    grid_sizer_equip->Remove(count_grid);
    
    vector_equip.push_back( new wxComboBox(scrolled_equip, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
    vector_equip[count_vector]->Append(all_names_equip);
    vector_equip[count_vector]->AutoComplete(all_names_equip);

    grid_sizer_equip->Add(vector_equip[count_vector], 0, wxALL | wxALIGN_CENTER);

    wxDateTime start_work = wxDateTime::Now();

    wxString string_start_used = start_work.Format(wxT("%Y")) + wxT("-") + start_work.Format(wxT("%m")) + wxT("-") + 
                    start_work.Format(wxT("%d"));

    vector_start_equip.push_back( new wxTextCtrl(scrolled_equip, wxID_ANY, string_start_used, wxDefaultPosition, 
            wxSize(300, -1), 0 | wxTE_READONLY) );
    
    grid_sizer_equip->Add(vector_start_equip[count_vector], 0, wxALL | wxALIGN_CENTER);

    vector_equip_bool.push_back( new wxComboBox(scrolled_equip, window::id_buttons::ID_COMBOBOX, wxT("Нету"), 
                wxDefaultPosition, wxSize(300, -1), 0, NULL, 0 | wxCB_READONLY) );
    vector_equip_bool[count_vector_bool]->Append(equip_bool);
    vector_equip_bool[count_vector_bool]->AutoComplete(equip_bool);
    vector_equip_bool[count_vector_bool]->SetValue(wxT("Нету"));

    grid_sizer_equip->Add(vector_equip_bool[count_vector_bool], 0, wxALL | wxALIGN_CENTER);

    grid_sizer_equip->Add( new wxStaticText(scrolled_equip, wxID_ANY, wxT(" ")), 1);

    scrolled_equip->SetSizer(grid_sizer_equip);
    scrolled_equip->FitInside();
    scrolled_equip->SetScrollRate(10, 10); 
    
    this->Layout();
}

void ExtraWGroup::DeleteLineStaff()
{
    int count_vector = vector_staff.size();
    int index_delete = count_vector;
    if(count_vector == 0)
    {
        ErrorSQL(wxT("В группе больше нет сотрудников"));
        return;
    }

    if(start_count_staff == count_vector)
    {
        start_count_staff--;
    }

    grid_sizer_staff->Remove(index_delete);
    
    vector_staff[count_vector - 1]->Destroy();
    vector_staff.pop_back();

    scrolled_staff->SetSizer(grid_sizer_staff);
    scrolled_staff->FitInside();
    scrolled_staff->SetScrollRate(10, 10); 

    this->Layout();
}

void ExtraWGroup::DeleteLineEquip()
{
    int count_vector = vector_equip.size();
    int index_delete;
    int index_vector_index = vector_index_ok.size();
    int count_delete = 3;
    if(count_vector == 0)
    {
        ErrorSQL(wxT("Нет используемого оборудования"));
        return;
    }

    if(index_vector_index == 0)
    {
        ErrorSQL(wxT("Нет оборудование, которое можно удалить"));
        return;
    }

    int value_vector_index = vector_index_ok.back();
    index_delete = value_vector_index * 3 + 2 + 3;

    if(start_count_equip == count_vector)
    {
        start_count_equip--;
    }

    for(int i = 0; i < count_delete; i++)
    {
        grid_sizer_equip->Remove(index_delete);
        index_delete--;
    }

    
    vector_equip[value_vector_index]->Destroy();
    vector_equip.pop_back();

    vector_start_equip[value_vector_index]->Destroy();
    vector_start_equip.pop_back();


    
    int count_vector_bool = vector_equip_bool.size() - 1;
    vector_equip_bool[count_vector_bool]->Destroy();
    vector_equip_bool.pop_back();
    vector_index_ok.pop_back();


    scrolled_equip->SetSizer(grid_sizer_equip);
    scrolled_equip->FitInside();
    scrolled_equip->SetScrollRate(10, 10); 
    

    this->Layout();
}

void ExtraWGroup::OnOk(wxCommandEvent &event)
{
    sql_files.Clear();
    FillFileWithSQL(wxT("sql/work_group/extra/set_extra.txt"));

    wxString new_boss = box_boss->GetValue();

    wxArrayString check_boss;
    check_boss.Add(new_boss);

    wxArrayString id_new_boss;
    std::vector<wxArrayString> vector_all_names_boss;
    vector_all_names_boss.push_back(all_names_boss);
    std::vector<wxComboBox*> vector_name_boss;
    vector_name_boss.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, new_boss) );
    vector_name_boss[0]->Show(false);

    std::vector<wxArrayString> vector_all_names_staff;
    vector_all_names_staff.push_back(all_names_staff);

    int count_lines = vector_staff.size();

    if(!CheckInput(0, 0, vector_all_names_boss, vector_name_boss))
    {
        ErrorSQL(wxT("Данный человек не может быть руководителем проекта"));
        vector_name_boss[0]->SetSelection(-1, -1);
        int y_scroll = 2;
        scrolled_boss->Scroll(0, y_scroll);
        return;
    }

    for(int i = 0; i < count_lines; i++)
    {
        if(!CheckInput(i, 0, vector_all_names_staff, vector_staff))
        {
            ErrorSQL(wxT("Данный человек не является сотрудником"));
            vector_staff[i]->SetSelection(-1, -1);
            int y_scroll = (i * 3) + 2;
            scrolled_staff->Scroll(0, y_scroll);
            return;
        }
    }

    std::vector<wxArrayString> vector_all_names_equip;
    vector_all_names_equip.push_back(all_names_equip);

    for(int i = 0; i < vector_equip.size(); i++)
    {
        if(!CheckInput(i, 0, vector_all_names_equip, vector_equip))
        {
            ErrorSQL(wxT("Название оборудования введено не правильно"));
            vector_equip[i]->SetSelection(-1, -1);
            int y_scroll = (i * 3) + 2;
            scrolled_equip->Scroll(0, y_scroll);
            return;
        }
    }

    if(!CheckRepeatProject(count_lines, 0, 1, vector_staff, scrolled_staff))
    {
        ErrorSQL(wxT("Данный человек уже находится в группе"));
        return;
    }

    int count_vector_equip = vector_equip.size();

    std::vector<wxComboBox*> vector_new_equip;
    int index_vector_equip = 0;
    for(int i = 0; i < info_use_equip.GetCount(); i+=3)
    {
        if(info_use_equip[i+2].IsNull())
        {
            vector_new_equip.push_back(vector_equip[index_vector_equip]);
        }
        index_vector_equip++;
    }

    if(!CheckRepeatProject(vector_new_equip.size(), 0, 1, vector_new_equip, scrolled_equip))
    {
        ErrorSQL(wxT("Данное оборудование уже выбранно"));
        return;
    }

    wxString is_contract;
    if(bool_is_contract)
    {
        is_contract = wxT("true");
        if(!CheckStartProject())
        {
            ErrorSQL(wxT("Проект нельзя начать выполнять раньше,\nчем, когда приступили к работе рабочая группа"));
            return;
        }

        if(!CheckEndProject())
        {
            ErrorSQL(wxT("Проект нельзя завершить раньше,\nчем, когда приступили к его выполнению"));
            return;
        }
    }
    else
    {
        is_contract = wxT("false");
    }
    

    if(!GetWhereSQL(sql_files[0], &id_new_boss, new_boss, bd, true))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        Close();
        return;
    }

    wxArrayString update_id_staff;
    for(int i = 0; i < start_count_staff; i++)
    {
        wxString update_name_staff = vector_staff[i]->GetValue();
        if(!GetWhereSQL(sql_files[1], &update_id_staff, update_name_staff, bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    wxArrayString id_equip;
    for(int i = 0; i < vector_index_ok.size(); i++)
    {
        int index_vector_index_ok = vector_index_ok[i];
        wxString bool_name_equip = vector_equip[index_vector_index_ok]->GetValue();
        if(!GetWhereSQL(sql_files[2], &id_equip, bool_name_equip, bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    wxArrayString original_id_equip;
    for(int i = 0; i < info_use_equip.GetCount(); i+=3)
    {
        if(!GetWhereSQL(sql_files[2], &original_id_equip, info_use_equip[i], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    wxArrayString new_id_staff;
    for(int i = start_count_staff; i < count_lines; i++)
    {
        wxString new_name_staff = vector_staff[i]->GetValue();
        if(!GetWhereSQL(sql_files[1], &new_id_staff, new_name_staff, bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    if(!UpdateSQL(sql_files[3], id_new_boss[0], id_wgroup, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        Close();
        return;
    }

    if(!UpdateSQL(sql_files[14], id_new_boss[0], id_wgroup, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        Close();
        return;
    }

    int origin_count_staff = id_staff_wgroup.GetCount();
    for(int i = start_count_staff; i < origin_count_staff; i++)
    {
        if(!GetWhereSQL(sql_files[4], &id_new_boss, id_staff_wgroup[i], bd, false))
        {
            PGresult* res = bd->request("ROLLBACK;");
            PQclear(res);
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    for(int i = 0; i < start_count_staff; i++)
    {
        if(!UpdateSQL(sql_files[5], update_id_staff[i], id_staff_wgroup[i], bd))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    std::vector<wxComboBox*> vector_insert_staff;

    std::vector<wxString> vector_uuid_wgroup;
    int index_new_staff = 0;
    for(int i = start_count_staff; i < count_lines; i++)
    {
        vector_uuid_wgroup.push_back(id_wgroup);

        vector_insert_staff.push_back(new wxComboBox(this, window::id_buttons::ID_COMBOBOX, id_contract_project, wxPoint(2000, -1), wxSize(300, -1)));
        vector_insert_staff.push_back(new wxComboBox(this, window::id_buttons::ID_COMBOBOX, id_new_boss[0], wxPoint(2000, -1), wxSize(300, -1)));
        vector_insert_staff.push_back(new wxComboBox(this, window::id_buttons::ID_COMBOBOX, new_id_staff[index_new_staff], wxPoint(2000, -1), wxSize(300, -1)));
        vector_insert_staff.push_back(new wxComboBox(this, window::id_buttons::ID_COMBOBOX, is_contract, wxPoint(2000, -1), wxSize(300, -1)));

        index_new_staff++;
    }

    int has_new_staff = count_lines - start_count_staff;
    if(has_new_staff > 0)
    {
        if(!InsertSQL(vector_uuid_wgroup, insert_uuid, vector_insert_staff, 5, sql_files[6], bd))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    wxDateTime end_used = wxDateTime::Now();

    wxString string_end_used = end_used.Format(wxT("%Y")) + wxT("-") + end_used.Format(wxT("%m")) + wxT("-") + 
                    end_used.Format(wxT("%d"));
    
    std::vector<wxComboBox*> inset_yes_equip;
    std::vector<wxComboBox*> inset_no_equip;
                
    
    int second_start_count = start_count_equip - 1;
    for(int i = 0; i < vector_index_ok.size(); i++)
    {
        int number_vector_index = vector_index_ok[i];
        wxString is_used = vector_equip_bool[i]->GetValue();
        if(number_vector_index <= second_start_count)
        {
            if(is_used.Cmp(wxT("Да")) == 0)
            {
                if(!UpdateSQL(sql_files[7], string_end_used, id_equip[i], bd))
                {
                    ErrorSQL(wxT("Попробуйте снова попозже"));
                    Close();
                    return;
                }

                if(!UpdateSQL(sql_files[13], wxT("false"), id_equip[i], bd))
                {
                    ErrorSQL(wxT("Попробуйте снова попозже"));
                    Close();
                    return;
                }
            }
            continue;
        }

        wxString new_start_time = vector_start_equip[number_vector_index]->GetValue();
        if(is_used.Cmp(wxT("Да")) == 0)
        {
            inset_yes_equip.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, id_equip[i], wxPoint(2000, -1), wxSize(300, -1)) );
            inset_yes_equip.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, new_start_time, wxPoint(2000, -1), wxSize(300, -1)) );
            inset_yes_equip.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, string_end_used, wxPoint(2000, -1), wxSize(300, -1)) );
        }
        else
        {
            inset_no_equip.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, id_equip[i], wxPoint(2000, -1), wxSize(300, -1)) );
            inset_no_equip.push_back( new wxComboBox(this, window::id_buttons::ID_COMBOBOX, new_start_time, wxPoint(2000, -1), wxSize(300, -1)) );
        }
        
    }

    std::vector<wxString> vector_uuid_wgroup_equip;
    for(int i = 0; i < vector_index_ok.size(); i++)
    {
        vector_uuid_wgroup_equip.push_back(id_wgroup);
    }

    if(vector_index_ok.size() > 0)
    {
        std::vector<int> insert_uuid_wgroup;
        insert_uuid_wgroup.push_back(1);
        if(inset_yes_equip.size() > 0)
        {
            if(!InsertSQL(vector_uuid_wgroup_equip, insert_uuid_wgroup, inset_yes_equip, 4, sql_files[8], bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }

            for(int i = 0; i < inset_yes_equip.size(); i+=3)
            {
                wxString value_inset_yes_equip = inset_yes_equip[i]->GetValue();
                if(!UpdateSQL(sql_files[13], wxT("false"), value_inset_yes_equip, bd))
                {
                    ErrorSQL(wxT("Попробуйте снова попозже"));
                    Close();
                    return;
                }
            }
        }

        if(inset_no_equip.size() > 0)
        {
            if(!InsertSQL(vector_uuid_wgroup_equip, insert_uuid_wgroup, inset_no_equip, 3, sql_files[9], bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }

            for(int i = 0; i < inset_no_equip.size(); i+=2)
            {
                wxString value_inset_no_equip = inset_no_equip[i]->GetValue();
                if(!UpdateSQL(sql_files[13], wxT("true"), value_inset_no_equip, bd))
                {
                    ErrorSQL(wxT("Попробуйте снова попозже"));
                    Close();
                    return;
                }
            }
        }
    }

    int origin_count_equip = info_use_equip.GetCount() / 3;
    for(int i = start_count_equip; i < origin_count_equip; i++)
    {
        if(!DeleteEquip(sql_files[12], id_wgroup, original_id_equip[i], bd))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }

        if(!UpdateSQL(sql_files[13], wxT("false"), original_id_equip[i], bd))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }

    if(bool_is_contract)
    {
        for(int i = 0; i < vector_projects.size(); i++)
        {
            wxString string_name_work = vector_projects[i]->GetValue();

            wxDateTime start_work = vector_start_project[i]->GetValue();

            wxString string_start_work = start_work.Format(wxT("%Y")) + wxT("-") + start_work.Format(wxT("%m")) + wxT("-") + 
                            start_work.Format(wxT("%d"));

            wxDateTime end_work = vector_end_project[i]->GetValue();

            wxString string_end_work = end_work.Format(wxT("%Y")) + wxT("-") + end_work.Format(wxT("%m")) + wxT("-") + 
                            end_work.Format(wxT("%d"));

            if(!UpdateSQL(sql_files[10], string_start_work, string_name_work, bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }

            if(!UpdateSQL(sql_files[11], string_end_work, string_name_work, bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }
        }
    }

    PGresult* res1 = bd->request("COMMIT;");
    if(PQresultStatus(res1) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        ErrorSQL(str);
        res1 = bd->request("ROLLBACK;");
        PQclear(res1);
        Close();
        return;
    }

    PQclear(res1);

    Close();
    return;
}

void ExtraWGroup::OnDone(wxCommandEvent &event)
{
    sql_files.Clear();
    FillFileWithSQL(wxT("sql/work_group/extra/done_extra.txt"));

    wxArrayString original_id_equip;
    for(int i = 0; i < info_use_equip.GetCount(); i+=3)
    {
        if(!GetWhereSQL(sql_files[0], &original_id_equip, info_use_equip[i], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            Close();
            return;
        }
    }
    
    wxDateTime end_time_work = wxDateTime::Now();

    wxString string_end_time_work = end_time_work.Format(wxT("%Y")) + wxT("-") + end_time_work.Format(wxT("%m")) + wxT("-") + 
                    end_time_work.Format(wxT("%d"));

    int index_original_id_equip = 0;
    for(int i = 2; i < info_use_equip.GetCount(); i+=3)
    {
        if(info_use_equip[i].IsNull())
        {
            if(!UpdateSQL(sql_files[1], string_end_time_work, original_id_equip[index_original_id_equip], bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }

            if(!UpdateSQL(sql_files[4], wxT("false"), original_id_equip[index_original_id_equip], bd))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                Close();
                return;
            }
        }
        index_original_id_equip++;
    }

    if(bool_is_contract)
    {
        for(int i = 2; i < info_use_projects.GetCount(); i+=3)
        {
            if(info_use_projects[i].IsNull())
            {
                if(!UpdateSQL(sql_files[2], string_end_time_work, info_use_projects[i - 2], bd))
                {
                    ErrorSQL(wxT("Попробуйте снова попозже"));
                    Close();
                    return;
                }
            }
        }
    }

    if(!UpdateSQL(sql_files[3], string_end_time_work, id_contract_project, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        Close();
        return;
    }

    Close();
    return;
}

bool ExtraWGroup::CheckStartProject()
{
    for(int i = 0; i < vector_projects.size(); i++)
    {
        wxDateTime start_do_wgroup = wxDateTime::Now();

        int start_year = wxAtoi(start_work_wgroup.Left(4));
        int start_month = wxAtoi(start_work_wgroup.Mid(6, 2));
        int start_day = wxAtoi(start_work_wgroup.Right(2));

        start_do_wgroup.SetYear(start_year);
        start_do_wgroup.SetMonth(wxDateTime::Month(start_month - 1));
        start_do_wgroup.SetDay(start_day);

        wxDateTime get_start_project = vector_start_project[i]->GetValue();

        if(get_start_project.IsLaterThan(start_do_wgroup))
        {
            continue;
        }
        else
        {
            if(get_start_project.IsSameDate(start_do_wgroup))
            {
                continue;
            }
            else
            {
                vector_projects[i]->SetSelection(-1, -1);
                int y_scroll = (i * 3) + 2;
                scrolled_projects->Scroll(0, y_scroll);
                return false;
            }
                
        }
            
    }
    return true;
}

bool ExtraWGroup::CheckEndProject()
{
    for(int i = 0; i < vector_start_project.size(); i++)
    {
        wxDateTime get_start_project = vector_start_project[i]->GetValue();
        wxDateTime get_end_project = vector_end_project[i]->GetValue();

        if(get_end_project.IsLaterThan(get_start_project))
        {
            continue;
        }
        else
        {
            if(get_end_project.IsSameDate(get_start_project))
            {
                continue;
            }
            else
            {
                vector_projects[i]->SetSelection(-1, -1);
                int y_scroll = (i * 3) + 2;
                scrolled_projects->Scroll(0, y_scroll);
                return false;
            }
                
        }
            
    }
    return true;
}

bool ExtraWGroup::DeleteEquip(wxString get_sql_file, wxString first_string, wxString second_string, BD* bd)
{
    PGresult* res;
    wxTextFile sql_file(get_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    wxString select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&select_sql, pos, &sql, first_string);
    }


    pos = select_sql.Find(wxT("AND"));
    wxString full_select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&full_select_sql, pos, &select_sql, second_string);
    }
    
    const char* ch_sql = full_select_sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK");
        PQclear(res);
        return false;
    }

    return true;
}