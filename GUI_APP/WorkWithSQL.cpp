#include "WorkWithSQL.h"

void SetTextInSQL(wxString* select_sql, int pos, wxString* sql, wxString input_text)
{
    wxString s1;
    wxString s2;
    wxString s3;
    if (pos != wxString::npos)
    {
        s1 = sql->SubString(0, pos-1);
        int len_s = sql->Len() - s1.Len();
        s2 = sql->Right(len_s);
    }

    pos = s2.Find(wxT("="));

    if (pos != wxString::npos)
    {
        s3 = s2.SubString(0, pos);
        int len_s = s2.Len() - s3.Len();
        *sql = s2.Right(len_s);
        s2 = s3;
    }

    *select_sql += s1 + s2 + wxT("'") + input_text + wxT("'") + *sql;
}

void ErrorSQL(wxString text_error)
{
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        text_error, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

bool InsertSQL(std::vector<wxString> vector_uuid, std::vector<int> insert_uuid, std::vector<wxComboBox*> vector_combobox, int count_cols, 
                wxString string_sql_file, BD* bd)
{
    PGresult* res;
    wxTextFile sql_file(string_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int count_insert_uuid = insert_uuid.size();
    int number_vector_uuid = 0;
    int count_vector_combobox = vector_combobox.size();
    int count_uuid_in_one_block = count_vector_combobox / (count_cols - count_insert_uuid);
    int index_vector_combobox = 0;

    wxString while_sql = wxT("");

    while(index_vector_combobox < count_vector_combobox)
    {
        int number_insert_uuid = 0;
        wxString line_sql = wxT("\n(");
        for(int j = 0; j < count_cols; j++)
        {
            if(insert_uuid[number_insert_uuid] == j)
            {
                int number_uuid_in_block = number_vector_uuid + (number_insert_uuid * count_uuid_in_one_block);
                line_sql += wxT("'") + vector_uuid[number_uuid_in_block] + wxT("', ");
                if(number_insert_uuid != (count_insert_uuid - 1) )
                    number_insert_uuid++;
                continue;
            }
            
            line_sql += wxT("'") + vector_combobox[index_vector_combobox]->GetValue() + wxT("', ");
            index_vector_combobox++;
        }

        int size_line_sql = line_sql.Len() - 2;
        while_sql += line_sql.Left(size_line_sql) + wxT("),");

        number_vector_uuid++;
    }

    int size_while_sql = while_sql.Len() - 1;
    sql += while_sql.Left(size_while_sql) + wxT(";");

    const char* ch_sql = sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
        return false;
    }

    PQclear(res);
    return true;
}

bool InsertOnlyUUIDSQL(std::vector<wxString> vector_uuid, int count_cols, wxString string_sql_file, BD* bd)
{
    PGresult* res;
    wxTextFile sql_file(string_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int count_all_strings = vector_uuid.size() / count_cols;

    wxString while_sql = wxT("");

    for(int number_string = 0; number_string < count_all_strings; number_string++)
    {
        wxString line_sql = wxT("\n(");
        for(int number_col = 0; number_col < count_cols; number_col++)
        {
            int number_uuid_in_block = number_string + (number_col * count_all_strings);
            line_sql += wxT("'") + vector_uuid[number_uuid_in_block] + wxT("', ");            
        }

        int size_line_sql = line_sql.Len() - 2;
        while_sql += line_sql.Left(size_line_sql) + wxT("),");
    }

    int size_while_sql = while_sql.Len() - 1;
    sql += while_sql.Left(size_while_sql) + wxT(";");

    const char* ch_sql = sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
        return false;
    }

    PQclear(res);
    return true;
}

bool GetSQL(wxString get_sql_file, wxArrayString* result, BD* bd)
{
    PGresult* res;
    wxTextFile sql_file(get_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();
    
    const char* ch_sql = sql.mb_str(wxConvUTF8);
    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        PQclear(res);
        return false;
    }

    int nFields = PQnfields(res);
    for(int j = 0; j < nFields; j++)
    {
        for (int i = 0; i < PQntuples(res); i++)
        {
            wxString res_col_string(PQgetvalue(res, i, j), wxConvUTF8);
            result->Add(res_col_string);
        }
    }

    PQclear(res);
    return true;
}

bool GetWhereSQL(wxString get_sql_file, wxArrayString* result, wxString where_string, BD* bd, bool is_select)
{
    PGresult* res;
    wxTextFile sql_file(get_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    wxString select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&select_sql, pos, &sql, where_string);
    }
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(is_select)
    {
        if(PQresultStatus(res) != PGRES_TUPLES_OK)
        {
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
            ErrorSQL(str);
            PQclear(res);
            return false;
        }

        int nFields = PQnfields(res);
        for(int i = 0; i < PQntuples(res); i++)
        {
            for (int j = 0; j < nFields; j++)
            {
                wxString res_col_string(PQgetvalue(res, i, j), wxConvUTF8);
                result->Add(res_col_string);
            }
        }
    }
    else
    {
        if(PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
            ErrorSQL(str);
            PQclear(res);
            return false;
        }
    }

    return true;
}

bool UpdateSQL(wxString name_sql_file, wxString new_result, wxString where_string, BD* bd)
{
    PGresult* res;
    wxString select_sql = wxT("");
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("SET"));
    if(pos != wxString::npos)
    {
        SetTextInSQL(&select_sql, pos, &sql, new_result);
    }

    pos = select_sql.Find(wxT("WHERE"));
    wxString full_select_sql = wxT(" ");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&full_select_sql, pos, &select_sql, where_string);
    }
    
    const char* ch_sql = full_select_sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
        return false;
    }

    return true;
}