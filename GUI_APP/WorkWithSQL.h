#ifndef insertsql_h
#define insertsql_h

#include <wx/wx.h>
#include <wx/textfile.h>
#include <iostream>
#include <vector>

#include "BD.h"

void SetTextInSQL(wxString* select_sql, int pos, wxString* sql, wxString input_text);
void ErrorSQL(wxString text_error);

bool InsertSQL(std::vector<wxString> vector_uuid, std::vector<int> insert_uuid, 
        std::vector<wxComboBox*> vector_combobox, int count_cols, wxString string_sql_file, BD* bd);
bool InsertOnlyUUIDSQL(std::vector<wxString> vector_uuid, int count_cols, wxString string_sql_file, BD* bd);

bool GetSQL(wxString get_sql_file, wxArrayString* result, BD* bd);

bool GetWhereSQL(wxString get_sql_file, wxArrayString* result, wxString where_string, BD* bd, bool is_select);
bool UpdateSQL(wxString name_sql_file, wxString new_result, wxString where_string, BD* bd);

#endif