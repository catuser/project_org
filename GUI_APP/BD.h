#ifndef bd_h
#define bd_h

#include <libpq-fe.h>
#include <wx/wx.h>
#include <wx/aui/auibook.h>
#include <wx/grid.h>

#include <iostream>


class BD
{
public:
    BD();
    PGresult* request(const char* sql);
    void exit_conn();
    void error_res(PGresult* res);

    PGresult* OnUpdate(const char* sql);

    PGconn* conn;
    bool isConnect;
    char* error;
};

#endif