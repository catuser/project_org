SELECT Оборудование.idОборудование, Оборудование.Название, Оборудование.Орг,  Оборудование.idОтдел, Отдел.НазваниеОтд AS "Название Отдела", Оборудование.Используется, 
Оборудование.isОбслуживается  
FROM Оборудование 
FULL JOIN Отдел ON Отдел.idОтдел = Оборудование.idОтдел 
WHERE Оборудование.idОборудование IS NOT NULL;
