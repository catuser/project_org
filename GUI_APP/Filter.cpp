#include "Filter.h"

Filter::Filter(std::vector<TableColumn> table_colums_main, std::vector<NumberColumn> sql_filter_table_main, 
    Exel* filter_table_main, wxString name_table_main, BD* bd_main, bool* isok)
        : wxDialog(NULL, -1, wxT("Фильт"), wxDefaultPosition, wxSize(1000, 450))
{
    table_colums = table_colums_main;
    sql_filter_table = sql_filter_table_main;
    filter_table = filter_table_main;
    name_table = name_table_main;
    bd = bd_main;
    count_buttons = 0;

    wxButton* button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    wxButton* button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    wxBoxSizer* sizer_dial = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer_buttons = new wxBoxSizer(wxHORIZONTAL);
    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    wxBoxSizer* sizer_block = new wxBoxSizer(wxHORIZONTAL);
    sizer_block->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);

    wxBoxSizer* sizer_colums = new wxBoxSizer(wxVERTICAL);
    sizer_colums->Add(new wxStaticText(this, wxID_ANY, wxT("Столбцы")), 0 , wxALIGN_CENTER);

    int number_col = table_colums.size();
    for(int i = 0; i < number_col; i++)
    {
        buttons_cols.push_back(new wxCheckBox(this, wxID_ANY, table_colums[i].name_column));
        sizer_colums->Add(buttons_cols[i], 0, wxALL, 5);
    }

    sizer_block->Add(sizer_colums, 0, wxEXPAND);

    std::vector<wxBoxSizer*> boxsiser_vector;
    int number_row = sql_filter_table.size();
    for(int i = 0; i < number_row; i++)
    {
        wxString file_name = sql_filter_table[i].name_sql_file;

        boxsiser_vector.push_back( new wxBoxSizer(wxVERTICAL) );
        boxsiser_vector[i]->Add(new wxStaticText(this, wxID_ANY, sql_filter_table[i].name_column), 0 , wxALIGN_CENTER);

        wxArrayString result;

        if( !(GetSelectFromSQL(&result, file_name)) )
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("ERROR: GET SQL OF ROW."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            *isok = false;
            return;
        }

        int number_row_get_table = result.GetCount();
        int size_buttons_row = buttons_rows.size();
        for(int j = 0; j < number_row_get_table; j++)
        {
            wxString name_button = result[j];
            wxString name_row = name_button;

            if(name_button.Cmp(wxT("t")) == 0)
            {
                name_button = sql_filter_table[i].name_column + wxT(" True");
                name_row = wxT("t");
            }
            if(name_button.Cmp(wxT("f")) == 0)
            {
                name_button = sql_filter_table[i].name_column + wxT(" False");
                name_row = wxT("f");
            }
            

            std::pair<wxCheckBox*, int> button_row_pair;
            button_row_pair.first = new wxCheckBox(this, wxID_ANY, name_button);
            button_row_pair.second = sql_filter_table[i].number_column;
            buttons_rows_name.Add(name_row);

            buttons_rows.push_back(button_row_pair);
            boxsiser_vector[i]->Add(buttons_rows[j + size_buttons_row].first, 0, wxALL, 5);
        }

        sizer_block->Add(boxsiser_vector[i], 0, wxEXPAND, 10);
        result.Clear();

    }

    sizer_block->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_dial->Add(sizer_block, 0, wxEXPAND);
    sizer_dial->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_dial->Add(sizer_buttons, 0, wxEXPAND, 5);
    GetFiter();
    SetSizer(sizer_dial);
    this->Layout();

    this->SetClientSize(this->GetBestSize());

    *isok = true;

    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Filter::OnOk));
}

Filter::Filter(std::vector<TableColumn> table_colums_main, Exel* filter_table_main, 
    wxString name_table_main, BD* bd_main, int count_buttons_main, bool* isok)
        : wxDialog(NULL, -1, wxT("Фильт"), wxDefaultPosition, wxSize(1000, 450))
{
    table_colums = table_colums_main;
    sql_filter_table.clear();
    filter_table = filter_table_main;
    name_table = name_table_main;
    bd = bd_main;
    count_buttons = count_buttons_main;

    wxButton* button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    wxButton* button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    wxBoxSizer* sizer_dial = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* sizer_buttons = new wxBoxSizer(wxHORIZONTAL);
    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    wxBoxSizer* sizer_block = new wxBoxSizer(wxHORIZONTAL);
    sizer_block->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);

    wxBoxSizer* sizer_colums = new wxBoxSizer(wxVERTICAL);
    sizer_colums->Add(new wxStaticText(this, wxID_ANY, wxT("Столбцы")), 0 , wxALIGN_CENTER);

    int number_col = table_colums.size();
    for(int i = 0; i < number_col; i++)
    {
        buttons_cols.push_back(new wxCheckBox(this, window::id_buttons::ID_CHECKBOX, table_colums[i].name_column));
        sizer_colums->Add(buttons_cols[i], 0, wxALL, 5);
    }

    sizer_block->Add(sizer_colums, 0, wxEXPAND);  
    sizer_block->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_dial->Add(sizer_block, 0, wxEXPAND);
    sizer_dial->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_dial->Add(sizer_buttons, 0, wxEXPAND, 5);
    GetFiter();
    SetSizer(sizer_dial);
    this->Layout();

    this->SetClientSize(this->GetBestSize());

    *isok = true;

    check_first_part = buttons_cols[0]->GetValue(); 
    check_second_part = false; 

    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Filter::OnOk));  
    Connect(window::id_buttons::ID_CHECKBOX, wxEVT_CHECKBOX,
        wxCommandEventHandler(Filter::OnCheckBox)); 
}

void Filter::GetFiter()
{
    int len_name_table = name_table.Len();
    wxTextFile sql_file(wxT("sql/settings.txt"));
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        wxString name_col_setting = sql.Left(len_name_table);
        if(name_table.Cmp(name_col_setting) == 0)
        {
            for(int i = 0; i < buttons_cols.size(); i++)
            {
                sql = sql_file.GetNextLine();
                int len_name_col = table_colums[i].name_column.Len();
                int len_bool = sql.Len() - len_name_col - 1;
                if(table_colums[i].name_column.Cmp(sql.Left(len_name_col)) == 0)
                {
                    wxString string_bool = sql.Right(len_bool);
                    if(string_bool.Cmp(wxT("t")) == 0)
                    {
                        table_colums[i].iscolumn_show = true;
                        buttons_cols[i]->SetValue(true);
                    }
                    else
                    {
                        table_colums[i].iscolumn_show = false;
                        buttons_cols[i]->SetValue(false);
                    }
                    
                }
            }

            for(int i = 0; i < buttons_rows.size(); i++)
            {
                sql = sql_file.GetNextLine();
                wxString string_bool = sql.Right(1);
                if(string_bool.Cmp(wxT("t")) == 0)
                {
                    buttons_rows[i].first->SetValue(true);
                }
                else
                {
                    buttons_rows[i].first->SetValue(false);
                }
            }
        }
        sql = sql_file.GetNextLine();
    }
    sql_file.Close();
}

void Filter::OnOk(wxCommandEvent &event)
{
    SetFilter();
    UseFilter();
    Close();
}

void Filter::SetFilter()
{
    int len_name_table = name_table.Len();
    wxTextFile sql_file(wxT("sql/settings.txt"));
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        wxString name_col_setting = sql.Left(len_name_table);
        if(name_table.Cmp(name_col_setting) == 0)
        {
            for(int i = 0; i < buttons_cols.size(); i++)
            {
                sql = sql_file.GetNextLine();
                wxString change_line;
                change_line = table_colums[i].name_column + wxT(" ");

                if(buttons_cols[i]->GetValue())
                {
                    change_line += wxT("t");
                }
                else
                {
                    change_line += wxT("f");
                }

                int number_line = sql_file.GetCurrentLine();
                sql_file.RemoveLine(number_line);
                sql_file.InsertLine(change_line, number_line);
                
                table_colums[i].iscolumn_show = buttons_cols[i]->GetValue();
            }

            for(int i = 0; i < buttons_rows.size(); i++)
            {
                sql = sql_file.GetNextLine();
                wxString change_line;
                change_line = buttons_rows_name[i] + wxT(" ");
                if(buttons_rows[i].first->GetValue())
                {
                    change_line += wxT("t");
                }
                else
                {
                    change_line += wxT("f");
                }

                int number_line = sql_file.GetCurrentLine();
                sql_file.RemoveLine(number_line);
                sql_file.InsertLine(change_line, number_line);
            }
        }
        sql = sql_file.GetNextLine();
    }

    sql_file.Write();
    sql_file.Close();
}

void Filter::UseFilter()
{
    wxArrayString array_id_boss;
    if(name_table.Cmp(wxT("work_group")) == 0)
    {
        wxString file_name = wxT("sql/work_group/filter/get_id_boss.sql");
        if(!GetSQL(file_name, &array_id_boss, bd))
        {
            ErrorSQL(wxT("Ошибка, повторите попытку позже"));
            Close();
            return;
        }
    }

    bool first_part = false;
    bool second_part = false;
    for(int i = 0; i < table_colums.size(); i++)
    {
        if(table_colums[i].iscolumn_show)
        {
            if(count_buttons == 0)
            {
                filter_table->ShowCol(table_colums[i].number_col);
            }
            else
            {
                int x = i * count_buttons;

                if(i == 1)
                {
                    count_buttons--;
                }

                for(int j = 0; j < count_buttons; j++)
                {
                    filter_table->ShowCol(x+j);
                }

                if(i == 1)
                {
                    count_buttons++;
                }
            }
            
        }
        else
        {
            if(count_buttons == 0)
            {
                filter_table->HideCol(table_colums[i].number_col);
            }
            else
            {
                int x = i * count_buttons;

                if(i == 1)
                {
                    count_buttons--;
                }

                for(int j = 0; j < count_buttons; j++)
                {
                    filter_table->HideCol(x+j);
                }

                if(i == 1)
                {
                    count_buttons++;
                }
            }
        }
        
    }

    if(name_table.Cmp(wxT("departments")) == 0 && table_colums[4].iscolumn_show)
    {
        int size_table = filter_table->GetNumberRows();
        for(int row = 0; row < size_table; row++)
        {
            wxString name_boss = filter_table->GetCellValue(row, 3);
            wxString name_fio = filter_table->GetCellValue(row, 4);
            if(name_boss.Cmp(name_fio) != 0)
            {
                filter_table->ShowRow(row);
            }
        }
    }

    // if(name_table.Cmp(wxT("work_group")) == 0 && table_colums[8].iscolumn_show)
    // {
    //     int size_table = filter_table->GetNumberRows();
    //     int start_name_array = array_id_boss.GetCount() / 2;
    //     wxString id_boss = array_id_boss[0];
    //     wxString name_boss = array_id_boss[start_name_array];
    //     for(int row = 0; row < size_table; row++)
    //     {
    //         wxString new_id_boss = filter_table->GetCellValue(row, 6);
    //         if(id_boss.Cmp(new_id_boss) != 0)
    //         {
    //             for(int i = 0; i < start_name_array; i++)
    //             {
    //                 if(array_id_boss[i].Cmp(new_id_boss) == 0)
    //                 {
    //                     name_boss = array_id_boss[i+start_name_array];
    //                 }
    //             }
    //         }
    //         wxString name_fio = filter_table->GetCellValue(row, 8);
    //         if(name_boss.Cmp(name_fio) != 0)
    //         {
    //             filter_table->ShowRow(row);
    //         }
    //     }
    // }

    std::vector<int> vector_count;
    for(int i = 0; i < buttons_rows.size();)
    {
        int number_col_table = buttons_rows[i].second;
        int count_same_value = 0;
        while(number_col_table == buttons_rows[i].second)
        {
            count_same_value++;
            i++;
            if(i == buttons_rows.size())
            {
                break;
            }
        }
        vector_count.push_back(count_same_value);
    }

    int count_row = filter_table->GetNumberRows();
    for(int row = 0; row < count_row; row++)
    {
        bool ishide = false;
        int counter_rows = 0;
        for(int i = 0; i < vector_count.size(); i++)
        {
            int number_col_table = buttons_rows[counter_rows].second;
            for(int j = 0; j < vector_count[i]; j++)
            {
                if(buttons_rows_name[counter_rows].Cmp(filter_table->GetCellValue(row, number_col_table)) == 0)
                {
                    if(buttons_rows[counter_rows].first->GetValue())
                    {
                    }
                    else
                    {
                        ishide = true;
                    }
                }
                counter_rows++;
            }
        }
        if(ishide)
        {
            filter_table->HideRow(row);
        }
        else
        {
            filter_table->ShowRow(row);
        }
        
    }

    if(name_table.Cmp(wxT("departments")) == 0 && !table_colums[4].iscolumn_show)
    {
        int size_table = filter_table->GetNumberRows();
        for(int row = 0; row < size_table; row++)
        {
            wxString name_boss = filter_table->GetCellValue(row, 3);
            wxString name_fio = filter_table->GetCellValue(row, 4);
            if(name_boss.Cmp(name_fio) != 0)
            {
                filter_table->HideRow(row);
            }
        }
    }

    // if(name_table.Cmp(wxT("work_group")) == 0 && !table_colums[8].iscolumn_show)
    // {
    //     int size_table = filter_table->GetNumberRows();
    //     int start_name_array = array_id_boss.GetCount() / 2;
    //     wxString id_boss = array_id_boss[0];
    //     wxString name_boss = array_id_boss[start_name_array];
    //     for(int row = 0; row < size_table; row++)
    //     {
    //         wxString new_id_boss = filter_table->GetCellValue(row, 6);
    //         if(id_boss.Cmp(new_id_boss) != 0)
    //         {
    //             for(int i = 0; i < start_name_array; i++)
    //             {
    //                 if(array_id_boss[i].Cmp(new_id_boss) == 0)
    //                 {
    //                     name_boss = array_id_boss[i+start_name_array];
    //                 }
    //             }
    //         }
    //         wxString name_fio = filter_table->GetCellValue(row, 8);
    //         if(name_boss.Cmp(name_fio) != 0)
    //         {
    //             filter_table->HideRow(row);
    //         }
    //     }
    // }
    
    filter_table->Layout();
}

bool Filter::GetSelectFromSQL(wxArrayString* result, wxString name_sql_file)
{
    PGresult* res;
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();
    
    const char* ch_sql = sql.mb_str(wxConvUTF8);
    res = bd->request(ch_sql);

    for (int i = 0; i < PQntuples(res); i++)
    {
        wxString res_col_string(PQgetvalue(res, i, 0), wxConvUTF8);
        result->Add(res_col_string);
    }

    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            str, wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        PQclear(res);
        return false;
    }
    PQclear(res);
    return true;
}

void Filter::OnCheckBox(wxCommandEvent &event)
{
    if(check_first_part != buttons_cols[0]->GetValue())
    {
        buttons_cols[1]->SetValue(check_first_part);
        check_first_part = buttons_cols[0]->GetValue();
    }

    if(buttons_cols[1]->GetValue())
    {
        buttons_cols[0]->SetValue(false);
        check_first_part = false;
    }
    else
    {
        buttons_cols[0]->SetValue(true);
        check_first_part = true;
    }
    event.Skip();
}