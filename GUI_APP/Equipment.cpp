#include "Equipment.h"

Equipment::Equipment(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main) :
            wxPanel(panel_buttons, wxID_ANY)
{
    isonequipment_table = false;
    isonequipment_used = false;
    isonequipment_efficiency = false;

    is_read_only_equip = true;

    table_buttons = table;
    table_buttons_sizer = table_sizer;
    bd = bd_main;
    nb = nb_main;
    error_bd = error_bd_main;
}

void Equipment::EquipmentSetSizer()
{
    equipment_table = new wxButton(table_buttons, window::id_buttons::ID_EQUIPMENT_TABLE, wxT("Оборудование"), wxDefaultPosition, wxSize(121, 33));
    equipment_used = new wxButton(table_buttons, window::id_buttons::ID_EQUIPMENT_USED, wxT("Исп Об"), wxDefaultPosition, wxSize(121, 33));
    equipment_efficiency = new wxButton(table_buttons, window::id_buttons::ID_EQUIPMENT_EFFICIENCY, wxT("Эфф Об"), wxDefaultPosition, wxSize(121, 33));

    table_buttons_sizer->Add(equipment_table, 0, wxLEFT, 4);
    table_buttons_sizer->Add(equipment_used, 0, wxLEFT, 4);
    table_buttons_sizer->Add(equipment_efficiency, 0, wxLEFT, 4);

    table_buttons->SetSizer(table_buttons_sizer);
    table_buttons->Layout();

    int height = 100;
    table_buttons->SetScrollbars(0, 2, 0, height/2);
}

void Equipment::OnEquipmentTable(bool isupdate)
{
    if(!isonequipment_table)
    {
        table_vector_equipment_table.clear();
        filter_equipment_table.clear();
        isonequipment_table = true;

        wxTextFile sql_file(wxT("sql/equipment_table/equipment_table.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onequipment_table = new Exel(nb);
        onequipment_table->is_read_only = is_read_only_equip;
        onequipment_table->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onequipment_table, wxT("Оборудование"), true);
        nb->Layout();

        for(int i = 0; i < 7; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onequipment_table->GetColLabelValue(i);
            table_columns.iscolumn_show = onequipment_table->IsColShown(i);
            table_columns.number_col = i;
            table_vector_equipment_table.push_back(table_columns);
        }

        NumberColumn filter_sql;

        filter_sql.name_sql_file = wxT("sql/equipment_table/filter/filter_org.sql");
        filter_sql.name_column = wxT("Организация");
        filter_sql.number_column = 2;
        filter_equipment_table.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/equipment_table/filter/filter_name_department.sql");
        filter_sql.name_column = wxT("Отделы");
        filter_sql.number_column = 4;
        filter_equipment_table.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/equipment_table/filter/filter_used.sql");
        filter_sql.name_column = wxT("Используется");
        filter_sql.number_column = 5;
        filter_equipment_table.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/equipment_table/filter/filter_sevice.sql");
        filter_sql.name_column = wxT("Обслуживается");
        filter_sql.number_column = 6;
        filter_equipment_table.push_back(filter_sql);

        bool isok = false;
        Filter* filter_equipment = new Filter(table_vector_equipment_table, filter_equipment_table, onequipment_table, wxT("equipment_table"), bd, &isok);
        if(isok)
        {
            filter_equipment->UseFilter(); 
            filter_equipment->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onequipment_table);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}


void Equipment::OnEquipmentUsed(bool isupdate)
{
    if(!isonequipment_used)
    {
        table_vector_equipment_used.clear();
        filter_equipment_used.clear();
        isonequipment_used = true;

        wxTextFile sql_file(wxT("sql/equipment_used/equipment_used.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onequipment_used = new Exel(nb);
        onequipment_used->is_read_only = is_read_only_equip;
        onequipment_used->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onequipment_used, wxT("Исп Оборудование"), true);
        nb->Layout();

        for(int i = 0; i < 6; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onequipment_used->GetColLabelValue(i);
            table_columns.iscolumn_show = onequipment_used->IsColShown(i);
            table_columns.number_col = i;
            table_vector_equipment_used.push_back(table_columns);
        }

        // NumberColumn filter_sql;
        // filter_sql.name_sql_file = wxT("sql/equipment_used/filter/filter_name.sql");
        // filter_sql.name_column = wxT("Название");
        // filter_sql.number_column = 1;
        // filter_equipment_used.push_back(filter_sql);

        bool isok = false;
        Filter* filter_equipment = new Filter(table_vector_equipment_used, filter_equipment_used, onequipment_used, wxT("equipment_used"), bd, &isok);
        if(isok)
        {
            filter_equipment->UseFilter(); 
            filter_equipment->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onequipment_used);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}


void Equipment::OnEquipmentEfficiency(bool isupdate)
{
    if(!isonequipment_efficiency)
    {
        table_vector_equipment_efficiency.clear();
        filter_equipment_efficiency.clear();
        isonequipment_efficiency = true;

        wxTextFile sql_file(wxT("sql/equipment_efficiency/equipment_efficiency.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onequipment_efficiency = new Exel(nb);
        onequipment_efficiency->is_read_only = is_read_only_equip;
        onequipment_efficiency->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onequipment_efficiency, wxT("Эффективность Об"), true);
        nb->Layout();

        for(int i = 0; i < 3; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onequipment_efficiency->GetColLabelValue(i);
            table_columns.iscolumn_show = onequipment_efficiency->IsColShown(i);
            table_columns.number_col = i;
            table_vector_equipment_efficiency.push_back(table_columns);
        }

        // NumberColumn filter_sql;
        // filter_sql.name_sql_file = wxT("sql/equipment_efficiency/filter/filter_name.sql");
        // filter_sql.name_column = wxT("Название");
        // filter_sql.number_column = 1;
        // filter_equipment_efficiency.push_back(filter_sql);

        bool isok = false;
        Filter* filter_equipment = new Filter(table_vector_equipment_efficiency, filter_equipment_efficiency, 
            onequipment_efficiency, wxT("equipment_efficiency"), bd, &isok);
        if(isok)
        {
            filter_equipment->UseFilter(); 
            filter_equipment->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onequipment_efficiency);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Equipment::OnUpdate(int number_exel, wxString file_name)
{
    wxTextFile sql_file(file_name);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql_string = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql_string += sql_file.GetNextLine();
    }
    sql_file.Close();
    const char* sql = sql_string.mb_str(wxConvUTF8);

    switch(number_exel)
    {
        case 1:
        {
            onequipment_table->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_table, filter_equipment_table, 
                onequipment_table, wxT("equipment_table"), bd, &isok);
            if(isok)
            {
                filter_equipment->UseFilter(); 
                filter_equipment->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 2:
        {
            onequipment_used->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_used, filter_equipment_used, 
                onequipment_used, wxT("equipment_used"), bd, &isok);
            if(isok)
            {
                filter_equipment->UseFilter(); 
                filter_equipment->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 3:
        {
            onequipment_efficiency->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_efficiency, filter_equipment_efficiency, 
                onequipment_efficiency, wxT("equipment_efficiency"), bd, &isok);
            if(isok)
            {
                filter_equipment->UseFilter(); 
                filter_equipment->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Equipment::OnFilter(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_table, filter_equipment_table, 
                onequipment_table, wxT("equipment_table"), bd, &isok);
            filter_equipment->ShowModal();
            filter_equipment->Destroy();
            break;
        }
        case 2:
        {
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_used, filter_equipment_used, 
                onequipment_used, wxT("equipment_used"), bd, &isok);
            filter_equipment->ShowModal();
            filter_equipment->Destroy();
            break;
        }
        case 3:
        {
            bool isok = false;
            Filter* filter_equipment = new Filter(table_vector_equipment_efficiency, filter_equipment_efficiency, 
                onequipment_efficiency, wxT("equipment_efficiency"), bd, &isok);
            filter_equipment->ShowModal();
            filter_equipment->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Equipment::OnSearch(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            Search* search_equipment = new Search(this, table_vector_equipment_table, onequipment_table);
            search_equipment->Show(true);
            break;
        }
        case 2:
        {
            Search* search_equipment = new Search(this, table_vector_equipment_used, onequipment_used);
            search_equipment->Show(true);
            break;
        }
        case 3:
        {
            Search* search_equipment = new Search(this, table_vector_equipment_efficiency, onequipment_efficiency);
            search_equipment->Show(true);
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Equipment::OnAdd(int number_exel)
{
    switch(number_exel)
    {
    case 1:
        {
            bool is_ok = true;
            MyAdd* add_equipment = new MyAdd(wxT("sql/equipment_table/add/get_add.txt"), onequipment_table, wxT("equipment_table"), bd, &is_ok);
            if(is_ok)
            {
                add_equipment->ShowModal();
                OnUpdate(1, wxT("sql/equipment_table/equipment_table.sql"));
            }
            add_equipment->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Equipment::OnChange(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool is_ok = true;
            Change* change_equipment = new Change(wxT("sql/equipment_table/change/get_change.txt"), onequipment_table, wxT("equipment_table"), bd, &is_ok);
            if(is_ok)
            {
                change_equipment->ShowModal();
                OnUpdate(1, wxT("sql/equipment_table/equipment_table.sql"));
            }
            change_equipment->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Equipment::OnDelete(int number_exel)
{
    switch(number_exel)
        {
            case 1:
            {
                OnDeleteEqTable();
                break;
            }
            default:
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Equipment don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
        }
}

int Equipment::OnDeleteSQL(wxString name_file, wxString cell_id, bool isselect)
{
    PGresult* res_sql;
    wxString select_sql = wxT(" ");
    wxTextFile sql_file(name_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    while(pos != wxString::npos)
    {
        wxString s1;
        wxString s2;
        wxString s3;
        if (pos != wxString::npos)
        {
            s1 = sql.SubString(0, pos-1);
            int len_s = sql.Len() - s1.Len();
            s2 = sql.Right(len_s);
        }

        pos = s2.Find(wxT("="));
        if (pos != wxString::npos)
        {
            s3 = s2.SubString(0, pos);
            int len_s = s2.Len() - s3.Len();
            sql = s2.Right(len_s);
            s2 = s3;
        }

        select_sql += s1 + s2 + wxT("'") + cell_id + wxT("'");
        pos = sql.Find(wxT("WHERE"));
    }

    select_sql += sql + wxT("\0");
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    res_sql = bd->request(ch_sql);

    if(!(ErrorRequest(res_sql, isselect)))
    {
        return -1;
    }
    

    int count_int = 0;
    if(isselect)
    {
        char* count_sql = PQgetvalue(res_sql, 0, 0);
        count_int = atoi(count_sql);
    }
    return count_int;
}

void Equipment::OnDeleteEqTable()
{
    wxArrayInt delete_rows = onequipment_table->GetSelectedRows();
    int i = delete_rows.GetCount() - 1;
    while(i >= 0)
    {
        wxString cell_id = onequipment_table->GetCellValue(delete_rows[i], 0);
        if(!OnDeleteEqTableMainPart(cell_id))
        {
            delete_rows.Clear();
            return;
        }
        
        i--;
    }
    delete_rows.Clear();

    wxGridCellCoordsArray delete_cells = onequipment_table->GetSelectedCells();
    i = delete_cells.Count() - 1;
    while(i >= 0) 
    {
	    int row = delete_cells[i].GetRow();   
        wxString cell_id = onequipment_table->GetCellValue(row, 0);
        if(!OnDeleteEqTableMainPart(cell_id))
        {
            delete_cells.Clear();
            return;
        }
        i--;
	}
    delete_cells.Clear();
    OnUpdate(1, wxT("sql/equipment_table/equipment_table.sql"));
}

bool Equipment::OnDeleteEqTableMainPart(wxString id_eq)
{
    int result = OnDeleteSQL(wxT("sql/equipment_table/delete/delete_isused.sql"), id_eq, true);
    if( result == -1)
    {
        return false;
    }

    if(result > 0)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Выбранное оборудование используется"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    result = OnDeleteSQL(wxT("sql/equipment_table/delete/delete.sql"), id_eq, false);

    if(result == -1)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        wxT("Не удалось удалить запись"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    return true;
}

void Equipment::ErrorConnectBD()
{
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        error_bd, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

bool Equipment::ErrorRequest(PGresult* res, bool isselect)
{
    if(isselect)
    {
        if(PQresultStatus(res) != PGRES_TUPLES_OK)
        {
            std::cout << "RES ERROR" << std::endl;
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
                wxMessageDialog *dial = new wxMessageDialog(NULL,
            str, wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            return false;
        }
    }
    else
    {
        if(PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            std::cout << "RES ERROR" << std::endl;
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
                wxMessageDialog *dial = new wxMessageDialog(NULL,
            str, wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            return false;
        }
    }
    return true;
}

void Equipment::SetBool(bool is_read_only)
{
    is_read_only_equip = is_read_only;

    if(isonequipment_table)
    {
        onequipment_table->is_read_only = is_read_only;
    }

    if(isonequipment_used)
    {
        onequipment_used->is_read_only = is_read_only;
    }

    if(isonequipment_efficiency)
    {
        onequipment_efficiency->is_read_only = is_read_only;
    }
}