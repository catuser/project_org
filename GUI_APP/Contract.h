#include <wx/wx.h>
#include <wx/aui/auibook.h>
#include <wx/textfile.h>

#include "id.h"
#include "Exel.h"
#include "Search.h"
#include "Filter.h"
#include "Change.h"
#include "MyAdd.h"

class Contract : public wxPanel
{
public:
    Contract(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main);
    void ContractSetSizer();
    void ErrorConnectBD();

    void OnCustomer(bool isupdate);
    void OnContractTable(bool isupdate);
    void OnProject(bool isupdate);
    void OnContractProject(bool isupdate);
    void OnSubcontrOrg(bool isupdate);
    void OnCPEfficiency(bool isupdate);

    void OnUpdate(int number_exel, wxString file_name);
    void OnUpdateTwo(int number_exel, wxString sql1, wxString sql2);
    void OnFilter(int number_exel);
    void OnSearch(int number_exel);
    void OnAdd(int number_exel);
    void OnChange(int number_exel);

    void SetBool(bool is_read_only);

    wxScrolledWindow* table_buttons;
    wxBoxSizer* table_buttons_sizer;
    BD* bd;
    wxString error_bd;
    wxAuiNotebook* nb;

    std::vector<TableColumn> table_vector_customer;
    std::vector<TableColumn> table_vector_contract_table;
    std::vector<TableColumn> table_vector_project;
    std::vector<TableColumn> table_vector_contract_project;
    std::vector<TableColumn> table_vector_subcontr_org;
    std::vector<TableColumn> table_vector_c_p_efficiency;

    std::vector<NumberColumn> filter_customer;
    std::vector<NumberColumn> filter_contract_table;
    std::vector<NumberColumn> filter_project;
    std::vector<NumberColumn> filter_contract_project;
    std::vector<NumberColumn> filter_subcontr_org;
    std::vector<NumberColumn> filter_c_p_efficiency;

    wxButton* customer;
    wxButton* contract_table;
    wxButton* project;
    wxButton* contract_project;
    wxButton* subcontr_org;
    wxButton* c_p_efficiency;

    Exel* oncustomer;
    Exel* oncontract_table;
    Exel* onproject;
    Exel* oncontract_project;
    Exel* onsubcontr_org;
    Exel* onc_p_efficiency;

    bool isoncustomer;
    bool isoncontract_table;
    bool isonproject;
    bool isoncontract_project;
    bool isonsubcontr_org;
    bool isonc_p_efficiency;

    bool is_read_only_contract;
};