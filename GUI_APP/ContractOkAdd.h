#include "Exel.h"
#include "id.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class ContractOkAdd
{
public:
    ContractOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, std::vector<wxComboBox*> vector_combobox_contract_main, 
            wxScrolledWindow* scrolled_main);
    void GetTableSQL();
    void GetNewProject();
    bool InsertContract(std::vector<wxString> vector_uuid, std::vector<wxComboBox*> vector_combobox, 
        int count_cols, int number_sql_file);
    
    std::vector<wxString> set_sql;
    Exel* table;
    Exel* uuid_project;
    BD* bd;
    wxScrolledWindow* scrolled;

    bool is_ok;
    wxArrayString result;

    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxComboBox*> vector_combobox_contract;
    std::vector<wxString> vector_uuid_contract;
    std::vector<wxString> vector_uuid_project;
    std::vector<wxArrayString> select_result;

    std::vector<wxComboBox*> insert_project;
    std::vector<wxComboBox*> insert_contact_project;
    std::vector<wxString> vector_name_project;
};