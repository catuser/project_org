#ifndef change_h
#define change_h

#include <wx/wx.h>

#include "Exel.h"
#include "id.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

struct ChangeInfo
{
    int number_col;
    bool ischange_arbitrarily;
    wxString get_sql_file;
    wxString set_sql_file;
};

class Change : public wxDialog
{
public:
    Change(wxString get_change_main, Exel* change_table_main, wxString name_table_main, BD* bd_main, bool* is_ok);
    void GetFillingVector();
    void SetFillingVector();
    bool CheckIsSelected();
    bool GetSelectFromSQL(wxArrayString* result, wxString name_sql_file);
    bool SetSQL(wxString name_sql_file, wxString result, wxString where_string);
    void SetTextSQL(wxString* select_sql, int pos, wxString* sql, wxString input_text);
    void AddNameCols();
    bool FillingDialog();
    void FillingDialogMainPart(int* rows, int number_count_rows, int* number_real_count_rows, int number_row);
    void DefaultFillingLine(int number_count_rows, int number_row);
    void StaffFillingLine(int number_count_rows, int number_row);
    void ContractFillingLine(int number_count_rows, int number_row, int dont_change);
    void FillingLineAdd(wxString cell_value, int line_number_col, int index_vector, int* number_combobox_result);
    void DeleteCurrencySymbol(int number_row, int number_col);
    void OnOk(wxCommandEvent &event);
    bool CheckChangeDepartment(int number_combobox, int score_number_row);
    bool DepartmentOk();
    bool GetDepartmentSQL(wxString name_sql_file, wxString* result, wxString where_string);
    bool ChangeInsertSQL(wxString name_sql_file, wxString insert_string);
    bool EquipmentSQL(wxString new_result, wxString where_string);
    wxString get_change;
    wxString set_change;
    Exel* change_table;
    wxString name_table;
    BD* bd;

    wxBoxSizer* vsizer;
    wxBoxSizer* sizer_name_cols;
    wxBoxSizer* sizer_buttons; 
    wxScrolledWindow* scrolled;
    wxGridSizer* grid_sizer;

    wxArrayString read_result;
    wxArrayInt number_rows;
    std::vector<ChangeInfo> vector_change;
    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxArrayString> combobox_result;
};

#endif