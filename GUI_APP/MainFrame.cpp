#include "MainFrame.h"


MainFrame::MainFrame (const wxString &title) :
    wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(640, 280))
{
    //Флаг - подлючились ли к БД или нет
    isConnect_bd = false;

    //инициализация сплит меню
    spl = new wxSplitterWindow(this, window::id_bar::ID_SPLITWINDOW);

    //инициализация главной панели, на которой будут располагаться другие панели с кнопками, кроме auinotebook
    //в котором находятся таблицы
    panel_buttons = new wxPanel(spl, wxID_ANY);

    //инициализация панелей с кнопками в верхней части приложения
    bdbuttons();

    //инициализация панели, на которой будет находится auinotebook (панель с вкладками, 
    //которые можно выводить нужную информацию. В данном случае, там выводятся таблицы
    // с информацией полученной от БД)
    panel2 = new wxPanel(spl, wxID_ANY);

    nb = new wxAuiNotebook(panel2, window::panels::ID_AUINoteBook, wxDefaultPosition, wxDefaultSize, wxAUI_NB_BOTTOM | wxAUI_NB_TAB_MOVE |
            wxAUI_NB_SCROLL_BUTTONS | wxAUI_NB_CLOSE_ON_ACTIVE_TAB);

    //Инициализация объектов, в которых содержатся методы для взаимодейсвия с таблицами
    //Объекты разделены на блоки. То есть в Personal идет взаимодействие  с таблицами Работники, Отделы, Рабочии группы
    personnal = new Personnal(panel_buttons, table_buttons, table_buttons_sizer, bd, nb, error_bd);
    equipment = new Equipment(panel_buttons, table_buttons, table_buttons_sizer, bd, nb, error_bd);
    contract = new Contract(panel_buttons, table_buttons, table_buttons_sizer, bd, nb, error_bd);

    //задание позиции auinotebook в его панели
    wxBoxSizer* panel2Sizer = new wxBoxSizer(wxHORIZONTAL);
    panel2Sizer->Add(nb, 1, wxEXPAND);
    panel2->SetSizer(panel2Sizer);

    //Разделение двух панелей
    spl->SplitHorizontally(panel_buttons, panel2, 150);

    wxBoxSizer* topSizer = new wxBoxSizer(wxHORIZONTAL);
    topSizer->Add(spl, 1, wxEXPAND);
    SetSizerAndFit(topSizer);

    //инициализация бар меню
    menubar = new wxMenuBar;
    //инициализация кнопок, которые будут расположенны в бар меню
    file = new wxMenu;

    help = new wxMenu;
    //инициализация кнопок в меню.
    about_program = new wxMenuItem(help, wxID_ABOUT, wxT("&About program"));
    help_doc = new wxMenuItem(help, wxID_HELP, wxT("&Help\tCtrl+H"));
    //добавления этих вкнопок в мменю
    help->Append(about_program);
    help->Append(help_doc);

    quit = new wxMenuItem(file, wxID_EXIT, wxT("&Quit\tCtrl+W"));
    file->Append(quit);

    split = new wxMenu;
    split->AppendCheckItem(window::id_bar::ID_SPLITMENU, wxT("&Split"));
    split->Check(window::id_bar::ID_SPLITMENU, true);
    split->AppendCheckItem(window::id_bar::ID_READ_ONLY, wxT("&Read Only"));
    split->Check(window::id_bar::ID_READ_ONLY, true);

    //добавление кнопок меню в бар меню
    menubar->Append(file, wxT("&File"));
    menubar->Append(help, wxT("&Help"));
    menubar->Append(split, wxT("&Options"));
    SetMenuBar(menubar);

    //Задание минимального размера окна.
    //Уменьшить сильнее окно нельзя будет заданных размеров (640x480)
    this->SetMinSize(wxSize(640, 480));

    //Тут указываются какие функции должны начать выполняться, когда ID (например кнопки) произошел какой либо ивент
    //(например бала нажата кнопка)
    Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED,
        wxCommandEventHandler(MainFrame::OnQuit));
    Connect(wxID_HELP, wxEVT_COMMAND_MENU_SELECTED,
        wxCommandEventHandler(MainFrame::OnHelp));
    Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED,
        wxCommandEventHandler(MainFrame::OnAboutProgram));

    Connect(window::id_bar::ID_SPLITMENU, wxEVT_COMMAND_MENU_SELECTED,
        wxCommandEventHandler(MainFrame::OnMenuSplit));
    Connect(window::id_bar::ID_SPLITWINDOW, wxEVT_SPLITTER_UNSPLIT,
        wxSplitterEventHandler(MainFrame::OnUnsplit));
    
    Connect(window::id_bar::ID_READ_ONLY, wxEVT_COMMAND_MENU_SELECTED,
        wxCommandEventHandler(MainFrame::OnReadOnly));

    //Чтобы окно при запуске располагалось в центре экрана
    Centre();
}

//При нажатии кнопки Quit срабатывает данная функция.
//Она закрывает окно
void MainFrame::OnQuit(wxCommandEvent &event)
{
    //Отключаемся от БД
    bd->exit_conn();
    //Закрываем окно
    Close(true);
    //Если после данного эвента, должны сразу срабатывать другие
    //то Skip позваляет это сделать.
    //То есть без Skip() следуюющие эвенты не выполнялись бы
    event.Skip();
}

//Открывает файл help стандартным редактором
void MainFrame::OnHelp(wxCommandEvent &event)
{
    wxLaunchDefaultApplication(wxT("help.odt"));
    event.Skip();
}

//Открывается панель, в котором находится информация о программе
void MainFrame::OnAboutProgram(wxCommandEvent &event)
{
    HelpDialog *custom = new HelpDialog(wxT("About Program"));
    custom->ShowModal();
    custom->Destroy();
}

//Устанавливает сплит на определенную позицию
//Или поднимает его до самого вверха, тем самым закрывая панель, 
//в которой находятся все кнопки
void MainFrame::OnMenuSplit(wxCommandEvent &event)
{
    if(!spl || !panel_buttons || !panel2)
    {
        return;
    }

    if(event.IsChecked())
    {
        spl->SplitHorizontally(panel_buttons, panel2, 150);
    }
    else
    {
        spl->Unsplit(panel_buttons);
    }
    event.Skip();
}

//Если самостоятельно спрятать одну из панелей (кнопок или notebook),
//то автоматически галочка пропадает из check box
void MainFrame::OnUnsplit(wxSplitterEvent &event)
{
    if(menubar)
    {
        menubar->Check(window::id_bar::ID_SPLITMENU, false);
    }
    event.Skip();
}

//При снятии галочки с кнопки read only
//ставит во всех таблицах флаг того, что 
//в панелях, где есть выбор можно вводить любые символы
// Или наоборот
void MainFrame::OnReadOnly(wxCommandEvent &event)
{
    if(event.IsChecked())
    {
        personnal->SetBool(true);
        equipment->SetBool(true);
        contract->SetBool(true);
    }
    else
    {
        personnal->SetBool(false);
        equipment->SetBool(false);
        contract->SetBool(false);
    }
    event.Skip();
}

//инициализация панелей с кнопками в верхней части приложения
void MainFrame::bdbuttons()
{
    //Соединение с БД
    bd = new BD();
    if(!bd->isConnect)
    {
        error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
        bd->exit_conn();
        ErrorConnectBD();
        isConnect_bd = false;
    }
    else
    {
        isConnect_bd = true;
    }
    
    //инициализация панели, имеющий уже встроенный скрол бар
    wxScrolledWindow* main_buttons = new wxScrolledWindow(panel_buttons, wxID_ANY, wxPoint(0, 0), wxSize(130, 100));

    //инициализация главных кнопок, при нажатии которых появляются под кнопки
    wxButton* personnel = new wxButton(main_buttons, window::id_buttons::ID_PERSONAL, wxT("Персонал"), wxDefaultPosition, wxSize(121, 33));
    wxButton* equipment = new wxButton(main_buttons, window::id_buttons::ID_EQUIPMENT, wxT("Оборудование"), wxDefaultPosition, wxSize(121, 33));
    wxButton* contract = new wxButton(main_buttons, window::id_buttons::ID_CONTRACT, wxT("Договоры"), wxDefaultPosition, wxSize(121, 33));

    //расположение кнопок на панеле (задал как они должны распологаться)
    wxBoxSizer* main_buttons_sizer = new wxBoxSizer(wxVERTICAL);
    main_buttons_sizer->Add(personnel, 0, wxLEFT, 4);
    main_buttons_sizer->Add(equipment, 0, wxLEFT, 4);
    main_buttons_sizer->Add(contract, 0, wxLEFT, 4);
    main_buttons->SetSizer(main_buttons_sizer);

    //настройка скрол бара
    //если у панели будет меньше области, чем ей было задано, то появится верикальный скрол бар
    int height = 100;
    main_buttons->SetScrollbars(0, 2, 0, height/2);


    //инициализация панели, на которой находятся под кнопки. То есть те кноки, при нажатии которых,
    //будут появляться таблица
    table_buttons = new wxScrolledWindow(panel_buttons, wxID_ANY, wxDefaultPosition, wxSize(130, 100));
    table_buttons_sizer = new wxBoxSizer(wxVERTICAL);
    //Флаги - отображать ли данные кнопки или нет
    isPersonal = false;
    isEquipment = false;
    isContract = false;

    //инициализация панели, на которой находяться кнопки действий (Добавит, удали и тд)
    option_buttons = new wxScrolledWindow(panel_buttons, wxID_ANY, wxDefaultPosition, wxSize(380, 70));

    //Флаг - отображать ли эти кнопки
    isOptionButtons = false;

    option_buttons_sizer = new wxBoxSizer(wxVERTICAL);

    //инициализация кнопок действий
    wxBoxSizer* option_buttons_sizer_top = new wxBoxSizer(wxHORIZONTAL);
    button_delete = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_DELETE, 
        wxT("Удалить"), wxDefaultPosition, wxSize(121, 33));
    button_change = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_CHANGE, 
        wxT("Изменить"), wxDefaultPosition, wxSize(121, 33));
    button_add = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_ADD, 
        wxT("Добавить"), wxDefaultPosition, wxSize(121, 33));

    //задаются позиции кнопок на панели
    option_buttons_sizer_top->Add(button_delete, 0, wxLEFT, 4);
    option_buttons_sizer_top->Add(button_change, 0, wxLEFT, 4);
    option_buttons_sizer_top->Add(button_add, 0, wxLEFT | wxRIGHT, 4);

    wxBoxSizer* option_buttons_sizer_bottom = new wxBoxSizer(wxHORIZONTAL);
    wxButton* button_find = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_FIND,
        wxT("Поиск"), wxDefaultPosition, wxSize(121, 33));
    wxButton* button_filter = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_FILTER, 
        wxT("Фильтр"), wxDefaultPosition, wxSize(121, 33));
    wxButton* button_update = new wxButton(option_buttons, window::id_buttons_panel::ID_BUTTON_UPDATE, 
        wxT("Обновить"), wxDefaultPosition, wxSize(121, 33));

    option_buttons_sizer_bottom->Add(button_find, 0, wxLEFT | wxRIGHT, 4);
    option_buttons_sizer_bottom->Add(button_filter, 0, wxTOP);
    option_buttons_sizer_bottom->Add(button_update, 0, wxLEFT | wxRIGHT, 4);
    
    option_buttons_sizer->Add(option_buttons_sizer_top, 0, wxBOTTOM, 4);
    option_buttons_sizer->Add(option_buttons_sizer_bottom, 0);

    option_buttons->SetSizer(option_buttons_sizer);

    int height2 = 68;
    option_buttons->SetScrollbars(0, 2, 0, height2/2);
    //скрыть панель с кнопками действий
    option_buttons->Show(isOptionButtons);

    wxBoxSizer* panel1Sizer = new wxBoxSizer(wxHORIZONTAL);
    panel1Sizer->Add(main_buttons, 0, wxEXPAND);
    panel1Sizer->Add(table_buttons, 0, wxEXPAND);
    panel1Sizer->Add(option_buttons, 0, wxEXPAND);
    panel_buttons->SetSizer(panel1Sizer);

    //Тут указываются какие функции должны начать выполняться, когда ID (например кнопки) произошел какой либо ивент
    //(например бала нажата кнопка)
    Connect(window::id_buttons::ID_PERSONAL, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnPersonnel));
    Connect(window::id_buttons::ID_EQUIPMENT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnEquipment));
    Connect(window::id_buttons::ID_CONTRACT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnContract));

    Connect(window::id_buttons_panel::ID_BUTTON_UPDATE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnUpdatePage));
    Connect(window::id_buttons_panel::ID_BUTTON_DELETE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnDeletePage));
    Connect(window::id_buttons_panel::ID_BUTTON_FIND, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnSearchPage));
    Connect(window::id_buttons_panel::ID_BUTTON_FILTER, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnFilterPage));
    Connect(window::id_buttons_panel::ID_BUTTON_CHANGE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnChangePage));
    Connect(window::id_buttons_panel::ID_BUTTON_ADD, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnAddPage));
}

//отображения под кнопок панели Personnel, 
//а остальные подкнопки скрываются
void MainFrame::OnPersonnel(wxCommandEvent &event)
{
    //если отображались другие под кнопки, то спрятать их
    if(isEquipment || isContract)
    {
        free_table_buttons_sizer();
        isEquipment = false;
        isContract = false;
    }
    
    //если уже отображаются под кнопки Personnal, 
    //то ничего не делать
    if(isPersonal)
    {
        return;
    }
    else
    {
        //флаг того, что отображаются подкнопки Personnal
        isPersonal = true;

        //Отображение подкнопок Personnal 
        personnal->PersonnalSetSizer();
    }

    //Если будет нажата определенная кнопка (понимает по ID), то 
    //начнется выполнение указанной функции
    Connect(window::id_buttons::ID_STAFF, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnStaffM));
    Connect(window::id_buttons::ID_DEPARTMENTS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnDepartmentsM));
    Connect(window::id_buttons::ID_WGROUP, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnWGroupM));
}

//Аналогично Personnal
void MainFrame::OnEquipment(wxCommandEvent &event)
{
    if(isPersonal || isContract)
    {
        free_table_buttons_sizer();
        isPersonal = false;
        isContract = false;
    }
    
    if(isEquipment)
    {
        return;
    }
    else
    {
        isEquipment = true;

        equipment->EquipmentSetSizer();
    }

    Connect(window::id_buttons::ID_EQUIPMENT_TABLE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnEquipmentTableM));
    Connect(window::id_buttons::ID_EQUIPMENT_USED, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnEquipmentUsedM));
    Connect(window::id_buttons::ID_EQUIPMENT_EFFICIENCY, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnEquipmentEfficiencyM));
    
}

//Аналогично Personnal
void MainFrame::OnContract(wxCommandEvent &event)
{
    if(isPersonal || isEquipment)
    {
        free_table_buttons_sizer();
        isPersonal = false;
        isEquipment = false;
    }
    
    if(isContract)
    {
        return;
    }
    else
    {
        isContract = true;

        contract->ContractSetSizer();
    }

    Connect(window::id_buttons::ID_CUSTOMER, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnCustomerM));
    Connect(window::id_buttons::ID_CONTRACT_TABLE, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnContractTableM));
    Connect(window::id_buttons::ID_PROJECT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnProjectM));
    Connect(window::id_buttons::ID_CONTRACT_PROJECT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnContractProjectM));
    Connect(window::id_buttons::ID_SUBCONTR_ORG, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnSubcontrOrgM));
    Connect(window::id_buttons::ID_C_P_EFFICIENCY, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MainFrame::OnCPEfficiencyM));
}

//Очищение Sizer, в котором отображаются подкнопки
void MainFrame::free_table_buttons_sizer()
{
    table_buttons_sizer->Clear(true);
}

//Нажали на подкнопку Персонал
void MainFrame::OnStaffM(wxCommandEvent &event)
{
    //Проверяет подключение к БД
    //Если не были подключены, то пытается заного 
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {
        //Если не были отображены кнопки действий, то отображает их
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(true);
        button_delete->Show(true);
        panel_buttons->Layout();
        //Создание и Отображение Таблицы Персонал
        personnal->OnStaff(false);
        event.Skip();
    }
}
//Аналогично OnStaffM
void MainFrame::OnDepartmentsM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(true);
        button_delete->Show(true);
        panel_buttons->Layout();
        personnal->OnDepartments(false);
        event.Skip();
    }
}
//Аналогично OnStaffM
void MainFrame::OnWGroupM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(false);
        button_delete->Show(true);
        panel_buttons->Layout();
        personnal->OnWGroup(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnEquipmentTableM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(true);
        button_delete->Show(true);
        panel_buttons->Layout();
        equipment->OnEquipmentTable(false);
        event.Skip();
    }
}


//Аналогично OnStaffM
void MainFrame::OnEquipmentUsedM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        //Скрывает кнопки Добавить, Изменить, Удалить
        //После чего обнавляет панель, чтобы эти изменения отобразились на экране
        button_add->Show(false);
        button_change->Show(false);
        button_delete->Show(false);
        panel_buttons->Layout();
        equipment->OnEquipmentUsed(false);
        event.Skip();
    }
}


//Аналогично OnStaffM
void MainFrame::OnEquipmentEfficiencyM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(false);
        button_change->Show(false);
        button_delete->Show(false);
        panel_buttons->Layout();
        equipment->OnEquipmentEfficiency(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnCustomerM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(true);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnCustomer(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnContractTableM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(false);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnContractTable(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnProjectM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(false);
        button_change->Show(true);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnProject(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnContractProjectM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(false);
        button_change->Show(false);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnContractProject(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnSubcontrOrgM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }

        button_add->Show(true);
        button_change->Show(true);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnSubcontrOrg(false);
        event.Skip();
    }
}

//Аналогично OnStaffM
void MainFrame::OnCPEfficiencyM(wxCommandEvent &event)
{
    if(!isConnect_bd)
    {
        bd = new BD();
        if(!bd->isConnect)
        {
            error_bd = wxT("Connection to database failed: \n") + wxString(bd->error, wxConvUTF8);
            bd->exit_conn();
            ErrorConnectBD();
            isConnect_bd = false;
            return;
        }
        else
        {
            isConnect_bd = true;
        }
    }
    else
    {    
        if(!isOptionButtons)
        {
            isOptionButtons = true;
            option_buttons->Show(isOptionButtons);
        }
    
        button_add->Show(false);
        button_change->Show(false);
        button_delete->Show(false);
        panel_buttons->Layout();
        contract->OnCPEfficiency(false);
        event.Skip();
    }
}

//Если была нажата кнопка Обновить
void MainFrame::OnUpdatePage(wxCommandEvent &event)
{
    //Узнаем индекс страницы на которой сейчас находится пользователь
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    //по индексу узнаем название данной вкладки
    wxString page_text = nb->GetPageText(index_nb);
    //И по наззвании вкладкии находим таблицу, на которой сейчас находится пользователь
    //И обнавляем ее
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        //Номер под кнопки и файл с запросом данной таблицы
        personnal->OnUpdate(1, wxT("sql/staff/staff.sql"));
    }

    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnUpdateTwo(2, wxT("sql/departments/departments_1.sql"), wxT("sql/departments/departments_2.sql"));
    }

    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->OnUpdate(3, wxT("sql/work_group/work_group.sql"));
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnUpdate(1, wxT("sql/equipment_table/equipment_table.sql"));
    }

    if(page_text.Cmp(wxT("Исп Оборудование")) == 0)
    {
        equipment->OnUpdate(2, wxT("sql/equipment_used/equipment_used.sql"));
    }

    if(page_text.Cmp(wxT("Эффективность Об")) == 0)
    {
        equipment->OnUpdate(3, wxT("sql/equipment_efficiency/equipment_efficiency.sql"));
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->OnUpdate(1, wxT("sql/customer/customer.sql"));
    }

    if(page_text.Cmp(wxT("Договоры")) == 0)
    {
        contract->OnUpdate(2, wxT("sql/contract/contract.sql"));
    }

    if(page_text.Cmp(wxT("Проекты")) == 0)
    {
        contract->OnUpdate(3, wxT("sql/project/project.sql"));
    }

    if(page_text.Cmp(wxT("Договор-Проект")) == 0)
    {
        contract->OnUpdate(4, wxT("sql/contract-project/contract-project.sql"));
    }

    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->OnUpdate(5, wxT("sql/subcontr_org/subcontr_org.sql"));
    }

    if(page_text.Cmp(wxT("Эффективность Дог/Пр")) == 0)
    {
        contract->OnUpdateTwo(6, wxT("sql/C-P_efficiency/contract.sql"), wxT("sql/C-P_efficiency/project.sql"));
    }
}

//Аналогично с OnUpdatePage, только на сей раз была нажата кнопка Фильтр
//И для нужной таблицы откроется окно фильтра
void MainFrame::OnFilterPage(wxCommandEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->OnFilter(1);
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnFilter(2);
    }
    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->OnFilter(3);
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnFilter(1);
    }
    if(page_text.Cmp(wxT("Исп Оборудование")) == 0)
    {
        equipment->OnFilter(2);
    }
    if(page_text.Cmp(wxT("Эффективность Об")) == 0)
    {
        equipment->OnFilter(3);
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->OnFilter(1);
    }
    if(page_text.Cmp(wxT("Договоры")) == 0)
    {
        contract->OnFilter(2);
    }
    if(page_text.Cmp(wxT("Проекты")) == 0)
    {
        contract->OnFilter(3);
    }
    if(page_text.Cmp(wxT("Договор-Проект")) == 0)
    {
        contract->OnFilter(4);
    }
    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->OnFilter(5);
    }
    if(page_text.Cmp(wxT("Эффективность Дог/Пр")) == 0)
    {
        contract->OnFilter(6);
    }
}

//Аналогично с OnUpdatePage, только на сей раз была нажата кнопка Поиск
//И для нужной таблицы откроется окно поиска
void MainFrame::OnSearchPage(wxCommandEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->OnSearch(1);
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnSearch(2);
    }
    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->OnSearch(3);
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnSearch(1);
    }
    if(page_text.Cmp(wxT("Исп Оборудование")) == 0)
    {
        equipment->OnSearch(2);
    }
    if(page_text.Cmp(wxT("Эффективность Об")) == 0)
    {
        equipment->OnSearch(3);
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->OnSearch(1);
    }
    if(page_text.Cmp(wxT("Договоры")) == 0)
    {
        contract->OnSearch(2);
    }
    if(page_text.Cmp(wxT("Проекты")) == 0)
    {
        contract->OnSearch(3);
    }
    if(page_text.Cmp(wxT("Договор-Проект")) == 0)
    {
        contract->OnSearch(4);
    }
    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->OnSearch(5);
    }
    if(page_text.Cmp(wxT("Эффективность Дог/Пр")) == 0)
    {
        contract->OnSearch(6);
    }
}

//Аналогично с OnUpdatePage, только на сей раз была нажата кнопка Добавить
//И для нужной таблицы откроется окно добавить
void MainFrame::OnAddPage(wxCommandEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->OnAdd(1);
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnAdd(2);
    }
    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->OnAdd(3);
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnAdd(1);
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->OnAdd(1);
    }
    if(page_text.Cmp(wxT("Договоры")) == 0)
    {
        contract->OnAdd(2);
    }
    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->OnAdd(5);
    }
}

//Аналогично с OnUpdatePage, только на сей раз была нажата кнопка Изменить
//И для нужной таблицы откроется окно изменить
void MainFrame::OnChangePage(wxCommandEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->OnChange(1);
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnChange(2);
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnChange(1);
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->OnChange(1);
    }
    if(page_text.Cmp(wxT("Проекты")) == 0)
    {
        contract->OnChange(3);
    }
    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->OnChange(5);
    }
}

//Аналогично с OnUpdatePage, только на сей раз была нажата кнопка Удалить
//И после чего удалятся те строчки, которые были выделены
void MainFrame::OnDeletePage(wxCommandEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->OnDelete(1);
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->OnDelete(2);
    }
    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->OnDelete(3);
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->OnDelete(1);
    }
}

//Был нажат крести у вкладки. Закрывается вкладка,
//попутно помечая, что данная таблица теперь закрыта
void MainFrame::OnClosePage(wxAuiNotebookEvent &event)
{
    int index_nb = nb->GetPageIndex(nb->GetCurrentPage());
    wxString page_text = nb->GetPageText(index_nb);
    if(page_text.Cmp(wxT("Сотрудники")) == 0)
    {
        personnal->isonstaff = false;
    }
    if(page_text.Cmp(wxT("Отделы")) == 0)
    {
        personnal->isondepartments = false;
    }
    if(page_text.Cmp(wxT("Рабочие Группы")) == 0)
    {
        personnal->isonwork_group = false;
    }

    if(page_text.Cmp(wxT("Оборудование")) == 0)
    {
        equipment->isonequipment_table = false;
    }
    if(page_text.Cmp(wxT("Исп Оборудование")) == 0)
    {
        equipment->isonequipment_used = false;
    }
    if(page_text.Cmp(wxT("Эффективность Об")) == 0)
    {
        equipment->isonequipment_efficiency = false;
    }

    if(page_text.Cmp(wxT("Заказчики")) == 0)
    {
        contract->isoncustomer = false;
    }
    if(page_text.Cmp(wxT("Договоры")) == 0)
    {
        contract->isoncontract_table = false;
    }
    if(page_text.Cmp(wxT("Проекты")) == 0)
    {
        contract->isonproject = false;
    }
    if(page_text.Cmp(wxT("Договор-Проект")) == 0)
    {
        contract->isoncontract_project = false;
    }
    if(page_text.Cmp(wxT("Субподрятные Организации")) == 0)
    {
        contract->isonsubcontr_org = false;
    }
    if(page_text.Cmp(wxT("Эффективность Дог/Пр")) == 0)
    {
        contract->isonc_p_efficiency = false;
    }

    int count_page = nb->GetPageCount();

    if(count_page == 1)
    {
        isOptionButtons = false;
        option_buttons->Show(isOptionButtons);
    }
}

//Сообщение об ошибка подключении к БД
void MainFrame::ErrorConnectBD()
{
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        error_bd, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

//Другой способ подключения эвентов. Более старый и статический.
//Его использовал только для эвента закрытия вкладки, потому что 
//С обычным Connect не хочет работать
//Не знаю почему 
wxBEGIN_EVENT_TABLE(MainFrame, wxFrame)
    EVT_AUINOTEBOOK_PAGE_CLOSE(window::panels::ID_AUINoteBook, MainFrame::OnClosePage)
wxEND_EVENT_TABLE()