#ifndef exel_h
#define exel_h

#include <wx/wx.h>
#include <wx/grid.h>
#include <wx/textfile.h>

#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <fstream>

#include "BD.h"
#include "ExtraStaff.h"
#include "ExtraWGroup.h"

struct TableColumn
{
    wxString name_column;
    bool iscolumn_show;
    int number_col;
};

struct NumberColumn
{
    wxString name_sql_file;
    wxString name_column;
    int number_column;
};


class Exel : public wxGrid
{
public:
    Exel(wxAuiNotebook* nb);
    void GetTable(BD* bd, const char* sql, bool isupdate);
    void GetTableDepartments(BD* bd, const char* sql, bool isupdate);
    void GetTableCPEff(BD* bd, const char* sql, bool isupdate);
    void OnSort(wxGridEvent &event);
    void ExtraMenu(wxGridEvent &event);

    void OnUpdateTable(BD* bd, const char* sql);
    void OnUpdateTableDepartments(BD* bd, const char* sql);
    void OnUpdateTableCPEff(BD* bd, const char* sql);

    int selected_col = -1;

    bool is_read_only;

    BD* bd;
};

#endif