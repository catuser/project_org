#include "ContractOkAdd.h"

ContractOkAdd::ContractOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, std::vector<wxComboBox*> vector_combobox_contract_main, 
            wxScrolledWindow* scrolled_main)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;
    vector_combobox_contract = vector_combobox_contract_main;
    scrolled = scrolled_main;
    is_ok = true;

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }
    select_result.push_back(result);
    result.Clear();

    if(!CheckInput(1, 0, select_result, vector_combobox))
    {
        ErrorSQL(wxT("Неправильный ввод"));
        vector_combobox[1]->SetSelection(-1, -1);
        int y_scroll = 2;
        scrolled->Scroll(0, y_scroll);
        is_ok = false;
        return;
    }

    if(!GetSQL(set_sql[1], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }
    result.Add(wxT("new"));
    result.Add(wxT("New"));
    result.Add(wxT("NEW"));
    select_result.push_back(result);
    result.Clear();

    for(int i = 2; i < vector_combobox.size(); i++)
    {
        if(!CheckInput(i, 1, select_result, vector_combobox))
        {
            ErrorSQL(wxT("Неправильный ввод"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 2) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            is_ok = false;
            return;
        }
    }

    int number_uuid = 1;
    GetAndCheckUuid(table, 1, vector_uuid_contract, number_uuid);

    wxAuiNotebook *nb = new wxAuiNotebook(scrolled, wxID_ANY);
    uuid_project = new Exel(nb);

    GetTableSQL();

    GetNewProject();

    number_uuid = insert_project.size() / 2;
    GetAndCheckUuid(uuid_project, 0, vector_uuid_project, number_uuid);

    wxString combobox_name_customer = vector_combobox[1]->GetValue();
    if(!GetWhereSQL(set_sql[3], &result, combobox_name_customer, bd, true))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        is_ok = true;
        return;
    }

    vector_combobox_contract[1]->SetValue(result[0]);
    result.Clear();

    std::vector<wxComboBox*> insert_contract;
    for(int i = 0; i < 2; i++)
    {
        insert_contract.push_back(vector_combobox_contract[i]);
    }

    int size_vector_name_project = vector_name_project.size();
    for(int i = 0; i < size_vector_name_project; i++)
    {
        if(!GetWhereSQL(set_sql[4], &result, vector_name_project[i], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            is_ok = true;
            return;
        }
    }

    int size_result = result.GetCount();
    std::vector<wxString> uuid_contract_project;
    wxString uuid_contract_string = vector_uuid_contract[0];
    for(int i = 0; i < size_result; i++)
    {
        uuid_contract_project.push_back(uuid_contract_string);
        insert_contact_project.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, result[i]) );
        insert_contact_project[i]->Hide();
    }

    for(int i = 0; i < vector_uuid_project.size(); i++)
    {
        wxString string_uuid_project = vector_uuid_project[i];
        uuid_contract_project.push_back(uuid_contract_string);
        insert_contact_project.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, string_uuid_project) );
        insert_contact_project[i + size_result]->Hide();
    }
    result.Clear();
    
    if( !InsertContract(vector_uuid_contract, insert_contract, 3, 5) )
        return;
    if(insert_project.size() > 0)
    {
        if( !InsertContract(vector_uuid_project, insert_project, 3, 6) )
            return;
    }
    else
    {
        wxString new_result = wxT("t");
        if(!UpdateSQL(set_sql[8], new_result, vector_uuid_contract[0], bd))
            return;
    }
    
    if( !InsertContract(uuid_contract_project, insert_contact_project, 2, 7) )
        return;

    PGresult* res = bd->request("COMMIT;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }

    PQclear(res);
    
}

void ContractOkAdd::GetTableSQL()
{
    wxTextFile sql_file(set_sql[2]);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();
    const char* ch_sql = sql.mb_str(wxConvUTF8);
    uuid_project->GetTable(bd, ch_sql, false);
}

bool ContractOkAdd::InsertContract(std::vector<wxString> vector_uuid, std::vector<wxComboBox*> vector_combobox, 
    int count_cols, int number_sql_file)
{
    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    if(!InsertSQL(vector_uuid, insert_uuid, vector_combobox, count_cols, set_sql[number_sql_file], bd) )
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Повторите попытку попозже"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    return true;
}

void ContractOkAdd::GetNewProject()
{
    int size_project = vector_combobox_contract.size();
    int index_hide = 0;
    wxDateTime now = wxDateTime::Now();
    wxString old_name_contract = wxT("");
    wxString string_uuid_contract = vector_uuid_contract[0];

    for(int i = 2; i < size_project; i+=2)
    {
        wxString name_project = vector_combobox_contract[i]->GetValue();
        if (name_project.Cmp(wxT("NEW")) == 0 || name_project.Cmp(wxT("New")) == 0 ||
            name_project.Cmp(wxT("new")) == 0)
        {
            now = wxDateTime::Now();
            wxString name_contract = wxT("П") + now.Format(wxT("%d%m%C")) + wxT("/")
                + now.Format(wxT("%H%M%S"));
            if(name_contract.Cmp(old_name_contract) == 0)
            {
                wxSleep(1);
                now = wxDateTime::Now();
                name_contract = wxT("П") + now.Format(wxT("%d%m%C")) + wxT("/")
                    + now.Format(wxT("%H%M%S"));
            }
            old_name_contract = name_contract;
            insert_project.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, name_contract));
            insert_project[index_hide]->Hide();
            index_hide += 2;
            insert_project.push_back(vector_combobox_contract[i+1]);
        }
        else
        {
            wxString add_vector_name_project = vector_combobox_contract[i]->GetValue();
            vector_name_project.push_back(add_vector_name_project);
        }
        
    }
}