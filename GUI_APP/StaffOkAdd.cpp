#include "StaffOkAdd.h"

StaffOkAdd::StaffOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, std::vector<wxComboBox*> vector_combobox_main)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;

    int number_uuid = vector_combobox.size() / 5;
    GetAndCheckUuid(table, 0, vector_uuid, number_uuid);

    for(int i = 1; i <= number_uuid; i++)
    {
        int index_vector_combobox = (i * 5) - 1;
        wxString profession = vector_combobox[index_vector_combobox]->GetValue();
        if(profession.Cmp(wxT("Лаборант")) == 0)
        {
            vector_lab.push_back(vector_uuid[i - 1]);
        }

        if(profession.Cmp(wxT("Техник")) == 0)
        {
            vector_tech.push_back(vector_uuid[i - 1]);
        }

        if(profession.Cmp(wxT("Обслуживающий Персонал")) == 0)
        {
            vector_service.push_back(vector_uuid[i - 1]);
        }

        if(profession.Cmp(wxT("Конструктор")) == 0)
        {
            vector_constructor.push_back(vector_uuid[i - 1]);
        }

        if(profession.Cmp(wxT("Инженер")) == 0)
        {
            vector_engineer.push_back(vector_uuid[i - 1]);
        }
    }

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    if(!InsertSQL(vector_uuid, insert_uuid, vector_combobox, 6, set_sql[0], bd) )
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Повторите попытку попозже"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return;
    }
    
    for(int i = 1; i < set_sql.size(); i++)
    {
        bool is_free_vector = false;
        std::vector<wxString> insert_vector_uuid;
        switch (i)
        {
        case 1:
        {
            if(vector_constructor.size() == 0 && vector_engineer.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            for (int i = 0; i < vector_constructor.size(); i++)
                insert_vector_uuid.push_back(vector_constructor[i]);
            for (int i = 0; i < vector_engineer.size(); i++)
                insert_vector_uuid.push_back(vector_engineer[i]);

            break;
        }
        case 2:
        {
            if(vector_lab.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            insert_vector_uuid = vector_lab;

            break;
        }
        case 3:
        {
            if(vector_tech.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            insert_vector_uuid = vector_tech;

            break;
        }
        case 4:
        {
            if(vector_service.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            insert_vector_uuid = vector_service;

            break;
        }
        case 5:
        {
            if(vector_constructor.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            insert_vector_uuid = vector_constructor;
            break;
        }
        case 6:
        {
            if(vector_engineer.size() == 0)
            {
                is_free_vector = true;
                break;
            }
            insert_vector_uuid = vector_engineer;

            break;
        }
        
        default:
            break;
        }

        if(is_free_vector)
            continue;

        if(!InsertOnlyUUIDSQL(insert_vector_uuid, 1, set_sql[i], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
    }

    PGresult* res = bd->request("COMMIT;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }
}