#include "BD.h"

BD::BD()
{
    conn = PQconnectdb("user=postgres password=mysecretpassword host=localhost port=54325 dbname=project_org");
    if (PQstatus(conn) != CONNECTION_OK)
    {
        error = PQerrorMessage(conn);
        isConnect = false;
    }
    else 
        isConnect = true;
}

PGresult* BD::request(const char* sql)
{
    PGresult* res = NULL;
    res = PQexec(conn, sql);
    
    return res;
}

void BD::error_res(PGresult* res)
{
    wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        str, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

void BD::exit_conn()
{
    PQfinish(conn);
}

PGresult* BD::OnUpdate(const char* sql)
{
    PGresult* res = NULL;
    res = PQexec(conn, sql);
    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        error_res(res);
    }
    return res;
    
}