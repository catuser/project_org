#include "CustomerOkAdd.h"

CustomerOkAdd::CustomerOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
    std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;
    is_ok = true;

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }
    select_result.push_back(result);
    result.Clear();

    for(int i = 0; i < vector_combobox.size(); i+=3)
    {
        if(CheckInput(i, 0, select_result, vector_combobox))
        {
            ErrorSQL(wxT("Неправильный ввод"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 2) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            is_ok = false;
            return;
        }
    }

    int number_uuid = vector_combobox.size() / 3;
    GetAndCheckUuid(table, 0, vector_uuid, number_uuid);

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    if(!InsertSQL(vector_uuid, insert_uuid, vector_combobox, 4, set_sql[1], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }
}