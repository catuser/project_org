#include "Exel.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"

class StaffOkAdd
{
public:
    StaffOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, std::vector<wxComboBox*> vector_combobox_main);

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;

    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxString> vector_uuid;
    std::vector<wxString> vector_lab;
    std::vector<wxString> vector_tech;
    std::vector<wxString> vector_service;
    std::vector<wxString> vector_constructor;
    std::vector<wxString> vector_engineer;
};