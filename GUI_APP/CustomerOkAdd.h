#include "Exel.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class CustomerOkAdd
{
public:
    CustomerOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled);

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;

    bool is_ok;
    wxArrayString result;

    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxString> vector_uuid;
    std::vector<wxArrayString> select_result;
};