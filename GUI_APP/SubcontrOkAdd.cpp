#include "SubcontrOkAdd.h"

SubcontrOkAdd::SubcontrOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, std::vector<wxComboBox*> vector_combobox_subcontr_main, 
            std::vector<wxDatePickerCtrl*> vector_date_main, wxScrolledWindow* scrolled_main)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;
    vector_combobox_subcontr = vector_combobox_subcontr_main;
    vector_date = vector_date_main;
    scrolled = scrolled_main;
    is_ok = true;

    if(!CheckData())
    {
        ErrorSQL(wxT("Неправильный ввод даты"));
        is_ok = false;
        return;
    }

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }
    select_result.push_back(result);
    result.Clear();
    
    for(int i = 3; i < vector_combobox_subcontr.size(); i+=6)
    {
        if(!CheckInput(i, 0, select_result, vector_combobox_subcontr))
        {
            ErrorSQL(wxT("Неправильно введено название проекта"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 6) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            is_ok = false;
            return;
        }
    }

    int count_lines = vector_combobox_subcontr.size() / 6;
    if(!CheckRepeatProject(count_lines, 3, 4, vector_combobox, scrolled))
    {
        ErrorSQL(wxT("Нельзя отдать один проект двум организациям"));
        is_ok = false;
        return;
    }

    int count_cols = 6;
    int number_col = 3;
    if(!ReplaceNameId(count_cols, vector_combobox_subcontr, number_col, set_sql[1], bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }

    int count_vector = 3;
    wxArrayString info_contract;
    for(int i = 0; i < count_lines; i++)
    {
        wxString uuid_project = vector_combobox_subcontr[count_vector]->GetValue();
        if(!GetWhereSQL(set_sql[7], &info_contract, uuid_project, bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            is_ok = true;
            return;
        }
        count_vector += 6;

        int count_result = info_contract.GetCount() - 1;
        if(!GetWhereSQL(set_sql[8], &info_contract, info_contract[count_result], bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            is_ok = true;
            return;
        }
    }

    if(!CheckReapeatName())
    {
        return;
    }

    std::vector<int> insert_uuid;
    insert_uuid.push_back(9);

    if(!InsertSQL(vector_uuid, insert_uuid, insert_subcontr, 6, set_sql[3], bd) )
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Повторите попытку попозже"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return;
    }

    int number_lines = vector_combobox_subcontr.size() / 6;
    int index_id_project = 3;
    int insert_end_work = 4;
    wxDateTime now = wxDateTime::Now();
    wxString start_work = now.Format(wxT("%Y")) + wxT("-") + now.Format(wxT("%m")) + wxT("-") + 
                    now.Format(wxT("%d"));
    
    for(int i = 0; i < number_lines; i++)
    {
        wxString id_project = vector_combobox_subcontr[index_id_project]->GetValue();
        if(!UpdateSQL(set_sql[4], start_work, id_project, bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
        index_id_project += 6;

        wxString string_end_work = vector_combobox_subcontr[insert_end_work]->GetValue();
        insert_end_work += 6;
        if(!UpdateSQL(set_sql[5], string_end_work, id_project, bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }

        if(!UpdateSQL(set_sql[6], wxT("true"), id_project, bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
    }

    for(int i = 0; i < info_contract.GetCount(); i+=2)
    {
        int number_null_start_time_project = wxAtoi(info_contract[i + 1]);
        if(number_null_start_time_project == 1)
        {
            if(!UpdateSQL(set_sql[9], wxT("true"), info_contract[i], bd) )
            {
                ErrorSQL(wxT("Повторите попытку попозже"));
                return;
            }
        }
    }

    PGresult* res = bd->request("COMMIT;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }
}

bool SubcontrOkAdd::CheckData()
{
    int count_lines = vector_combobox_subcontr.size() / 6;
    int index_select = 0;
    for(int i = 0; i < count_lines; i++)
    {
        wxDateTime compare_date = wxDateTime::Now();
        wxDateTime enter_date = vector_date[i]->GetValue();

        if(enter_date.IsLaterThan(compare_date))
        {
            return true;
        }
        else
        {
            if(enter_date.IsSameDate(compare_date))
            {
                return true;
            }

            vector_combobox[index_select]->SetSelection(-1, -1);
            vector_combobox[index_select + 1]->SetSelection(-1, -1);
            vector_combobox[index_select + 2]->SetSelection(-1, -1);
            vector_combobox[index_select + 3]->SetSelection(-1, -1);
            index_select += 4;
            int y_scroll = (i * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            return false;
        }
        
    }
    return false;
}

bool SubcontrOkAdd::CheckReapeatName()
{
    int count_lines = vector_combobox_subcontr.size() / 6;
    int i = 0;
    int index_first = 0;
    int index_result = 0;
    int how_filling = 3;
    int number_rows = table->GetNumberRows();
    while(i < count_lines)
    {
        wxString name_project = vector_combobox[index_first]->GetValue();
        for(int j = 0; j < number_rows; j++)
        {
            wxString compare_name_project = table->GetCellValue(j, 2);
            if(name_project.Cmp(compare_name_project) == 0)
            {
                how_filling = 1;
                break;
            }
        }

        int index_second = 0;
        int index_j = 0;
        for(; index_j < i; index_j++)
        {
            wxString compare_name_project = vector_combobox[index_second]->GetValue();

            if(name_project.Cmp(compare_name_project) == 0)
            {
                how_filling = 2;
                break;
            }

            index_second += 4;
        }

        if(!FillingInsertVector(how_filling, i, index_j, name_project))
        {
            return false;
        }        

        i++;
        index_first += 4;
    }
    result.Clear();
    return true;
}

bool SubcontrOkAdd::FillingInsertVector(int how_filling, int index_i, int index_j, wxString name_project)
{
    int count_cols = 6;
    int index_insert = index_i * count_cols;
    switch(how_filling)
    {
        case 1:
        {
            if(!GetWhereSQL(set_sql[2], &result, name_project, bd, true))
            {
                ErrorSQL(wxT("Попробуйте снова попозже"));
                return false;
            }
            int index_result = result.GetCount() - 1;
            insert_subcontr.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, result[index_result]) );
            for(int index_vector = 0; index_vector < count_cols; index_vector++)
            {
                if(index_vector == 4)
                    continue;
                insert_subcontr.push_back(vector_combobox_subcontr[index_insert + index_vector]);
            }
            break;
        }
        case 2:
        {
            int index_uuid = index_j * count_cols;
            wxString uuid_from_insert = insert_subcontr[index_uuid]->GetValue();

            insert_subcontr.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, uuid_from_insert) );
            for(int index_vector = 0; index_vector < count_cols; index_vector++)
            {
                if(index_vector == 4)
                    continue;
                insert_subcontr.push_back(vector_combobox_subcontr[index_insert + index_vector]);
            }
            break;
        }
        case 3:
        {
            int number_uuid = 1;
            GetAndCheckUuid(table, 1, vector_uuid, number_uuid);
            int index_vector_uuid = vector_uuid.size() - 1;
            insert_subcontr.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, vector_uuid[index_vector_uuid]) );
            for(int index_vector = 0; index_vector < count_cols; index_vector++)
            {
                if(index_vector == 4)
                    continue;
                insert_subcontr.push_back(vector_combobox_subcontr[index_insert + index_vector]);
            }
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Произошел сбой"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            return false;
        }
    }

    return true;
}