#include "ExtraStaff.h"

ExtraStaff::ExtraStaff(wxString id_staff_main, wxString name_staff_main, wxString profession_main, BD* bd_main)
    : wxDialog(NULL, -1, wxT("Дополнительная информация"), wxDefaultPosition, wxSize(640, 160))
{
    id_staff = id_staff_main;
    name_staff = name_staff_main;
    profession = profession_main;
    bd = bd_main;
    is_ok = true;

    scrolled = new wxScrolledWindow(this); 

    button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    button_plus = new wxButton(this, window::id_buttons::ID_PLUS, wxT("+"));
    button_minus = new wxButton(this, window::id_buttons::ID_MINUS, wxT("-"));
    button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    sizer = new wxBoxSizer(wxVERTICAL);
    sizer_buttons = new wxBoxSizer(wxHORIZONTAL);

    enter_info = new wxTextCtrl(this, window::id_buttons::ID_TEXT, wxEmptyString, wxDefaultPosition, wxSize(300, -1));
    enter_info->Hide();
    count_evidence = new wxSpinCtrl(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300, -1));
    count_evidence->Hide();

    if(profession.Cmp(wxT("Техник")) == 0)
    {
        if(!YesTech())
        {
            is_ok = false;
            return;
        }
    }
    else
    {
        scrolled->Show(false);
        button_plus->Show(false);
        button_minus->Show(false);

        if(!NotTech())
        {
            is_ok = false;
            return;
        }
    }
    
    Connect(window::id_buttons::ID_PLUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraStaff::AddNewLine));
    Connect(window::id_buttons::ID_MINUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraStaff::DeleteNewLine));
    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(ExtraStaff::OnOk));
}

bool ExtraStaff::YesTech()
{
    text_name_staff = new wxTextCtrl(scrolled, window::id_buttons::ID_TEXT, name_staff, wxDefaultPosition, wxSize(300, -1), wxTE_READONLY);

    grid_sizer = new wxGridSizer(2, 5, 10);
    grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, wxT("ФИО")), 0 , wxALIGN_CENTER);
    grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, wxT("Название")), 0 , wxALIGN_CENTER);
    grid_sizer->Add(text_name_staff, 0, wxALIGN_CENTER);

    wxArrayString get_sql_files;

    wxTextFile sql_file(wxT("sql/staff/extra/tech/get_extra.txt"));
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        get_sql_files.Add(sql);
        sql = sql_file.GetNextLine();
    }
    sql_file.Close();

    if(!GetWhereSQL(get_sql_files[0], &id_result, id_staff, bd, true))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    count_id_result = id_result.GetCount();
    for(int i = 0; i < count_id_result; i++)
    {
        if(!GetWhereSQL(get_sql_files[1], &name_result, id_result[i], bd, true))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return false;
        }
    }

    wxArrayString is_dont_service;

    if(!GetSQL(get_sql_files[2], &is_dont_service, bd))
    {
        ErrorSQL(wxT("Повторите попытку позже"));
        return false;
    }

    number_dont_service = wxAtoi(is_dont_service[0]);
    if(number_dont_service != 0)
    {
        if(!GetSQL(get_sql_files[3], &name_result, bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return false;
        }
    }

    for(int i = 0; i < count_id_result; i++)
    {
        if(i != 0)
        {
            grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, wxT("")), 1 , wxALIGN_CENTER);
        }
        int size_vector = vector_name_equip.size();
        vector_name_equip.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, name_result[i], wxDefaultPosition, wxSize(300, -1)) );
        vector_name_equip[size_vector]->Append(name_result);
        vector_name_equip[size_vector]->AutoComplete(name_result);

        grid_sizer->Add(vector_name_equip[size_vector], 0, wxALL | wxALIGN_CENTER);
    }

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10); 

    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_minus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(button_plus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);
    
    sizer->Add(scrolled, 1, wxEXPAND);
    sizer->Add(sizer_buttons, 0, wxEXPAND | wxTOP, 5);
    SetSizer(sizer);

    GetSizeMyExtra();

    is_dont_service.Clear();
    return true;
}

bool ExtraStaff::NotTech()
{
    text_name_staff = new wxTextCtrl(this, window::id_buttons::ID_TEXT, name_staff, wxDefaultPosition, wxSize(300, -1), wxTE_READONLY);

    int number_profession = WhatProfession();
    if(number_profession == 4)
    {
        ErrorSQL(wxT("Как возможна эта ошибка?"));
        return false;
    }
    bool is_sql;

    grid_sizer = new wxGridSizer(2, 2, 5, 10);
    grid_sizer->Add(new wxStaticText(this, wxID_ANY, wxT("ФИО")), 0 , wxALIGN_CENTER);

    wxString sql_file_name;
    switch (number_profession)
    {
        case 0:
        {
            sql_file_name = wxT("sql/staff/extra/lab/get_extra.sql");
            break;
        }
        case 1:
        {
            sql_file_name = wxT("sql/staff/extra/service/get_extra.sql");
            break;
        }
        case 2:
        {
            sql_file_name = wxT("sql/staff/extra/constructor/get_extra.sql");
            break;
        }
        case 3:
        {
            sql_file_name = wxT("sql/staff/extra/engineer/get_extra.sql");
            break;
        }
        default:
            break;
    }

    wxTextFile sql_file(sql_file_name);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    wxString error_string = wxT("ERROR");
    int size_error_string = error_string.Len();
    wxString check_error = sql.Left(size_error_string);
    if(check_error.Cmp(error_string) == 0)
    {
        is_sql = false;
    }
    else
    {
        is_sql = true;
    }

    if(is_sql)
    {
        if(!TrueSQL(sql, number_profession))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return false;
        }
    }
    else
    {
        FalseSQL();
    }

    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    sizer->Add(grid_sizer, 0, wxEXPAND | wxRIGHT | wxLEFT, 5);
    sizer->Add(new wxStaticText(this, wxID_ANY, wxT("")), 1);
    sizer->Add(sizer_buttons, 0, wxEXPAND, 5);
	SetSizer(sizer);

    return true;    
}

int ExtraStaff::WhatProfession()
{
    if(profession.Cmp(wxT("Лаборант")) == 0)
        return 0;
    
    if(profession.Cmp(wxT("Обслуживающий Персонал")) == 0)
        return 1;
    
    if(profession.Cmp(wxT("Конструктор")) == 0)
        return 2;
    
    if(profession.Cmp(wxT("Инженер")) == 0)
        return 3;
    
    return 4;
}

void ExtraStaff::FalseSQL()
{
    button_ok->Show(false);
    grid_sizer->Add(new wxStaticText(this, wxID_ANY, wxT("")), 1 , wxALIGN_CENTER);
    grid_sizer->Add(text_name_staff, 0, wxALIGN_CENTER);
    grid_sizer->Add(new wxStaticText(this, wxID_ANY, wxT("")), 1 , wxALIGN_CENTER);
}

bool ExtraStaff::TrueSQL(wxString sql, int number_profession)
{
    int pos = sql.Find(wxT("WHERE"));
    wxString select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&select_sql, pos, &sql, id_staff);
    }
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    PGresult* res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        PQclear(res);
        return false;
    }

    wxString name_col(PQfname(res, 0), wxConvUTF8);
    wxString value_col(PQgetvalue(res, 0, 0), wxConvUTF8);

    grid_sizer->Add(new wxStaticText(this, wxID_ANY, name_col), 0 , wxALIGN_CENTER);
    grid_sizer->Add(text_name_staff, 0, wxALIGN_CENTER);

    if(number_profession == 2)
    {
        count_evidence->SetValue(value_col);
        count_evidence->Show(true);
        grid_sizer->Add(count_evidence, 0, wxALIGN_CENTER);
    }
    else
    {
        enter_info->SetValue(value_col);
        enter_info->Show(true);
        grid_sizer->Add(enter_info, 0, wxALIGN_CENTER);
    }
    

    return true;
}

void ExtraStaff::GetSizeMyExtra()
{
    int height = vector_name_equip.size();

    if(height <= 5 && height > 0)
    {
        this->SetClientSize(640, 100 + height * 30);
    }
    if(height > 5)
    {
        this->SetClientSize(640, 250);
    }
}

void ExtraStaff::AddNewLine(wxCommandEvent &event)
{
    if(number_dont_service == 0)
    {
        ErrorSQL(wxT("Всё оборудование обслуживается"));
        return;
    }

    int count_vector = vector_name_equip.size();
    if(count_vector != 0)
    {
        grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, wxT("")), 1 , wxALIGN_CENTER);
    }
    
    vector_name_equip.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
    vector_name_equip[count_vector]->Append(name_result);
    vector_name_equip[count_vector]->AutoComplete(name_result);

    grid_sizer->Add(vector_name_equip[count_vector], 0, wxALL | wxALIGN_CENTER);

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10); 
    
    GetSizeMyExtra();
    this->Layout();
}

void ExtraStaff::DeleteNewLine(wxCommandEvent &event)
{
    int count_vector = vector_name_equip.size();
    int index_delete = (count_vector * 2) + 2;
    int count_delete = 2;
    if(count_vector == 0)
    {
        ErrorSQL(wxT("Всё обслуживающее оборудование удалено"));
        return;
    }

    if(count_id_result == count_vector)
    {
        count_id_result--;
    }

    if(count_vector == 1)
    {
        count_delete = 1;
    }

    for(int i = 0; i < count_delete; i++)
    {
        index_delete--;
        grid_sizer->Remove(index_delete);
    }
    
    vector_name_equip[count_vector - 1]->Destroy();
    vector_name_equip.pop_back();

    GetSizeMyExtra();
    this->Layout();
}

void ExtraStaff::OnOk(wxCommandEvent &event)
{
    wxArrayString set_sql_files;
    wxTextFile sql_file(wxT("sql/staff/extra/set_extra.txt"));
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        set_sql_files.Add(sql);
        sql = sql_file.GetNextLine();
    }
    sql_file.Close();

    if(profession.Cmp(wxT("Техник")) == 0)
    {
        if(!TechOk(set_sql_files))
        {
            return;
        }
    }
    else
    {
        if(!NotTechOk(set_sql_files))
        {
            return;
        }
    }

    Close();
}

bool ExtraStaff::NotTechOk(wxArrayString set_sql_files)
{
    int number_profession = WhatProfession();
    if(number_profession == 4)
    {
        ErrorSQL(wxT("Как возможна эта ошибка?"));
        return false;
    }

    wxString set_value;
    if(number_profession == 2)
    {
        set_value << count_evidence->GetValue();
    }
    else
    {
        set_value = enter_info->GetValue();
    }

    wxString name_sql_file;
    switch (number_profession)
    {
        case 0:
        {
            name_sql_file = set_sql_files[0];
            break;
        }
        case 1:
        {
            name_sql_file = set_sql_files[1];
            break;
        }
        case 2:
        {
            name_sql_file = set_sql_files[2];
            break;
        }
        case 3:
        {
            name_sql_file = set_sql_files[3];
            break;
        }
    
        default:
            break;
    }

    if(!UpdateSQL(name_sql_file, set_value, id_staff, bd))
    {
        ErrorSQL(wxT("Не получилось обновить данные\nПопробуйте снова позже"));
        return false;
    }
    return true;
}

bool ExtraStaff::TechOk(wxArrayString set_sql_files)
{
    int count_lines = vector_name_equip.size();
    if(!CheckRepeatProject(count_lines, 0, 1, vector_name_equip, scrolled))
    {
        ErrorSQL(wxT("Нельзя дважды добавить одно оборудование"));
        return false;
    }

    if(!ReplaceNameId(1, vector_name_equip, 0, set_sql_files[4], bd))
    {
        ErrorSQL(wxT("Попробуйте снова позже"));
        return true;
    }

    PGresult* res = bd->request("BEGIN;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }
    PQclear(res);

    int fix_count_id_result = id_result.GetCount();

    wxArrayString plug;
    if(fix_count_id_result != count_id_result)
    {
        for(int i = count_id_result; i < fix_count_id_result; i++)
        {
            if(!GetWhereSQL(set_sql_files[5], &plug, id_result[i], bd, false))
            {
                ErrorSQL(wxT("Попробуйте снова позже"));
                return true;
            }

            if(!UpdateSQL(set_sql_files[8], wxT("false"), id_result[i], bd))
            {
                ErrorSQL(wxT("Попробуйте снова позже"));
                return true;
            }
        }
    }

    for(int i = 0; i < count_id_result; i++)
    {
        wxString new_result = vector_name_equip[i]->GetValue();
        if(!UpdateSQL(set_sql_files[6], new_result, id_result[i], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return true;
        }
    }

    std::vector<wxString> vector_uuid;
    std::vector<wxComboBox*> vector_insert;
    for(int i = count_id_result; i < vector_name_equip.size(); i++)
    {
        vector_uuid.push_back(id_staff);
        vector_insert.push_back(vector_name_equip[i]);
    }

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    int is_insert = count_lines - count_id_result;
    if(is_insert != 0)
    {
        if(!InsertSQL(vector_uuid, insert_uuid, vector_insert, 2, set_sql_files[7], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return true;
        }

        for(int i = 0; i < vector_insert.size(); i++)
        {
            wxString update_id = vector_insert[i]->GetValue();
            if(!UpdateSQL(set_sql_files[8], wxT("true"), update_id, bd))
            {
                ErrorSQL(wxT("Попробуйте снова позже"));
                return true;
            }
        }
    }

    PGresult* res2 = bd->request("COMMIT;");
    if(PQresultStatus(res2) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res2), wxConvUTF8);
        ErrorSQL(str);
        res2 = bd->request("ROLLBACK;");
        PQclear(res2);
    }
    PQclear(res2);

    return true;
}