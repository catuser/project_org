#include "main.h"
#include "MainFrame.h"

IMPLEMENT_APP(Buid_Org)

bool Buid_Org::OnInit()
{
    MainFrame *mainframe = new MainFrame(wxT("BD"));
    mainframe->Show(true);
    return true;
}