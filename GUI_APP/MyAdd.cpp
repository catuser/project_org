#include "MyAdd.h"

MyAdd::MyAdd(wxString get_add_main, Exel* add_table_main, wxString name_table_main, BD* bd_main, bool* is_ok)
    : wxDialog(NULL, -1, wxT("Добавить"), wxDefaultPosition, wxDefaultSize)
{
    get_add = get_add_main;
    add_table = add_table_main;
    name_table = name_table_main;
    bd = bd_main;

    *is_ok = FillDiaflog();

    Connect(window::id_buttons::ID_PLUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::AddNewLine));
    Connect(window::id_buttons::ID_MINUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::DeleteNewLine));
    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::OnOk));
}

MyAdd::MyAdd(Exel* add_table_main, BD* bd_main, bool* is_ok, wxAuiNotebook *nb)
    : wxDialog(NULL, -1, wxT("Добавить"), wxDefaultPosition, wxDefaultSize)
{
    get_add = wxT("sql/contract/add/get_add.txt");
    bd = bd_main;
    contract_table = add_table_main;
    add_table = new Exel(nb);
    add_table->is_read_only = add_table_main->is_read_only;
    add_table->CreateGrid(1, 4);
    add_table->SetColLabelValue(0, wxT("Договор"));
    add_table->SetColLabelValue(1, wxT("Заказчик"));
    add_table->SetColLabelValue(2, wxT("Проект"));
    add_table->SetColLabelValue(3, wxT("Стоимость"));
    name_table = wxT("contract_table");

    *is_ok = FillDiaflog();

    Connect(window::id_buttons::ID_PLUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::AddNewLineContract));
    Connect(window::id_buttons::ID_MINUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::DeleteNewLineContract));
    Connect(window::id_buttons::ID_COMBOBOX, wxEVT_TEXT,
        wxCommandEventHandler(MyAdd::CheckProject));
    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(MyAdd::OnOk));
}

bool MyAdd::FillDiaflog()
{
    GetFillingVector();

    for(int i = 0 ; i < vector_add.size(); i++)
    {
        if(vector_add[i].get_sql_file.Cmp(wxT("0")) != 0)
        {
            wxArrayString result;
            if(GetSelectFromSQL(&result, vector_add[i].get_sql_file))
            {
                if(name_table.Cmp(wxT("work_group")) == 0 || name_table.Cmp(wxT("subcontr_org")) == 0)
                {
                    int count_result = result.GetCount();
                    if (count_result == 0)
                    {
                        ErrorSQL(wxT("Нет свободных проектов"));
                        return false;
                    }
                }
                if(name_table.Cmp(wxT("equipment_table")) == 0)
                    result.Add(wxT("Организация"));
                combobox_result.push_back(result);
            }
            else
            {
                ErrorSQL(wxT("Попробуйте снова немного позже."));
                return false;
            }
            
        }
    }
    
    vsizer = new wxBoxSizer(wxVERTICAL);

    scrolled = new wxScrolledWindow(this); 

    sizer_name_cols = new wxBoxSizer(wxHORIZONTAL);

    grid_sizer = new wxGridSizer(vector_add.size(), 5, 10);

    AddNameCols();

    button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    button_plus = new wxButton(this, window::id_buttons::ID_PLUS, wxT("+"));
    button_minus = new wxButton(this, window::id_buttons::ID_MINUS, wxT("-"));
    button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    sizer_buttons = new wxBoxSizer(wxHORIZONTAL);

    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_minus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(button_plus, 0, wxALIGN_CENTER | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    if(name_table.Cmp(wxT("contract_table")) == 0)
        FirstFillingLineContract();
    else if(name_table.Cmp(wxT("subcontr_org")) == 0)
        FillingLineSubcontr();
        else
            FillingLine();

    vsizer->Add(scrolled, 1, wxEXPAND);
    vsizer->Add(sizer_buttons, 0, wxEXPAND | wxTOP, 5);
    SetSizer(vsizer);

    int x_size = vector_add.size();
    if(x_size <= 5)
    {
        x_size = x_size * 320;
        this->SetClientSize(x_size, 130);
    }
    else
    {
        this->SetClientSize(1600, 130);
    }
    
    return true;
}

void MyAdd::GetFillingVector()
{
    wxTextFile get_add_file(get_add);
    get_add_file.Open();
    wxString line_file = get_add_file.GetFirstLine();
    set_add = line_file;
    line_file = get_add_file.GetNextLine();
    while(!get_add_file.Eof())
    {
        wxString part_of_line = line_file.Left(1);
        AddInfo info_add;
        info_add.number_col = wxAtoi(part_of_line);

        int size_part_of_line = line_file.Len() - 2;
        part_of_line = line_file.Right(size_part_of_line);
        info_add.get_sql_file = part_of_line;

        vector_add.push_back(info_add);

        line_file = get_add_file.GetNextLine();
    }
    get_add_file.Close();
}

bool MyAdd::GetSelectFromSQL(wxArrayString* result, wxString name_sql_file)
{
    PGresult* res;
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();
    
    const char* ch_sql = sql.mb_str(wxConvUTF8);
    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        PQclear(res);
        return false;
    }

    for (int i = 0; i < PQntuples(res); i++)
    {
        wxString res_col_string(PQgetvalue(res, i, 0), wxConvUTF8);
        result->Add(res_col_string);
    }

    PQclear(res);
    return true;
}

void MyAdd::AddNameCols()
{
    for(int i = 0; i < vector_add.size(); i++)
    {
        wxString name_col = add_table->GetColLabelValue(vector_add[i].number_col);
        grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, name_col), 0, wxALIGN_CENTER | wxALL);
    }
}

void MyAdd::FillingLine()
{
    int number_vector_combobox = vector_combobox.size();
    int number_combobox_result = 0;
    for(int line_number_col = 0; line_number_col < vector_add.size(); line_number_col++)
    {
        if(vector_add[line_number_col].get_sql_file.Cmp(wxT("0")) != 0)
        {
            if(add_table->is_read_only)
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                        0, NULL, 0 | wxCB_READONLY) );
            }
            else
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
            }
            
            vector_combobox[number_vector_combobox + line_number_col]->Append(combobox_result[number_combobox_result]);
            vector_combobox[number_vector_combobox + line_number_col]->AutoComplete(combobox_result[number_combobox_result]);
            number_combobox_result++;
        }
        else
        {
            vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
        }
        
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10);  
}

void MyAdd::FirstFillingLineContract()
{
    int number_vector_combobox = vector_combobox.size();
    int number_combobox_result = 0;
    int number_vector_add = vector_add.size() - 1;

    wxDateTime now = wxDateTime::Now();
    wxString name_contract = wxT("Д") + now.Format(wxT("%d%m%C")) + wxT("/")
        + now.Format(wxT("%H%M%S"));
    vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, name_contract, wxDefaultPosition, wxSize(300, -1),
            0, NULL, wxCB_READONLY) );
    vector_combobox[number_vector_combobox]->Append(name_contract);
    vector_combobox[number_vector_combobox]->SetValue(name_contract);
    grid_sizer->Add(vector_combobox[number_vector_combobox], 0, wxALL | wxALIGN_CENTER);

    for(int line_number_col = 1; line_number_col < number_vector_add; line_number_col++)
    {
        if(vector_add[line_number_col].get_sql_file.Cmp(wxT("0")) != 0)
        {
            if(add_table->is_read_only && line_number_col == 1)
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                        0, NULL, 0 | wxCB_READONLY) );
            }
            else
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
            }
            
            vector_combobox[number_vector_combobox + line_number_col]->Append(combobox_result[number_combobox_result]);
            vector_combobox[number_vector_combobox + line_number_col]->AutoComplete(combobox_result[number_combobox_result]);
            number_combobox_result++;
        }
        else
        {
            vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
        }
        
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }

    vector_spin.push_back( new wxSpinCtrl(scrolled, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                            wxSP_ARROW_KEYS, 0, 2147483647) );
        grid_sizer->Add(vector_spin[0], 0, wxALL | wxALIGN_CENTER);

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10);
}

void MyAdd::FillingLineContract()
{
    for(int line_number_col = 0; line_number_col < vector_add.size(); line_number_col++)
    {
        if(line_number_col == 0 || line_number_col == 1)
        {
            wxStaticText* invisible_text = new wxStaticText(scrolled, wxID_ANY, wxT(" "));
            grid_sizer->Add(invisible_text, 1, wxALL | wxALIGN_CENTER);
        }
        else
        {
            if(line_number_col == 2)
            {
                int number_vector_combobox = vector_combobox.size();
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
                if(vector_add[line_number_col].get_sql_file.Cmp(wxT("0")) != 0)
                {
                    vector_combobox[number_vector_combobox]->Append(combobox_result[1]);
                    vector_combobox[number_vector_combobox]->AutoComplete(combobox_result[1]);
                }
                grid_sizer->Add(vector_combobox[number_vector_combobox], 0, wxALL | wxALIGN_CENTER);
            }
            else
            {
                int number_vector_spin = vector_spin.size();
                vector_spin.push_back( new wxSpinCtrl(scrolled, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                            wxSP_ARROW_KEYS, 0, 2147483647) );
                grid_sizer->Add(vector_spin[number_vector_spin], 0, wxALL | wxALIGN_CENTER);
            }
            
        }
    }
}

void MyAdd::FillingLineSubcontr()
{
    int number_vector_combobox = vector_combobox.size();
    int count_vector_add = vector_add.size() - 2;
    int number_combobox_result = 0;
    for(int line_number_col = 0; line_number_col < count_vector_add; line_number_col++)
    {
        if(vector_add[line_number_col].get_sql_file.Cmp(wxT("0")) != 0)
        {
            if(add_table->is_read_only)
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                        0, NULL, 0 | wxCB_READONLY) );
            }
            else
            {
                vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
            }
            
            vector_combobox[number_vector_combobox + line_number_col]->Append(combobox_result[number_combobox_result]);
            vector_combobox[number_vector_combobox + line_number_col]->AutoComplete(combobox_result[number_combobox_result]);
            number_combobox_result++;
        }
        else
        {
            vector_combobox.push_back( new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, wxEmptyString, wxDefaultPosition, wxSize(300, -1)) );
        }
        
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }
    int number_vector_date = vector_date.size();
    vector_date.push_back( new wxDatePickerCtrl(scrolled, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxSize(300, -1)) );
    grid_sizer->Add(vector_date[number_vector_date], 0, wxALL | wxALIGN_CENTER);

    int number_vector_spin = vector_spin.size();
    vector_spin.push_back( new wxSpinCtrl(scrolled, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize(300, -1), 
                wxSP_ARROW_KEYS, 0, 2147483647) );
    grid_sizer->Add(vector_spin[number_vector_spin], 0, wxALL | wxALIGN_CENTER);

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10);  
}

void MyAdd::GetSizeMyAdd()
{
    int width;
    int height;
    int count_vector_add = vector_add.size();
    int count_vector_combobox = vector_combobox.size();
    GetSize(&width, &height);
    if (name_table.Cmp(wxT("subcontr_org")) == 0)
        count_vector_add = vector_add.size() - 2;

    height = count_vector_combobox / count_vector_add;

    if(height <= 5)
    {
        this->SetClientSize(width, 100 + height * 30);
    }
    else
    {
        this->SetClientSize(width, 250);
    }
}

void MyAdd::GetSizeMyAddContract()
{
    int width;
    int height;
    GetSize(&width, &height);
    height = (vector_combobox.size() - 2);

    if(height <= 5)
    {
        this->SetClientSize(width, 100 + height * 30);
    }
    else
    {
        this->SetClientSize(width, 250);
    }
}

void MyAdd::AddNewLine(wxCommandEvent &event)
{
    if(name_table.Cmp(wxT("subcontr_org")) == 0)
        FillingLineSubcontr();
    else
        FillingLine();
    GetSizeMyAdd();
    this->Layout();
}

void MyAdd::AddNewLineContract(wxCommandEvent &event)
{
    FillingLineContract();
    GetSizeMyAddContract();
    this->Layout();
}

void MyAdd::DeleteNewLine(wxCommandEvent &event)
{
    if (name_table.Cmp(wxT("subcontr_org")) == 0)
    {
        DeleteNewLineSubcontr();
        return;
    }
    
    int count_delete = vector_add.size();
    int index_delete = vector_combobox.size() + count_delete;
    int check_delete = index_delete / count_delete;
    if(check_delete == 2)
    {
        ErrorSQL(wxT("Нельзя удалять первую строчку"));
        return;
    }


    for(int i = 0; i < count_delete; i++)
    {
        index_delete--;
        grid_sizer->Remove(index_delete);
        vector_combobox[index_delete - count_delete]->Destroy();
        vector_combobox.pop_back();
    }

    GetSizeMyAdd();
    this->Layout();
}

void MyAdd::DeleteNewLineContract(wxCommandEvent &event)
{
    int count_delete = vector_add.size();
    int index_delete_vector = vector_combobox.size() - 1;
    int index_del_spin = vector_spin.size() - 1;
    int index_delete_sizer = ((vector_combobox.size() - 3) * 4) + (count_delete * 2);
    if(vector_combobox.size() == 3)
    {
        ErrorSQL(wxT("Нельзя удалять первую строчку"));
        return;
    }


    for(int i = 0; i < 4; i++)
    {
        index_delete_sizer--;
        grid_sizer->Remove(index_delete_sizer);
    }

    vector_combobox[index_delete_vector]->Destroy();
    vector_combobox.pop_back();
    vector_spin[index_del_spin]->Destroy();
    vector_spin.pop_back();

    GetSizeMyAddContract();
    this->Layout();
}

void MyAdd::DeleteNewLineSubcontr()
{
    int count_delete = vector_add.size() - 2;
    int index_delete = vector_combobox.size() + count_delete;
    int count_vector_date = vector_date.size() - 1;
    int count_vector_spin = vector_spin.size() - 1;
    int check_delete = index_delete / count_delete;
    if(check_delete == 2)
    {
        ErrorSQL(wxT("Нельзя удалять первую строчку"));
        return;
    }

    int index_delete_grid = (vector_combobox.size() / 4) * 6 + vector_add.size();

    for(int i = 0; i < 6; i++)
    {
        index_delete_grid--;
        grid_sizer->Remove(index_delete_grid);
    }

    for(int i = 0; i < count_delete; i++)
    {
        index_delete--;
        vector_combobox[index_delete - count_delete]->Destroy();
        vector_combobox.pop_back();
    }

    vector_date[count_vector_date]->Destroy();
    vector_date.pop_back();

    vector_spin[count_vector_spin]->Destroy();
    vector_spin.pop_back();

    GetSizeMyAdd();
    this->Layout();
}

void MyAdd::CheckProject(wxCommandEvent &event)
{
    int count_vector = vector_combobox.size();
    int number_vector_spin = 0;
    for(int i = 2; i < count_vector; i++)
    {
        wxString check_value_project = vector_combobox[i]->GetValue();
        if(check_value_project.Cmp(wxT("NEW")) == 0 || check_value_project.Cmp(wxT("new")) == 0 ||
            check_value_project.Cmp(wxT("New")) == 0)
        {
            vector_spin[number_vector_spin]->Enable(true);
        }
        else
        {
            vector_spin[number_vector_spin]->SetValue(0);
            vector_spin[number_vector_spin]->Enable(false);
        }
        number_vector_spin++;
    }
}

void MyAdd::OnOk(wxCommandEvent &event)
{
    SetFillingVector();

    int size_vector_combobox = vector_combobox.size();
    int size_vector_add = vector_add.size();
    int i = 0;

    if(name_table.Cmp(wxT("contract_table")) == 0)
    {
        vector_combobox_contract.clear();
        int index_money_j = 2;
        int index_money = 0;
        for(int j = 0; j < 2; j++)
        {
            vector_combobox_contract.push_back(vector_combobox[j]);
        }

        int index_hide = 3;
        for(int j = 2; j < size_vector_combobox; j++)
        {
            wxString money_string;
            int money_int = vector_spin[index_money]->GetValue();
            money_string << money_int;
            vector_combobox_contract.push_back(vector_combobox[j]);
            vector_combobox_contract.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, money_string));
            vector_combobox_contract[index_hide]->Hide();
            index_hide += 2;
            index_money++;
        }
        size_vector_combobox = 0;
    } 

    if (name_table.Cmp(wxT("subcontr_org")) == 0)
    {
        int index_last_two_in_line = 3;
        int index_vector_date = 0;
        int index_vector_spin = 0;
        vector_combobox_subcontr.clear();
        for(int j = 0; j < size_vector_combobox; j++)
        {
            vector_combobox_subcontr.push_back(vector_combobox[j]);
            if(j == index_last_two_in_line)
            {
                int index_hide = vector_combobox_subcontr.size();
                wxDateTime end_work = vector_date[index_vector_date]->GetValue();
                wxString time_end_work = end_work.Format(wxT("%Y")) + wxT("-") + end_work.Format(wxT("%m")) + wxT("-") + 
                    end_work.Format(wxT("%d"));
                vector_combobox_subcontr.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, time_end_work));
                vector_combobox_subcontr[index_hide]->Hide();
                index_vector_date++;

                wxString money_string;
                int money_int = vector_spin[index_vector_spin]->GetValue();
                money_string << money_int;
                vector_combobox_subcontr.push_back(new wxComboBox(scrolled, window::id_buttons::ID_COMBOBOX, money_string));
                vector_combobox_subcontr[index_hide + 1]->Hide();
                index_vector_spin++;

                index_last_two_in_line += 4;
            }
        }
        size_vector_combobox = 0;
    }

    while(i < size_vector_combobox)
    {
        int number_combobox_result = 0;
        for(int j = 0; j < size_vector_add; j++)
        {
            if(vector_add[j].get_sql_file.Cmp(wxT("0")) != 0)
            {
                if(!CheckInput(i, number_combobox_result, combobox_result, vector_combobox))
                {
                    ErrorSQL(wxT("Неправильный ввод данных"));
                    vector_combobox[i]->SetSelection(-1, -1);
                    int y_scroll = ((i / size_vector_add) * 3) + 2;
                    scrolled->Scroll(0, y_scroll);
                    return;
                }
                number_combobox_result++;
            }
            i++;
        }
    }
    if(ChooseAdd())
        Close();
}

void MyAdd::SetFillingVector()
{
    wxTextFile set_add_file(set_add);
    set_add_file.Open();
    wxString line_file = set_add_file.GetFirstLine();
    int number_vector = 0;
    while(!set_add_file.Eof())
    {
        vector_set.push_back(line_file);
        line_file = set_add_file.GetNextLine();
    }
    set_add_file.Close();
}

bool MyAdd::ChooseAdd()
{
    if(name_table.Cmp(wxT("staff")) == 0)
    {
        StaffOkAdd* add_staff = new StaffOkAdd(vector_set, add_table, bd, vector_combobox);
        return true;
    }

    if(name_table.Cmp(wxT("departments")) == 0)
    {
        DepartmentOkAdd* add_department = new DepartmentOkAdd(vector_set, add_table, bd, vector_combobox, scrolled);
        if(add_department->is_ok)
            return true;
        
        return false;
    }

    if(name_table.Cmp(wxT("work_group")) == 0)
    {
        WGroupOkAdd* add_wgroup = new WGroupOkAdd(vector_set, add_table, bd, vector_combobox, scrolled);
        if(add_wgroup->is_ok)
            return true;
        
        return false;
    }

    if(name_table.Cmp(wxT("equipment_table")) == 0)
    {
        EquipOkAdd* add_equip = new EquipOkAdd(vector_set, add_table, bd, vector_combobox);
        return true;
    }

    if(name_table.Cmp(wxT("customer")) == 0)
    {
        CustomerOkAdd* add_customer = new CustomerOkAdd(vector_set, add_table, bd, vector_combobox, scrolled);
        if(add_customer->is_ok)
            return true;
        
        return false;
    }

    if(name_table.Cmp(wxT("contract_table")) == 0)
    {
        ContractOkAdd* add_contract = new ContractOkAdd(vector_set, contract_table, bd, vector_combobox, 
                vector_combobox_contract, scrolled);
        if(add_contract->is_ok)
            return true;
        
        return false;
    }

    if(name_table.Cmp(wxT("subcontr_org")) == 0)
    {
        SubcontrOkAdd* add_subcontr = new SubcontrOkAdd(vector_set, add_table, bd, vector_combobox, vector_combobox_subcontr, vector_date, scrolled);
        if(add_subcontr->is_ok)
            return true;
        
        return false;
    }

    return true;
}