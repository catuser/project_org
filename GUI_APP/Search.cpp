#include "Search.h"

Search::Search(wxPanel* panel_main, std::vector<TableColumn> table_colums_main, Exel* myexel) 
    : wxFrame(panel_main, -1, wxT("Поиск"), wxDefaultPosition, wxSize(600, 200), 
        wxSYSTEM_MENU | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN | wxSTAY_ON_TOP)
{
    search_inexel = myexel;
    table_colums = table_colums_main;
    sizer = new wxBoxSizer(wxVERTICAL);
    sizer_buttons = new wxBoxSizer(wxHORIZONTAL);
    wxBoxSizer* sizer_buttons_list = new wxBoxSizer(wxHORIZONTAL);

    button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));
    button_next = new wxButton(this, window::id_buttons::ID_NEXT, wxT("Следующий"));
    button_previous = new wxButton(this, window::id_buttons::ID_PREVIOUS, wxT("Предыдущий"));
    
    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    sizer_buttons_list->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons_list->Add(button_previous, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons_list->Add(button_next, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    if(search_inexel->is_read_only)
    {
        box_colums1 = new wxComboBox(this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 
            0, NULL, 0 | wxCB_READONLY);
    }
    else
    {
        box_colums1 = new wxComboBox(this, wxID_ANY);
    }

    for(int i = 0; i < table_colums.size(); i++)
    {
        if(table_colums[i].iscolumn_show == true)
        {
            wxString name_column_string = table_colums[i].name_column;
            name_colums_arr.Add(name_column_string);
            box_colums1->Append(name_column_string);
        }
    }
    box_colums1->AutoComplete(name_colums_arr);

    enter_search = new wxTextCtrl(this, window::id_buttons::ID_TEXT, wxEmptyString, wxDefaultPosition,
        wxDefaultSize, 0 | wxTE_PROCESS_ENTER);

    wxGridSizer* grid_sizer = new wxGridSizer(2, 2, 5, 10);
    grid_sizer->Add(new wxStaticText(this, wxID_ANY, wxT("Столбец")), 0 , wxALIGN_CENTER);
    grid_sizer->Add(new wxStaticText(this, wxID_ANY, wxT("Искомое слово")), 0 , wxALIGN_CENTER);
    grid_sizer->Add(box_colums1, 0, wxEXPAND);
    grid_sizer->Add(enter_search, 0, wxEXPAND);

    sizer->Add(grid_sizer, 0, wxEXPAND | wxRIGHT | wxLEFT, 5);
    sizer->Add(new wxStaticText(this, wxID_ANY, wxT("")), 1);
    sizer->Add(sizer_buttons_list, 0, wxEXPAND, 5);
    sizer->Add(sizer_buttons, 0, wxEXPAND, 5);
	SetSizer(sizer);

    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Search::OnOk));
    Connect(window::id_buttons::ID_TEXT, wxEVT_TEXT_ENTER,
        wxCommandEventHandler(Search::OnOk));
    Connect(window::id_buttons::ID_NEXT, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Search::NextSearch));
    Connect(window::id_buttons::ID_PREVIOUS, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Search::PreviousSearch));
    Connect(wxID_CANCEL, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Search::OnQuit));
}

void Search::OnOk(wxCommandEvent &event)
{
    wxString check_enter_column = box_colums1->GetValue();
    if(check_enter_column.IsNull())
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Не выбран столбец."), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        event.Skip();
        return;
    }

    wxString get_search_name = enter_search->GetValue();

    if(get_search_name.IsNull())
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Искомое слово пустое."), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        event.Skip();
        return;
    }

    pairs_coords.clear();
    all_pairs_coords.clear();
    bool isfind = false;
    int len_search_name = get_search_name.Len();
    bool isfind_part = false;
    for(int i = 0; i < table_colums.size(); i++)
    {
        if(check_enter_column.Cmp(table_colums[i].name_column) == 0)
        {
            int pos_col = table_colums[i].number_col;
            int number_col = search_inexel->GetNumberRows();
            for(int j = 0; j < number_col; j++)
            {
                std::pair<int, int> pair_vector;
                int len_column_name = search_inexel->GetCellValue(wxGridCellCoords(j, pos_col)).Len();
                if(len_search_name > len_column_name)
                {
                    continue;
                }
                if(len_search_name == len_column_name)
                {
                    if(get_search_name.Cmp(search_inexel->GetCellValue(wxGridCellCoords(j, pos_col)) ) == 0)
                    {
                        pair_vector.first = j;
                        pair_vector.second = pos_col;
                        all_pairs_coords.push_back(pair_vector);

                        if(search_inexel->IsRowShown(j))
                        {
                            pairs_coords.push_back(pair_vector);
                        }

                        isfind = true;
                    }
                }
                if(len_search_name < len_column_name)
                {
                    wxString comparison_string = search_inexel->GetCellValue(wxGridCellCoords(j, pos_col)).Left(len_search_name);
                    if(get_search_name.Cmp(comparison_string) == 0)
                    {
                        pair_vector.first = j;
                        pair_vector.second = pos_col;
                        all_pairs_coords.push_back(pair_vector);

                        if(search_inexel->IsRowShown(j))
                        {
                            pairs_coords.push_back(pair_vector);
                        }
                        isfind = true;
                    }
                }
            }
            if(isfind)
            {
                if(pairs_coords.size() == 0)
                {
                    ErrorSQL(wxT("Все нужные строчки скрыты"));
                    button_next->Show(false);
                    button_previous->Show(false);
                    return;
                }

                wxString info_message;
                int is_part_rows_hide = all_pairs_coords.size() - pairs_coords.size();
                if(is_part_rows_hide > 0)
                {
                    info_message = wxT("Часть строчек скрыто");
                }
                else
                {
                    info_message = wxT("Нашел");
                }

                button_next->Show(true);
                button_previous->Show(true);
                
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    info_message, wxT("Info"), wxOK | wxICON_INFORMATION);
                dial->ShowModal();
                number_pair = 0;
                search_inexel->GoToCell(pairs_coords[number_pair].first, pairs_coords[number_pair].second);
                search_inexel->SelectBlock(
                    wxGridCellCoords(pairs_coords[number_pair].first, pairs_coords[number_pair].second), 
                        wxGridCellCoords(pairs_coords[number_pair].first, pairs_coords[number_pair].second), true);
                event.Skip();
                return;
            }
            else
            {
                ErrorSQL(wxT("Не получилось найти. Попробуйте снова."));
                button_next->Show(false);
                button_previous->Show(false);
                event.Skip();
                return;
            }
            
        }
    }

    ErrorSQL(wxT("Выбран несуществующий столбец."));
    event.Skip();
    return;
}

void Search::NextSearch(wxCommandEvent &event)
{
    
    SelectBlockOfTable(false);

    number_pair++;
    if(number_pair == pairs_coords.size())
    {
        number_pair = 0;
    }

    SelectBlockOfTable(false);

    search_inexel->GoToCell(pairs_coords[number_pair].first, pairs_coords[number_pair].second);
    SelectBlockOfTable(true);
}
void Search::PreviousSearch(wxCommandEvent &event)
{
    SelectBlockOfTable(false);
    
    number_pair--;
    if(number_pair == -1)
    {
        number_pair = pairs_coords.size() - 1;
    }

    SelectBlockOfTable(false);

    search_inexel->GoToCell(pairs_coords[number_pair].first, pairs_coords[number_pair].second);
    SelectBlockOfTable(true);
}

void Search::OnQuit(wxCommandEvent &event)
{
    Close();
}

void Search::SelectBlockOfTable(bool is_select)
{
    search_inexel->SelectBlock(
        wxGridCellCoords(pairs_coords[number_pair].first, pairs_coords[number_pair].second), 
            wxGridCellCoords(pairs_coords[number_pair].first, pairs_coords[number_pair].second), is_select);
}