#include <wx/wx.h>
#include <wx/tglbtn.h>
#include <wx/hyperlink.h>
#include <wx/textfile.h>

#include <iostream>

#include "id.h"

class HelpDialog : public wxDialog
{
public:
	HelpDialog(const wxString& title);
    void OnQuit(wxCommandEvent &event);
    void OnPopupPanel(wxCommandEvent &event);

    wxPanel* panel;

    wxStaticText* NameProg;
    wxStaticText* VersionProg;
    wxStaticText* AboutProg;
    wxStaticText* SourceProg;
    wxGenericHyperlinkCtrl *m_hyperlink1;

    wxToggleButton* Credits;
    wxButton* Quit;

    wxBoxSizer* vbox;
    wxBoxSizer* hbox1;
    wxBoxSizer* hbox2;

    wxListBox* listbox;

    int count_lines;
    int size_popup;
    bool is_popup;
};