#include "WorkWithInput.h"

bool CheckInput(int number_combobox, int number_combobox_result, 
        std::vector<wxArrayString> combobox_result, std::vector<wxComboBox*> vector_combobox)
{
    int number_array_string = combobox_result[number_combobox_result].GetCount();
    wxArrayString array_compair_string = combobox_result[number_combobox_result];
    wxString result_add = vector_combobox[number_combobox]->GetValue();
    for(int i = 0; i < number_array_string; i++)
    {
        if(result_add.Cmp(array_compair_string[i]) == 0)
            return true;
    }
    return false;
}

bool CheckRepeatProject(int count_lines, int value_index, int plus_index, 
        std::vector<wxComboBox*> vector_combobox, wxScrolledWindow* scrolled)
{
    int i = 0;
    int index_first = value_index;
    while(i < count_lines)
    {
        int index_second = value_index;
        wxString name_project = vector_combobox[index_first]->GetValue();
        for(int j = 0; j < count_lines; j++)
        {
            if(i == j)
            {
                index_second += plus_index;
                continue;
            }
            wxString compare_name_project = vector_combobox[index_second]->GetValue();
            if(name_project.Cmp(compare_name_project) == 0)
            {
                vector_combobox[index_second]->SetSelection(-1, -1);
                int y_scroll = (j * 3) + 2;
                scrolled->Scroll(0, y_scroll);
                return false;
            }
            index_second += plus_index;
        }

        i++;
        index_first += plus_index;
    }
    return true;
}

bool ReplaceNameId(int count_cols, std::vector<wxComboBox*> vector_combobox, int number_col, wxString set_sql, BD* bd)
{
    int count_lines = vector_combobox.size() / count_cols;
    int index_result = 0;
    wxArrayString result;
    for(int i = 0; i < count_lines; i++)
    {
        int index_insert = i * count_cols + number_col;
        wxString name_project = vector_combobox[index_insert]->GetValue();
        if(!GetWhereSQL(set_sql, &result, name_project, bd, true))
        {
            return false;
        }
        int index_result = result.GetCount() - 1;
        vector_combobox[index_insert]->SetValue(result[index_result]);
    }

    result.Clear();
    return true;
}