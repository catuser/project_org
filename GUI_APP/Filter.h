#ifndef filter_h
#define filter_h

#include <wx/wx.h>

#include "Exel.h"
#include "id.h"

class Filter : public wxDialog
{
public:
    Filter(std::vector<TableColumn> table_colums_main, std::vector<NumberColumn> sql_filter_table_main, 
        Exel* filter_table_main, wxString name_table_main, BD* bd_main, bool* isok);
    Filter(std::vector<TableColumn> table_colums_main, Exel* filter_table_main, 
        wxString name_table_main, BD* bd_main, int count_buttons_main, bool* isok);
    void GetFiter();
    void OnOk(wxCommandEvent &event);
    void SetFilter();
    void UseFilter();
    bool GetSelectFromSQL(wxArrayString* result, wxString name_sql_file);
    void OnCheckBox(wxCommandEvent &event);

    std::vector<TableColumn> table_colums;
    std::vector<NumberColumn> sql_filter_table;
    Exel* filter_table;
    wxString name_table;
    BD* bd;
    int count_buttons;

    std::vector<wxCheckBox*> buttons_cols;
    std::vector<std::pair<wxCheckBox*, int>> buttons_rows;
    wxArrayString buttons_rows_name;
    bool check_first_part;
    bool check_second_part;
};

#endif