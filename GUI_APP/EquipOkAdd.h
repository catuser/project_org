#include "Exel.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class EquipOkAdd
{
public:
    EquipOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main);
    void ChangeNameOnUUID();

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;

    wxArrayString result;

    std::vector<wxComboBox*> old_vector_combobox;
    std::vector<wxComboBox*> new_vector_combobox;
    std::vector<wxComboBox*> vector_org;
    std::vector<wxString> vector_uuid;
};