#ifndef UUID_H
#define UUID_H

#include <string>
#include <cstdlib>

#include "Exel.h"

std::string generateUUID();

void GetAndCheckUuid(Exel* check_table, int number_col, std::vector<wxString> &vector_id, int number_uuid);

#endif