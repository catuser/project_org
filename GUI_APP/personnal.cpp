#include "personnal.h"

Personnal::Personnal(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main) :
            wxPanel(panel_buttons, wxID_ANY)
{
    //Флаги - открыта ли данная таблица или нет
    isonstaff = false;
    isondepartments = false;
    isonwork_group = false;

    //Флаг - Стоит ли галочка у check box Read Only
    is_read_only_personal = true;

    table_buttons = table;
    table_buttons_sizer = table_sizer;
    //bd - Класс для соединения с БД
    bd = bd_main;
    //nb - это auinotebook
    nb = nb_main;
    //Текст ошибки подключения к БД
    error_bd = error_bd_main;
}

//Инициализация и размещения под кнопок Персонала в приложении 
void Personnal::PersonnalSetSizer()
{
    staff = new wxButton(table_buttons, window::id_buttons::ID_STAFF, wxT("Сотрудники"), wxDefaultPosition, wxSize(121, 33));
    departments = new wxButton(table_buttons, window::id_buttons::ID_DEPARTMENTS, wxT("Отделы"), wxDefaultPosition, wxSize(121, 33));
    work_group = new wxButton(table_buttons, window::id_buttons::ID_WGROUP, wxT("Раб Группы"), wxDefaultPosition, wxSize(121, 33));

    table_buttons_sizer->Add(staff, 0, wxLEFT, 4);
    table_buttons_sizer->Add(departments, 0, wxLEFT, 4);
    table_buttons_sizer->Add(work_group, 0, wxLEFT, 4);

    table_buttons->SetSizer(table_buttons_sizer);
    //Обновляю экран, чтобы пользователь увидел кнопки
    table_buttons->Layout();

    int height = 100;
    table_buttons->SetScrollbars(0, 2, 0, height/2);
}

//Была нажата кнопка Сотрудники
//Создается и заполняется таблица Сотрудники
void Personnal::OnStaff(bool isupdate)
{
    //isonstaff - открыта ли данная таблица или нет
    //Если нет, то тогда ее создает и заполняет
    //Если да, то просто показывает таблицу пользователю
    if(!isonstaff)
    {
        table_vector_staff.clear();
        filter_staff.clear();
        //помечаю, что таблица открыта
        isonstaff = true;

        wxTextFile sql_file(wxT("sql/staff/staff.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += wxT("\n");
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        //инициализация таблицы
        onstaff = new Exel(nb);
        //передаю таблицы флаг READ ONLY
        onstaff->is_read_only = is_read_only_personal;
        //Получаю данные из БД и заполняю таблицу
        onstaff->GetTable(bd, ch_sql, isupdate);
        //Добавляю таблицу в auinotebook и показываю ее
        nb->AddPage(onstaff, wxT("Сотрудники"), true);
        //Обновляю auinootebook, чтобы пользователь увидел на экране таблицу
        nb->Layout();

        //Заполняю данные о столбцах таблицы. Нужно для Фильтрации
        for(int i = 0; i < 7; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onstaff->GetColLabelValue(i);
            table_columns.iscolumn_show = onstaff->IsColShown(i);
            table_columns.number_col = i;
            table_vector_staff.push_back(table_columns);
        }

        //Заполняю данные о строчках, которые можно будет фильтровать
        NumberColumn filter_sql;
        filter_sql.name_sql_file = wxT("sql/staff/filter/name_department.sql");
        filter_sql.name_column = wxT("Отдел");
        filter_sql.number_column = 2;
        filter_staff.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/staff/filter/name_proffesion.sql");
        filter_sql.name_column = wxT("Профессия");
        filter_sql.number_column = 5;
        filter_staff.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/staff/filter/name_worker.sql");
        filter_sql.name_column = wxT("Работник");
        filter_sql.number_column = 6;
        filter_staff.push_back(filter_sql);

        //Инициализирую объект фильтр, чтобы применить фильтрацию
        bool isok = false;
        Filter* filter_personnal = new Filter(table_vector_staff, filter_staff, onstaff, wxT("staff"), bd, &isok);
        //Если не было ошибок, то применяет фильтрацию
        //Иначе выводит сообщение об ошибки
        if(isok)
        {
            filter_personnal->UseFilter(); 
            filter_personnal->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
        
    }
    else
    {
        //Получает индекс данной таблицы в auinotebook
        int page_index = nb->GetPageIndex(onstaff);
        if( page_index != wxNOT_FOUND)
        {
            //Показывает пользователю эту таблицу
            nb->SetSelection(page_index);
        }
    }
        
}

//Была нажата кнопка Отделы
//Создается и заполняется таблица Отдел
void Personnal::OnDepartments(bool isupdate)
{
    if(!isondepartments)
    {
        table_vector_departments.clear();
        filter_departments.clear();
        isondepartments = true;

        //Чтобы собрать данные для данной таблицы необходимы два разных запроса
        //Их собираю в одну переменную
        //B Во время создания таблицы я их разделю обратно на два разных запроса
        wxTextFile sql_file1(wxT("sql/departments/departments_1.sql"));
        sql_file1.Open();
        int count_lines = sql_file1.GetLineCount();
        wxString sql = sql_file1.GetFirstLine();
        while(!sql_file1.Eof())
        {
            sql += sql_file1.GetNextLine();
        }
        sql_file1.Close();
        sql += wxT("\n");

        wxTextFile sql_file2(wxT("sql/departments/departments_2.sql"));
        sql_file2.Open();
        count_lines = sql_file2.GetLineCount();
        sql += sql_file2.GetFirstLine();
        while(!sql_file2.Eof())
        {
            sql += sql_file2.GetNextLine();
        }
        sql_file2.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        //Аналогично как с "Сотрудники"
        ondepartments = new Exel(nb);
        ondepartments->is_read_only = is_read_only_personal;
        ondepartments->GetTableDepartments(bd, ch_sql, isupdate);
        nb->AddPage(ondepartments, wxT("Отделы"), true);
        nb->Layout();

        for(int i = 0; i < 7; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = ondepartments->GetColLabelValue(i);
            table_columns.iscolumn_show = ondepartments->IsColShown(i);
            table_columns.number_col = i;
            table_vector_departments.push_back(table_columns);
        }

        NumberColumn filter_sql;
        filter_sql.name_sql_file = wxT("sql/departments/filter/filter_dep.sql");
        filter_sql.name_column = wxT("НазваниеОтд");
        filter_sql.number_column = 1;
        filter_departments.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/departments/filter/filter_prof.sql");
        filter_sql.name_column = wxT("Профессия");
        filter_sql.number_column = 6;
        filter_departments.push_back(filter_sql);

        bool isok = false;
        Filter* filter_personnal = new Filter(table_vector_departments, filter_departments, ondepartments, wxT("departments"), bd, &isok);
        if(isok)
        {
            filter_personnal->UseFilter(); 
            filter_personnal->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(ondepartments);
        if( page_index != wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}


//Аналогично OnStaff
void Personnal::OnWGroup(bool isupdate)
{
    if(!isonwork_group)
    {
        table_vector_work_group.clear();
        filter_work_group.clear();
        isonwork_group = true;

        wxTextFile sql_file(wxT("sql/work_group/work_group.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += wxT("\n");
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onwork_group = new Exel(nb);
        onwork_group->is_read_only = is_read_only_personal;
        onwork_group->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onwork_group, wxT("Рабочие Группы"), true);
        nb->Layout();

        for(int i = 0; i < 10; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onwork_group->GetColLabelValue(i);
            table_columns.iscolumn_show = onwork_group->IsColShown(i);
            table_columns.number_col = i;
            table_vector_work_group.push_back(table_columns);
        }

        NumberColumn filter_sql;
        filter_sql.name_sql_file = wxT("sql/work_group/filter/filter.sql");
        filter_sql.name_column = wxT("isдоговр");
        filter_sql.number_column = 8;
        filter_work_group.push_back(filter_sql);

        bool isok = false;
        Filter* filter_personnal = new Filter(table_vector_work_group, filter_work_group, onwork_group, wxT("work_group"), bd, &isok);
        if(isok)
        {
            filter_personnal->UseFilter(); 
            filter_personnal->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onwork_group);
        if( page_index != wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

//Обновление Таблицы
void Personnal::OnUpdate(int number_exel, wxString file_name)
{
    //Открыаю файли и считывают от туда строки
    wxTextFile sql_file(file_name);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql_string = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql_string += wxT("\n");
        sql_string += sql_file.GetNextLine();
    }
    sql_file.Close();
    //Перевожу в тип char*, чтобы потом данный запрос передать БД
    const char* sql = sql_string.mb_str(wxConvUTF8);

    //По переданному номеру определается в какой таблице была нажата клавиша Обновить
    switch(number_exel)
    {
        case 1:
        {
            //У таблицы Сотрудники вызывается метод Обноввления таблицы
            onstaff->OnUpdateTable(bd, sql);
            //Флаг - были ли ошибки
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_staff, filter_staff, onstaff, wxT("staff"), bd, &isok);
            //Если не было, то тогда сразу применяется фильтр
            if(isok)
            {
                filter_personnal->UseFilter(); 
                filter_personnal->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        //Аналогично и для других таблиц
        case 2:
        {
            ondepartments->OnUpdateTableDepartments(bd, sql);
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_departments, filter_departments, ondepartments, wxT("departments"), bd, &isok);
            if(isok)
            {
                filter_personnal->UseFilter(); 
                filter_personnal->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 3:
        {
            onwork_group->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_work_group, filter_work_group, onwork_group, wxT("work_group"), bd, &isok);
            if(isok)
            {
                filter_personnal->UseFilter(); 
                filter_personnal->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnUpdateTwo(int number_exel, wxString sql1, wxString sql2)
{
    wxTextFile sql_file1(sql1);
    sql_file1.Open();
    int count_lines = sql_file1.GetLineCount();
    wxString sql_string = sql_file1.GetFirstLine();
    while(!sql_file1.Eof())
    {
        sql_string += sql_file1.GetNextLine();
    }
    sql_file1.Close();
    sql_string += wxT("\n");

    wxTextFile sql_file2(sql2);
    sql_file2.Open();
    count_lines = sql_file2.GetLineCount();
    sql_string += sql_file2.GetFirstLine();
    while(!sql_file2.Eof())
    {
        sql_string += sql_file2.GetNextLine();
    }
    sql_file2.Close();
    const char* sql = sql_string.mb_str(wxConvUTF8);

    switch(number_exel)
    {
        case 2:
        {
            ondepartments->OnUpdateTableDepartments(bd, sql);
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_departments, filter_departments, ondepartments, wxT("departments"), bd, &isok);
            if(isok)
            {
                filter_personnal->UseFilter(); 
                filter_personnal->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnFilter(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_staff, filter_staff, onstaff, wxT("staff"), bd, &isok);
            filter_personnal->ShowModal();
            filter_personnal->Destroy();
            break;
        }
        case 2:
        {
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_departments, filter_departments, ondepartments, wxT("departments"), bd, &isok);
            filter_personnal->ShowModal();
            filter_personnal->Destroy();
            break;
        }
        case 3:
        {
            bool isok = false;
            Filter* filter_personnal = new Filter(table_vector_work_group, filter_work_group, onwork_group, wxT("work_group"), bd, &isok);
            filter_personnal->ShowModal();
            filter_personnal->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnSearch(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            Search* search_personnal = new Search(this, table_vector_staff, onstaff);
            search_personnal->Show(true);
            break;
        }
        case 2:
        {
            Search* search_personnal = new Search(this, table_vector_departments, ondepartments);
            search_personnal->Show(true);
            break;
        }
        case 3:
        {
            Search* search_personnal = new Search(this, table_vector_work_group, onwork_group);
            search_personnal->Show(true);
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnAdd(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool is_ok = true;
            MyAdd* add_personnal = new MyAdd(wxT("sql/staff/add/get_add.txt"), onstaff, wxT("staff"), bd, &is_ok);
            if(is_ok)
            {
                add_personnal->ShowModal();
                OnUpdate(1, wxT("sql/staff/staff.sql"));
            }
            add_personnal->Destroy();
            break;
        }
        case 2:
        {
            bool is_ok = true;
            MyAdd* add_personnal = new MyAdd(wxT("sql/departments/add/get_add.txt"), ondepartments, wxT("departments"), bd, &is_ok);
            if(is_ok)
            {
                add_personnal->ShowModal();
                OnUpdateTwo(2, wxT("sql/departments/departments_1.sql"), wxT("sql/departments/departments_2.sql"));
            }
            add_personnal->Destroy();
            break;
        }
        case 3:
        {
            bool is_ok = true;
            MyAdd* add_personnal = new MyAdd(wxT("sql/work_group/add/get_add.txt"), onwork_group, wxT("work_group"), bd, &is_ok);
            if(is_ok)
            {
                add_personnal->ShowModal();
                OnUpdate(3, wxT("sql/work_group/work_group.sql"));
            }
            add_personnal->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnChange(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool is_ok = true;
            Change* change_personnal = new Change(wxT("sql/staff/change/get_change.txt"), onstaff, wxT("staff"), bd, &is_ok);
            if(is_ok)
            {
                change_personnal->ShowModal();
                OnUpdate(1, wxT("sql/staff/staff.sql"));
            }
            change_personnal->Destroy();
            break;
        }
        case 2:
        {
            bool is_ok = true;
            Change* change_personnal = new Change(wxT("sql/departments/change/get_change.txt"), ondepartments, wxT("departments"), bd, &is_ok);
            if(is_ok)
            {
                change_personnal->ShowModal();
                OnUpdateTwo(2, wxT("sql/departments/departments_1.sql"), wxT("sql/departments/departments_2.sql"));
            }
            change_personnal->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Personnal::OnDelete(int number_exel)
{
    if(!bd->isConnect)
    {
        ErrorConnectBD();
    }
    else
    {
        switch(number_exel)
        {
            case 1:
            {
                OnDeleteStaff();
                break;
            }
            case 2:
            {
                OnDeleteDepartments();
                break;
            }
            case 3:
            {
                OnDeleteWGroup();
                break;
            }
            default:
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Personnal don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
        }
    }
}

int Personnal::OnDeleteSQL(wxString name_file, wxString cell_id, bool isselect)
{
    PGresult* res_sql;
    //чтение файла
    wxString select_sql = wxT(" ");
    wxTextFile sql_file(name_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    //в файле ищет слово WHERE
    //Если находи, то в pos сохраняет ее позицию в файле
    int pos = sql.Find(wxT("WHERE"));
    //В цикле после каждого WHERE = ставит в файл 'cell_id'
    //cell_id - значение
    while(pos != wxString::npos)
    {
        wxString s1;
        wxString s2;
        wxString s3;
        if (pos != wxString::npos)
        {
            s1 = sql.SubString(0, pos-1);
            int len_s = sql.Len() - s1.Len();
            s2 = sql.Right(len_s);
        }

        pos = s2.Find(wxT("="));
        if (pos != wxString::npos)
        {
            s3 = s2.SubString(0, pos);
            int len_s = s2.Len() - s3.Len();
            sql = s2.Right(len_s);
            s2 = s3;
        }

        select_sql += s1 + s2 + wxT("'") + cell_id + wxT("'");
        pos = sql.Find(wxT("WHERE"));
    }

    select_sql += sql + wxT("\0");
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    //Отправялю запрос
    res_sql = bd->request(ch_sql);

    //Проверяю, была ли ошибка
    if(!(ErrorRequest(res_sql, isselect)))
    {
        return -1;
    }
    

    int count_int = 0;
    //Если запрос был на получение результата (SELECT)
    //То результат записываю
    if(isselect)
    {
        char* count_sql = PQgetvalue(res_sql, 0, 0);
        count_int = atoi(count_sql);
    }
    return count_int;
}

void Personnal::OnDeleteStaff()
{
    //Получаю выделенные строчки
    wxArrayInt delete_rows = onstaff->GetSelectedRows();
    //удаляю строчки снизу вверх
    int i = delete_rows.GetCount() - 1;
    while(i >= 0)
    {
        //получаю id сотрудника, которого удаляю
        wxString cell_id = onstaff->GetCellValue(delete_rows[i], 0);
        //функция, которая удаляет выделенную строчку
        if(!OnDeleteStaffMainPart(cell_id))
        {
            delete_rows.Clear();
            return;
        }
        
        i--;
    }
    delete_rows.Clear();

    //получаю список выделенных ячеек
    wxGridCellCoordsArray delete_cells = onstaff->GetSelectedCells();
    //удаляю ячейки снизу
    i = delete_cells.Count() - 1;
    while(i >= 0) 
    {
	    //получаю номер строчки, которую нужно удалить
        int row = delete_cells[i].GetRow();   
        wxString cell_id = onstaff->GetCellValue(row, 0);
        //функция, которая удаляет выделенную строчку
        if(!OnDeleteStaffMainPart(cell_id))
        {
            delete_cells.Clear();
            return;
        }
        i--;
	}
    delete_cells.Clear();
    //обновляю таблицу
    OnUpdate(1, wxT("sql/staff/staff.sql"));
}

bool Personnal::OnDeleteStaffMainPart(wxString cell_id)
{
    //Проверяю, является ли данный человек боссом отдела
    int result = OnDeleteSQL(wxT("sql/staff/delete/check_boss_dep.sql"), cell_id, true);
    if( result == -1)
    {
        return false;
    }

    //Если да, он вернул значение больше нуля
    if(result > 0)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        wxT("Поменяйте начальника у Отдела"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }


    //Проверяю, является ли данный человек боссом рабочей группы, 
    //которая сейчас выполняет работу
    result = OnDeleteSQL(wxT("sql/staff/delete/check_boss_wgroup.sql"), cell_id, true);
    if(result == -1)
    {
        return false;
    }

    if(result > 0)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        wxT("Поменяйте начальника у Работающей Группы"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    //удаляет сотрудника
    result = OnDeleteSQL(wxT("sql/staff/delete/delete.sql"), cell_id, false);

    if(result == -1)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        wxT("Не удалось удалить запись"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    return true;
}

void Personnal::OnDeleteDepartments()
{
    wxArrayInt delete_rows = ondepartments->GetSelectedRows();
    int i = delete_rows.GetCount() - 1;
    bool isorigin;
    wxArrayString cell_id_dep_block;
    cell_id_dep_block.Add(wxT(" "));
    int check_copy = cell_id_dep_block.GetCount() - 1;
    while(i >= 0)
    {
        //Проверяю на повтор. Чтобы не удалять то, что уже удалил
        wxString cell_id_dep = ondepartments->GetCellValue(delete_rows[i], 0);
        wxString cell_id_bos = ondepartments->GetCellValue(delete_rows[i], 2);
        isorigin = true;
        while(check_copy >= 0)
        {
            if(cell_id_dep.Cmp(cell_id_dep_block[check_copy]) == 0)
            {
                isorigin = false;
            }
            check_copy--;
        }
        if(isorigin)
        {
            wxString name_dep = ondepartments->GetCellValue(delete_rows[i], 1);
            cell_id_dep_block.Add(cell_id_dep);
            check_copy = cell_id_dep_block.GetCount() - 1;
            //Функция, в которой происходит удаление
            if(!OnDeleteDepartmentsMainPart(cell_id_dep, cell_id_bos, name_dep))
            {
                delete_rows.Clear();
                cell_id_dep_block.Clear();
                return;
            }
        }
        i--;
    }
    delete_rows.Clear();

    cell_id_dep_block.Empty();
    wxGridCellCoordsArray delete_cells = ondepartments->GetSelectedCells();
    i = delete_cells.Count() - 1;
    cell_id_dep_block.Add(wxT(" "));
    check_copy = cell_id_dep_block.GetCount() - 1;
    while(i >= 0) 
    {
	    int row = delete_cells[i].GetRow();

        wxString cell_id_dep = ondepartments->GetCellValue(row, 0);
        wxString cell_id_bos = ondepartments->GetCellValue(row, 2);
        isorigin = true;

        while(check_copy >= 0)
        {
            if(cell_id_dep.Cmp(cell_id_dep_block[check_copy]) == 0)
            {
                isorigin = false;
            }
            check_copy--;
        }

        if(isorigin)
        {
            wxString name_dep = ondepartments->GetCellValue(row, 1);
            cell_id_dep_block.Add(cell_id_dep);
            check_copy = cell_id_dep_block.GetCount() - 1;
            if(!OnDeleteDepartmentsMainPart(cell_id_dep, cell_id_bos, name_dep))
            {
                delete_cells.Clear();
                cell_id_dep_block.Clear();
                return;
            }  
        }          
        i--;
	}

    delete_cells.Clear();
    cell_id_dep_block.Clear();
    OnUpdateTwo(2, wxT("sql/departments/departments_1.sql"), wxT("sql/departments/departments_2.sql"));
}

bool Personnal::OnDeleteDepartmentsMainPart(wxString cell_id_dep, wxString cell_id_bos, wxString name_dep)
{
    PGresult* res;

    int what_string = 0;

    //Проверяю обслуживает ли данный отдел какое либо оборудование
    int result = OnDeleteSQL(wxT("sql/departments/delete/check_equip.sql"), cell_id_dep, true);
    if(result == -1)
    {
        return false;
    }

    if(result > 0)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
        wxT("У отдела числится оборудование"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        return false;
    }

    wxString select_sql = wxT("");
    wxTextFile sql_file(wxT("sql/departments/delete/delete.sql"));
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    while(pos != wxString::npos)
    {
        wxString s1;
        wxString s2;
        wxString s3;
        if (pos != wxString::npos)
        {
            s1 = sql.SubString(0, pos-1);
            int len_s = sql.Len() - s1.Len();
            s2 = sql.Right(len_s);
        }

        pos = s2.Find(wxT("="));
        if (pos != wxString::npos)
        {
            s3 = s2.SubString(0, pos);
            int len_s = s2.Len() - s3.Len();
            sql = s2.Right(len_s);
            s2 = s3;
        }

        //в зависимости от WHERE (первый, второй или третий)
        //закидывает определенное значени
        switch(what_string)
        {
            case 0:
            {
                select_sql += s1 + s2 + wxT("'") + cell_id_dep + wxT("'");
                break;
            }
            case 1:
            {
                select_sql += s1 + s2 + wxT("'") + cell_id_bos + wxT("'");
                break;
            }
            case 2:
            {
                select_sql += s1 + s2 + wxT("'") + name_dep + wxT("'");
                break;
            }
        }
            
        what_string++;
        pos = sql.Find(wxT("WHERE"));
    }

    select_sql += sql + wxT("\0");

    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(!(ErrorRequest(res, false)))
    {   
        res = bd->request("ROLLBACK;");
        PQclear(res);
        return false;
    }        

    PQclear(res);
    return true;
}

void Personnal::OnDeleteWGroup()
{
    wxArrayInt delete_rows = onwork_group->GetSelectedRows();
    int i = delete_rows.GetCount() - 1;
    wxArrayString cell_id_dep_block;
    cell_id_dep_block.Add(wxT(" "));
    wxString id_wgroup = onwork_group->GetCellValue(delete_rows[0], 1);
    int check_copy = cell_id_dep_block.GetCount() - 1;
    while(i >= 0)
    {
        bool isorigin = true;

        while(check_copy >= 0)
        {
            id_wgroup = onwork_group->GetCellValue(delete_rows[i], 1);
            if(id_wgroup.Cmp(cell_id_dep_block[check_copy]) == 0)
            {
                isorigin = false;
            }
            check_copy--;
        }
        //Проверяю значение в столбце Конец Работ
        //Если работы уже закончины, то нельзя удалять информацию
        if(isorigin)
        {
            if(onwork_group->GetCellValue(delete_rows[i], 5).IsNull())
            {
                cell_id_dep_block.Add(id_wgroup);
                check_copy = cell_id_dep_block.GetCount() - 1;
                wxString id_contract_project = onwork_group->GetCellValue(delete_rows[i], 2);
                wxString iscontract = onwork_group->GetCellValue(delete_rows[i], 9);
                if(!OnDeleteWGroupMainPart(id_contract_project, iscontract, id_wgroup))
                {
                    delete_rows.Clear();
                    return;
                }
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Можно удалять только незаконченные проекты"), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
                delete_rows.Clear();
                return;
            }
        }
        
        i--;
    }
    delete_rows.Clear();

    wxGridCellCoordsArray delete_cells = onwork_group->GetSelectedCells();
    i = delete_cells.Count() - 1;
    while(i >= 0) 
    {
	    int row = delete_cells[i].GetRow();

        bool isorigin = true;

        while(check_copy >= 0)
        {
            id_wgroup = onwork_group->GetCellValue(row, 1);
            if(id_wgroup.Cmp(cell_id_dep_block[check_copy]) == 0)
            {
                isorigin = false;
            }
            check_copy--;
        }

        if(isorigin)
        {
            if(onwork_group->GetCellValue(row, 4).IsNull())
            {
                cell_id_dep_block.Add(id_wgroup);
                check_copy = cell_id_dep_block.GetCount() - 1;
                wxString id_contract_project = onwork_group->GetCellValue(row, 2);
                wxString iscontract = onwork_group->GetCellValue(row, 9);
                if(!OnDeleteWGroupMainPart(id_contract_project, iscontract, id_wgroup))
                {
                    delete_cells.Clear();
                    return;
                }
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Можно удалять только незаконченные проекты"), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
                return;
            }
        }
        i--;
	}

    delete_cells.Clear();
    OnUpdate(3, wxT("sql/work_group/work_group.sql"));
}

bool Personnal::OnDeleteWGroupMainPart(wxString id_contract_project, wxString iscontract, wxString id_wgroup)
{
    PGresult* res_eq;
    wxTextFile sql_file(wxT("sql/work_group/delete/delete_geteq.sql"));
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql_eq_string = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql_eq_string += wxT("\n");
        sql_eq_string += sql_file.GetNextLine();
    }
    sql_file.Close();

    wxString select_sql = wxT(" ");
    int pos = sql_eq_string.Find(wxT("WHERE"));
    while(pos != wxString::npos)
    {
        wxString s1;
        wxString s2;
        wxString s3;
        if (pos != wxString::npos)
        {
            s1 = sql_eq_string.SubString(0, pos-1);
            int len_s = sql_eq_string.Len() - s1.Len();
            s2 = sql_eq_string.Right(len_s);
        }

        pos = s2.Find(wxT("="));
        if (pos != wxString::npos)
        {
            s3 = s2.SubString(0, pos);
            int len_s = s2.Len() - s3.Len();
            sql_eq_string = s2.Right(len_s);
            s2 = s3;
        }

        select_sql += s1 + s2 + wxT("'") + id_wgroup + wxT("'");
        pos = sql_eq_string.Find(wxT("WHERE"));
    }

    select_sql += sql_eq_string + wxT("\0");

    const char* sql_eq = select_sql.mb_str(wxConvUTF8);
    res_eq = bd->request(sql_eq);
    if(!(ErrorRequest(res_eq, true)))
    {
        PQclear(res_eq);
        return false;
    }

    if(iscontract.Cmp(wxT("t")) == 0)
    {
        //получаю idДоговорПроект
        wxString string_name_file = wxT("sql/work_group/delete/get_id_contract_project.sql");
        wxArrayString id_contract_project_wgroup;
        if(!GetWhereSQL(string_name_file, &id_contract_project_wgroup, id_wgroup, bd, true))
        {
            ErrorSQL(wxT("Попробуйте еще раз позже"));
            return false;
        }

        string_name_file = wxT("sql/work_group/delete/get_id_projects.sql");
        wxArrayString uuid_projects;
        if(!GetWhereSQL(string_name_file, &uuid_projects, id_contract_project_wgroup[0], bd, true))
        {
            ErrorSQL(wxT("Попробуйте еще раз позже"));
            return false;
        }

        PGresult* res;

        wxTextFile sql_file(wxT("sql/work_group/delete/delete_contract_getpr.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += wxT("\n");
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();

        select_sql = wxT(" ");
        pos = sql.Find(wxT("WHERE"));
        while(pos != wxString::npos)
        {
            wxString s1;
            wxString s2;
            wxString s3;
            if (pos != wxString::npos)
            {
                s1 = sql.SubString(0, pos-1);
                int len_s = sql.Len() - s1.Len();
                s2 = sql.Right(len_s);
            }

            pos = s2.Find(wxT("="));
            if (pos != wxString::npos)
            {
                s3 = s2.SubString(0, pos);
                int len_s = s2.Len() - s3.Len();
                sql = s2.Right(len_s);
                s2 = s3;
            }

            select_sql += s1 + s2 + wxT("'") + id_contract_project + wxT("'");
            pos = sql.Find(wxT("WHERE"));
        }

        select_sql += sql + wxT("\0");


        const char* ch_sql = select_sql.mb_str(wxConvUTF8);

        res = bd->request(ch_sql);

        if(!(ErrorRequest(res, true)))
        {
            PQclear(res);
            PQclear(res_eq);
            return false;
        } 

        //Начинаю Транзакцию
        res = bd->request("BEGIN;");

        string_name_file = wxT("sql/work_group/delete/update_only_contract.sql");

        if(!UpdateSQL(string_name_file, wxT("false"), id_contract_project_wgroup[0], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return false;
        }

        string_name_file = wxT("sql/work_group/delete/update_all_projects.sql");

        if(!UpdateSQL(string_name_file, wxT("false"), id_contract_project_wgroup[0], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return false;
        }

        string_name_file = wxT("sql/work_group/delete/update_start_time_project.sql");

        for(int i = 0; i < uuid_projects.GetCount(); i++)
        {
            if(!GetWhereSQL(string_name_file, &id_contract_project_wgroup, uuid_projects[i], bd, false))
            {
                ErrorSQL(wxT("Попробуйте снова позже"));
                return false;
            }
        }

        if(!(ErrorRequest(res, false)))
        {
            res = bd->request("ROLLBACK;");
            PQclear(res);
            PQclear(res_eq);
            return false;
        } 

        // for (int j = 0; j < PQntuples(res); j++)
        // {
        //     if(PQgetvalue(res, j, 1) == NULL || PQgetvalue(res, j, 1) == "\0")
        //     {
        //         const char* id_project = PQgetvalue(res, j, 0);
        //         wxString id_project_string(id_project, wxConvUTF8);
        //         if( !(ResultRequest(wxT("sql/work_group/delete/delete_contract_project.sql"), id_project_string, res, res_eq)) )
        //         {
        //             return false;
        //         }
        //     }
        // }

        for (int j = 0; j < PQntuples(res_eq); j++)
        {
            const char* id_eq = PQgetvalue(res_eq, j, 0);
            wxString id_eq_string(id_eq, wxConvUTF8);

            if(PQgetvalue(res_eq, j, 1) == NULL || PQgetvalue(res_eq, j, 1) == "\0")
            {
                if( !(ResultRequest(wxT("sql/work_group/delete/delete_eq.sql"), id_eq_string, res, res_eq)) )
                {
                    return false;
                }

                wxString name_file_sql = wxT("sql/work_group/delete/delete_equip.sql");
                if(!DeleteEquipWGroup(name_file_sql, id_eq_string, id_wgroup, bd))
                {
                    ErrorSQL(wxT("Попробуйте позже"));
                    return false;
                }
            }
            // else
            // {
            //     if( !(ResultRequest(wxT("sql/work_group/delete/delete_eq_work.sql"), id_eq_string, res, res_eq)) )
            //     {
            //         return false;
            //     }
            // }
            
        }

        if( !(ResultRequest(wxT("sql/work_group/delete/delete_contract.sql"), id_contract_project, res, res_eq)) )
        {
            return false;
        }

        // if( !(ResultRequest(wxT("sql/work_group/delete/delete_wgroup.sql"), id_wgroup, res, res_eq)) )
        // {
        //     return false;
        // }

    }
    else
    {
        wxString string_name_file = wxT("sql/work_group/delete/get_id_contract_project.sql");
        wxArrayString id_contract_project_wgroup;
        if(!GetWhereSQL(string_name_file, &id_contract_project_wgroup, id_wgroup, bd, true))
        {
            ErrorSQL(wxT("Попробуйте еще раз позже"));
            return false;
        }

        wxArrayString uuid_contract;
        string_name_file = wxT("sql/work_group/delete/get_id_contract.sql");
        if(!GetWhereSQL(string_name_file, &uuid_contract, id_contract_project_wgroup[0], bd, true))
        {
            ErrorSQL(wxT("Попробуйте еще раз позже"));
        }

        PGresult* res;
        res = bd->request("BEGIN;");
        if(!(ErrorRequest(res, false)))
        {
            res = bd->request("ROLLBACK;");
            PQclear(res);
            PQclear(res_eq);
            return false;
        } 

        string_name_file = wxT("sql/work_group/delete/update_only_contract_id_contract.sql");

        if(!UpdateSQL(string_name_file, wxT("false"), uuid_contract[0], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return false;
        }

        string_name_file = wxT("sql/work_group/delete/update_all_projects.sql");

        if(!UpdateSQL(string_name_file, wxT("false"), uuid_contract[0], bd))
        {
            ErrorSQL(wxT("Попробуйте снова позже"));
            return false;
        }

        for (int j = 0; j < PQntuples(res_eq); j++)
        {
            const char* id_eq = PQgetvalue(res_eq, j, 0);
            wxString id_eq_string(id_eq, wxConvUTF8);

            if(PQgetvalue(res_eq, j, 1) == NULL || PQgetvalue(res_eq, j, 1) == "\0")
            {
                if( !(ResultRequest(wxT("sql/work_group/delete/delete_eq.sql"), id_eq_string, res, res_eq)) )
                {
                    return false;
                }
                wxString name_file_sql = wxT("sql/work_group/delete/delete_equip.sql");
                if(!DeleteEquipWGroup(name_file_sql, id_eq_string, id_wgroup, bd))
                {
                    ErrorSQL(wxT("Попробуйте позже"));
                    return false;
                }
            }
            // else
            // {
            //     if( !(ResultRequest(wxT("sql/work_group/delete/delete_eq_work.sql"), id_eq_string, res, res_eq)) )
            //     {
            //         return false;
            //     }
            // }  
        }

        if( !(ResultRequest(wxT("sql/work_group/delete/delete_project.sql"), id_contract_project, res, res_eq)) )
        {
            return false;
        }

        // if( !(ResultRequest(wxT("sql/work_group/delete/delete_wgroup.sql"), id_wgroup, res, res_eq)) )
        // {
        //     return false;
        // }
    }

    //Заканчиваю транзакцию
    PGresult* resz = bd->request("COMMIT;");
    if(PQresultStatus(resz) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(resz), wxConvUTF8);
        ErrorSQL(str);
        resz = bd->request("ROLLBACK;");
        PQclear(resz);
    }
    PQclear(resz);
    return true;
}

void Personnal::ErrorConnectBD()
{
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        error_bd, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

bool Personnal::ErrorRequest(PGresult* res, bool isselect)
{
    if(isselect)
    {
        if(PQresultStatus(res) != PGRES_TUPLES_OK)
        {
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
                wxMessageDialog *dial = new wxMessageDialog(NULL,
            str, wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            return false;
        }
    }
    else
    {
        if(PQresultStatus(res) != PGRES_COMMAND_OK)
        {
            wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
                wxMessageDialog *dial = new wxMessageDialog(NULL,
            str, wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
            return false;
        }
    }
    return true;
}

bool Personnal::ResultRequest(wxString name_file, wxString id_column, PGresult* res, PGresult* res_eq)
{
    int result = OnDeleteSQL(name_file, id_column, false);
    if(result == -1)
    {
        wxMessageDialog *dial = new wxMessageDialog(NULL,
            wxT("Не удалось удалить запись"), wxT("Error"), wxOK | wxICON_ERROR);
        dial->ShowModal();
        res = bd->request("ROLLBACK;");
        PQclear(res_eq);
        PQclear(res);
        return false;
    }
    return true;
}

bool Personnal::DeleteEquipWGroup(wxString get_sql_file, wxString first_string, wxString second_string, BD* bd)
{
    PGresult* res;
    wxTextFile sql_file(get_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    wxString select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&select_sql, pos, &sql, first_string);
    }


    pos = select_sql.Find(wxT("AND"));
    wxString full_select_sql = wxT("");
    if(pos != wxString::npos)
    {
        SetTextInSQL(&full_select_sql, pos, &select_sql, second_string);
    }
    
    const char* ch_sql = full_select_sql.mb_str(wxConvUTF8);

    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK");
        PQclear(res);
        return false;
    }

    return true;
}

void Personnal::SetBool(bool is_read_only)
{
    is_read_only_personal = is_read_only;

    if(isonstaff)
    {
        onstaff->is_read_only = is_read_only;
    }

    if(isondepartments)
    {
        ondepartments->is_read_only = is_read_only;
    }

    if(isonwork_group)
    {
        onwork_group->is_read_only = is_read_only;
    }
}