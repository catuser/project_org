#include "Exel.h"
#include "id.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

#include <wx/datectrl.h>

class SubcontrOkAdd
{
public:
    SubcontrOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, std::vector<wxComboBox*> vector_combobox_subcontr_main, 
                std::vector<wxDatePickerCtrl*> vector_date_main, wxScrolledWindow* scrolled_main);
    bool CheckData();
    bool CheckReapeatName();
    bool FillingInsertVector(int how_filling, int index_i, int index_j, wxString name_project);

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;
    std::vector<wxDatePickerCtrl*> vector_date;
    wxScrolledWindow* scrolled;

    bool is_ok;
    wxArrayString result;

    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxComboBox*> vector_combobox_subcontr;
    std::vector<wxComboBox*> insert_subcontr;

    std::vector<wxString> vector_uuid;
    std::vector<wxArrayString> select_result;
};