#include "Contract.h"

Contract::Contract(wxPanel* panel_buttons, wxScrolledWindow* table, wxBoxSizer* table_sizer, 
        BD* bd_main, wxAuiNotebook* nb_main, wxString error_bd_main) :
            wxPanel(panel_buttons, wxID_ANY)
{
    isoncustomer = false;
    isoncontract_table = false;
    isonproject = false;
    isoncontract_project = false;
    isonsubcontr_org = false;
    isonc_p_efficiency = false;

    is_read_only_contract = true;

    table_buttons = table;
    table_buttons_sizer = table_sizer;
    bd = bd_main;
    nb = nb_main;
    error_bd = error_bd_main;
}

void Contract::ContractSetSizer()
{
    customer = new wxButton(table_buttons, window::id_buttons::ID_CUSTOMER, wxT("Заказчик"), wxDefaultPosition, wxSize(121, 33));
    contract_table = new wxButton(table_buttons, window::id_buttons::ID_CONTRACT_TABLE, wxT("Договор"), wxDefaultPosition, wxSize(121, 33));
    project = new wxButton(table_buttons, window::id_buttons::ID_PROJECT, wxT("Проект"), wxDefaultPosition, wxSize(121, 33));
    contract_project = new wxButton(table_buttons, window::id_buttons::ID_CONTRACT_PROJECT, wxT("ДогПроект"), wxDefaultPosition, wxSize(121, 33));
    subcontr_org = new wxButton(table_buttons, window::id_buttons::ID_SUBCONTR_ORG, wxT("Субпод Орг"), wxDefaultPosition, wxSize(121, 33));
    c_p_efficiency = new wxButton(table_buttons, window::id_buttons::ID_C_P_EFFICIENCY, wxT("Эфф Дог/Пр"), wxDefaultPosition, wxSize(121, 33));

    table_buttons_sizer->Add(customer, 0, wxLEFT, 4);
    table_buttons_sizer->Add(contract_table, 0, wxLEFT, 4);
    table_buttons_sizer->Add(project, 0, wxLEFT, 4);
    table_buttons_sizer->Add(contract_project, 0, wxLEFT, 4);
    table_buttons_sizer->Add(subcontr_org, 0, wxLEFT, 4);
    table_buttons_sizer->Add(c_p_efficiency, 0, wxLEFT, 4);

    table_buttons->SetSizer(table_buttons_sizer);
    table_buttons->Layout();

    int height = 200;
    table_buttons->SetScrollbars(0, 2, 0, height/2);
}

void Contract::OnCustomer(bool isupdate)
{
    if(!isoncustomer)
    {
        table_vector_customer.clear();
        filter_customer.clear();
        isoncustomer = true;

        wxTextFile sql_file(wxT("sql/customer/customer.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        oncustomer = new Exel(nb);
        oncustomer->is_read_only = is_read_only_contract;
        oncustomer->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(oncustomer, wxT("Заказчики"), true);
        nb->Layout();

        for(int i = 0; i < 4; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = oncustomer->GetColLabelValue(i);
            table_columns.iscolumn_show = oncustomer->IsColShown(i);
            table_columns.number_col = i;
            table_vector_customer.push_back(table_columns);
        }

        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_customer, filter_customer, 
            oncustomer, wxT("customer"), bd, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(oncustomer);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnContractTable(bool isupdate)
{
    if(!isoncontract_table)
    {
        table_vector_contract_table.clear();
        filter_contract_table.clear();
        isoncontract_table = true;

        wxTextFile sql_file(wxT("sql/contract/contract.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        oncontract_table = new Exel(nb);
        oncontract_table->is_read_only = is_read_only_contract;
        oncontract_table->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(oncontract_table, wxT("Договоры"), true);
        nb->Layout();

        for(int i = 0; i < 8; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = oncontract_table->GetColLabelValue(i);
            table_columns.iscolumn_show = oncontract_table->IsColShown(i);
            table_columns.number_col = i;
            table_vector_contract_table.push_back(table_columns);
        }

        NumberColumn filter_sql;
        filter_sql.name_sql_file = wxT("sql/contract/filter/filter_is_contract.sql");
        filter_sql.name_column = wxT("Только Договор");
        filter_sql.number_column = 1;
        filter_contract_table.push_back(filter_sql);

        filter_sql.name_sql_file = wxT("sql/contract/filter/filter_is_all_projects.sql");
        filter_sql.name_column = wxT("Все Проекты");
        filter_sql.number_column = 1;
        filter_contract_table.push_back(filter_sql);

        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_contract_table, filter_contract_table, 
            oncontract_table, wxT("contract_table"), bd, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(oncontract_table);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnProject(bool isupdate)
{
    if(!isonproject)
    {
        table_vector_project.clear();
        filter_project.clear();
        isonproject = true;

        wxTextFile sql_file(wxT("sql/project/project.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onproject = new Exel(nb);
        onproject->is_read_only = is_read_only_contract;
        onproject->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onproject, wxT("Проекты"), true);
        nb->Layout();

        for(int i = 0; i < 7; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onproject->GetColLabelValue(i);
            table_columns.iscolumn_show = onproject->IsColShown(i);
            table_columns.number_col = i;
            table_vector_project.push_back(table_columns);
        }

        NumberColumn filter_sql;
        filter_sql.name_sql_file = wxT("sql/project/filter/filter_suborg.sql");
        filter_sql.name_column = wxT("Субпод Орг");
        filter_sql.number_column = 0;
        filter_project.push_back(filter_sql);

        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_project, filter_project, 
            onproject, wxT("project"), bd, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onproject);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnContractProject(bool isupdate)
{
    if(!isoncontract_project)
    {
        table_vector_contract_project.clear();
        filter_contract_project.clear();
        isoncontract_project = true;

        wxTextFile sql_file(wxT("sql/contract-project/contract-project.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        oncontract_project = new Exel(nb);
        oncontract_project->is_read_only = is_read_only_contract;
        oncontract_project->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(oncontract_project, wxT("Договор-Проект"), true);
        nb->Layout();

        for(int i = 0; i < 4; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = oncontract_project->GetColLabelValue(i);
            table_columns.iscolumn_show = oncontract_project->IsColShown(i);
            table_columns.number_col = i;
            table_vector_contract_project.push_back(table_columns);
        }

        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_contract_project, filter_contract_project, 
            oncontract_project, wxT("contract_project"), bd, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(oncontract_project);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnSubcontrOrg(bool isupdate)
{
    if(!isonsubcontr_org)
    {
        table_vector_subcontr_org.clear();
        filter_c_p_efficiency.clear();
        isonsubcontr_org = true;

        wxTextFile sql_file(wxT("sql/subcontr_org/subcontr_org.sql"));
        sql_file.Open();
        int count_lines = sql_file.GetLineCount();
        wxString sql = sql_file.GetFirstLine();
        while(!sql_file.Eof())
        {
            sql += sql_file.GetNextLine();
        }
        sql_file.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onsubcontr_org = new Exel(nb);
        onsubcontr_org->is_read_only = is_read_only_contract;
        onsubcontr_org->GetTable(bd, ch_sql, isupdate);
        nb->AddPage(onsubcontr_org, wxT("Субподрятные Организации"), true);
        nb->Layout();

        for(int i = 0; i < 10; i++)
        { 
            TableColumn table_columns;        
            table_columns.name_column = onsubcontr_org->GetColLabelValue(i);
            table_columns.iscolumn_show = onsubcontr_org->IsColShown(i);
            table_columns.number_col = i;
            table_vector_subcontr_org.push_back(table_columns);
        }

        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_subcontr_org, filter_subcontr_org, 
            onsubcontr_org, wxT("subcontr_org"), bd, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onsubcontr_org);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnCPEfficiency(bool isupdate)
{
    if(!isonc_p_efficiency)
    {
        table_vector_c_p_efficiency.clear();
        isonc_p_efficiency = true;

        wxTextFile sql_file1(wxT("sql/C-P_efficiency/contract.sql"));
        sql_file1.Open();
        int count_lines = sql_file1.GetLineCount();
        wxString sql = sql_file1.GetFirstLine();
        while(!sql_file1.Eof())
        {
            sql += sql_file1.GetNextLine();
        }
        sql_file1.Close();
        sql += wxT("\n");

        wxTextFile sql_file2(wxT("sql/C-P_efficiency/project.sql"));
        sql_file2.Open();
        count_lines = sql_file2.GetLineCount();
        sql += sql_file2.GetFirstLine();
        while(!sql_file2.Eof())
        {
            sql += sql_file2.GetNextLine();
        }
        sql_file2.Close();
        const char* ch_sql = sql.mb_str(wxConvUTF8);
        onc_p_efficiency = new Exel(nb);
        onc_p_efficiency->is_read_only = is_read_only_contract;
        onc_p_efficiency->GetTableCPEff(bd, ch_sql, isupdate);
        nb->AddPage(onc_p_efficiency, wxT("Эффективность Дог/Пр"), true);
        nb->Layout();

        TableColumn table_columns;        
        table_columns.name_column = wxT("Эффективность Договора");
        table_columns.iscolumn_show = true;
        table_columns.number_col = 0;
        table_vector_c_p_efficiency.push_back(table_columns);
       
        table_columns.name_column = wxT("Эффективность Проекта");
        table_columns.iscolumn_show = false;
        table_columns.number_col = 0;
        table_vector_c_p_efficiency.push_back(table_columns);


        bool isok = false;
        Filter* filter_contract = new Filter(table_vector_c_p_efficiency, 
            onc_p_efficiency, wxT("c_p_efficiency"), bd, 5, &isok);
        if(isok)
        {
            filter_contract->UseFilter(); 
            filter_contract->Destroy();
        }
        else
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
    else
    {
        int page_index = nb->GetPageIndex(onc_p_efficiency);
        if( page_index!=wxNOT_FOUND)
            nb->SetSelection(page_index);
    }
}

void Contract::OnUpdate(int number_exel, wxString file_name)
{
    wxTextFile sql_file(file_name);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql_string = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql_string += sql_file.GetNextLine();
    }
    sql_file.Close();
    const char* sql = sql_string.mb_str(wxConvUTF8);

    switch(number_exel)
    {
        case 1:
        {
            oncustomer->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_customer, filter_customer, oncustomer, wxT("customer"), bd, &isok);
            if(isok)
            {
                filter_contract->UseFilter(); 
                filter_contract->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 2:
        {
            oncontract_table->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_contract_table, filter_contract_table, 
                oncontract_table, wxT("contract_table"), bd, &isok);
            if(isok)
            {
                filter_contract->UseFilter(); 
                filter_contract->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 3:
        {
            onproject->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_project, filter_project, 
                onproject, wxT("project"), bd, &isok);
            if(isok)
            {
                filter_contract->UseFilter(); 
                filter_contract->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 4:
        {
            oncontract_project->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_contract_project, filter_contract_project, 
                oncontract_project, wxT("contract_project"), bd, &isok);
            if(isok)
            {
                filter_contract->UseFilter(); 
                filter_contract->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        case 5:
        {
            onsubcontr_org->OnUpdateTable(bd, sql);
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_subcontr_org, filter_subcontr_org, 
                onsubcontr_org, wxT("subcontr_org"), bd, &isok);
            if(isok)
            {
                filter_contract->UseFilter(); 
                filter_contract->Destroy();
            }
            else
            {
                wxMessageDialog *dial = new wxMessageDialog(NULL,
                    wxT("Не удалось использовать настройки фильтра."), wxT("Error"), wxOK | wxICON_ERROR);
                dial->ShowModal();
            }
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::OnUpdateTwo(int number_exel, wxString sql1, wxString sql2)
{
    wxTextFile sql_file1(sql1);
    sql_file1.Open();
    int count_lines = sql_file1.GetLineCount();
    wxString sql_string = sql_file1.GetFirstLine();
    while(!sql_file1.Eof())
    {
        sql_string += sql_file1.GetNextLine();
    }
    sql_file1.Close();
    sql_string += wxT("\n");

    wxTextFile sql_file2(sql2);
    sql_file2.Open();
    count_lines = sql_file2.GetLineCount();
    sql_string += sql_file2.GetFirstLine();
    while(!sql_file2.Eof())
    {
        sql_string += sql_file2.GetNextLine();
    }
    sql_file2.Close();
    const char* sql = sql_string.mb_str(wxConvUTF8);

    switch(number_exel)
    {
        case 6:
        {
            onc_p_efficiency->OnUpdateTableCPEff(bd, sql);
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::OnFilter(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_customer, filter_customer, oncustomer, wxT("customer"), bd, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        case 2:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_contract_table, filter_contract_table, 
                oncontract_table, wxT("contract_table"), bd, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        case 3:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_project, filter_project, 
                onproject, wxT("project"), bd, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        case 4:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_contract_project, filter_contract_project, 
                oncontract_project, wxT("contract_project"), bd, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        case 5:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_subcontr_org, filter_subcontr_org, 
                onsubcontr_org, wxT("subcontr_org"), bd, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        case 6:
        {
            bool isok = false;
            Filter* filter_contract = new Filter(table_vector_c_p_efficiency, 
                onc_p_efficiency, wxT("c_p_efficiency"), bd, 5, &isok);
            filter_contract->ShowModal();
            filter_contract->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::OnSearch(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            Search* search_contract = new Search(this, table_vector_customer, oncustomer);
            search_contract->Show(true);
            break;
        }
        case 2:
        {
            Search* search_contract = new Search(this, table_vector_contract_table, oncontract_table);
            search_contract->Show(true);
            break;
        }
        case 3:
        {
            Search* search_contract = new Search(this, table_vector_project, onproject);
            search_contract->Show(true);
            break;
        }
        case 4:
        {
            Search* search_contract = new Search(this, table_vector_contract_project, oncontract_project);
            search_contract->Show(true);
            break;
        }
        case 5:
        {
            Search* search_contract = new Search(this, table_vector_subcontr_org, onsubcontr_org);
            search_contract->Show(true);
            break;
        }
        case 6:
        {
            Search* search_contract = new Search(this, table_vector_c_p_efficiency, onc_p_efficiency);
            search_contract->Show(true);
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::OnAdd(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool is_ok = true;
            MyAdd* add_contract = new MyAdd(wxT("sql/customer/add/get_add.txt"), oncustomer, wxT("customer"), bd, &is_ok);
            if(is_ok)
            {
                add_contract->ShowModal();
                OnUpdate(1, wxT("sql/customer/customer.sql"));
            }
            add_contract->Destroy();
            break;
        }
        case 2:
        {
            bool is_ok = true;
            MyAdd* add_contract = new MyAdd(oncontract_table, bd, &is_ok, nb);
            if(is_ok)
            {
                add_contract->ShowModal();
                OnUpdate(2, wxT("sql/contract/contract.sql"));
            }
            add_contract->Destroy();
            break;
        }
        case 5:
        {
            bool is_ok = true;
            MyAdd* add_contract = new MyAdd(wxT("sql/subcontr_org/add/get_add.txt"), onsubcontr_org, wxT("subcontr_org"), bd, &is_ok);
            if(is_ok)
            {
                add_contract->ShowModal();
                OnUpdate(5, wxT("sql/subcontr_org/subcontr_org.sql"));
            }
            add_contract->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::OnChange(int number_exel)
{
    switch(number_exel)
    {
        case 1:
        {
            bool is_ok = true;
            Change* change_contract = new Change(wxT("sql/customer/change/get_change.txt"), oncustomer, wxT("customer"), bd, &is_ok);
            if(is_ok)
            {
                change_contract->ShowModal();
                OnUpdate(1, wxT("sql/customer/customer.sql"));
            }
            change_contract->Destroy();
            break;
        }
        case 3:
        {
            bool is_ok = true;
            Change* change_contract = new Change(wxT("sql/project/change/get_change.txt"), onproject, wxT("project"), bd, &is_ok);
            if(is_ok)
            {
                change_contract->ShowModal();
                OnUpdate(3, wxT("sql/project/project.sql"));
            }
            change_contract->Destroy();
            break;
        }
        case 5:
        {
            bool is_ok = true;
            Change* change_contract = new Change(wxT("sql/subcontr_org/change/get_change.txt"), onsubcontr_org, wxT("subcontr_org"), bd, &is_ok);
            if(is_ok)
            {
                change_contract->ShowModal();
                OnUpdate(5, wxT("sql/subcontr_org/subcontr_org.sql"));
            }
            change_contract->Destroy();
            break;
        }
        default:
        {
            wxMessageDialog *dial = new wxMessageDialog(NULL,
                wxT("Contract don't have this button"), wxT("Error"), wxOK | wxICON_ERROR);
            dial->ShowModal();
        }
    }
}

void Contract::ErrorConnectBD()
{
    wxMessageDialog *dial = new wxMessageDialog(NULL,
        error_bd, wxT("Error"), wxOK | wxICON_ERROR);
    dial->ShowModal();
}

void Contract::SetBool(bool is_read_only)
{
    is_read_only_contract = is_read_only;

    if(isoncustomer)
    {
        oncustomer->is_read_only = is_read_only;
    }

    if(isoncontract_table)
    {
        oncontract_table->is_read_only = is_read_only;
    }

    if(isonproject)
    {
        onproject->is_read_only = is_read_only;
    }

    if(isoncontract_project)
    {
        oncontract_project->is_read_only = is_read_only;
    }

    if(isonsubcontr_org)
    {
        onsubcontr_org->is_read_only = is_read_only;
    }

    if(isonc_p_efficiency)
    {
        onc_p_efficiency->is_read_only = is_read_only;
    }
}