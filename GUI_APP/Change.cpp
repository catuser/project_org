#include "Change.h"

Change::Change(wxString get_change_main, Exel* change_table_main, wxString name_table_main, BD* bd_main, bool* is_ok)
    : wxDialog(NULL, -1, wxT("Изменение таблицы"), wxDefaultPosition, wxSize(1280, 250))
{
    get_change = get_change_main;
    change_table = change_table_main;
    name_table = name_table_main;
    bd = bd_main;

    *is_ok = CheckIsSelected();

    GetFillingVector();

    for(int i = 0 ; i < vector_change.size(); i++)
    {
        if(vector_change[i].get_sql_file.Cmp(wxT("0")) != 0)
        {
            wxArrayString result;
            if(GetSelectFromSQL(&result, vector_change[i].get_sql_file))
            {
                combobox_result.push_back(result);
            }
            else
            {
                ErrorSQL(wxT("Попробуйте снова немного позже."));
                *is_ok = false;
                return;
            }
            
        }
    }

    vsizer = new wxBoxSizer(wxVERTICAL);

    scrolled = new wxScrolledWindow(this); 

    sizer_name_cols = new wxBoxSizer(wxHORIZONTAL);

    grid_sizer = new wxGridSizer(vector_change.size(), 5, 10);

    AddNameCols();


    wxButton* button_ok = new wxButton(this, window::id_buttons::ID_OK, wxT("OK"));
    wxButton* button_cancel = new wxButton(this, wxID_CANCEL, wxT("Отмена"));

    sizer_buttons = new wxBoxSizer(wxHORIZONTAL);

    sizer_buttons->Add(button_cancel, 0, wxLEFT | wxBOTTOM | wxDOWN, 5);
    sizer_buttons->Add(new wxStaticText(this, wxID_ANY, wxT(" ")), 1);
    sizer_buttons->Add(button_ok, 0, wxRIGHT | wxBOTTOM | wxDOWN, 5);

    if(!FillingDialog())
    {
        *is_ok = false;
        return;
    }
    vsizer->Add(scrolled, 1, wxEXPAND);
    vsizer->Add(sizer_buttons, 0, wxEXPAND | wxTOP, 5);
    SetSizer(vsizer);

    int x_size = vector_change.size();
    int y_size = vector_combobox.size() / x_size;
    bool issize = false;
    if(x_size <= 4)
    {
        x_size = x_size * 320;
        issize = true;
        this->SetClientSize(x_size, 250);
    }
    if(y_size <= 5)
    {
        if(issize)
            this->SetClientSize(x_size, 100 + y_size * 30);
        else
            this->SetClientSize(1280, 100 + y_size * 30);
    }

    Connect(window::id_buttons::ID_OK, wxEVT_COMMAND_BUTTON_CLICKED,
        wxCommandEventHandler(Change::OnOk));
}

bool Change::CheckIsSelected()
{
    wxArrayInt changes_rows = change_table->GetSelectedRows();
    int rows = changes_rows.GetCount() - 1;
    wxGridCellCoordsArray change_cells = change_table->GetSelectedCells();
    int cells = change_cells.Count() - 1;
    if(rows == -1 && cells == -1)
    {
        ErrorSQL(wxT("Не выбраны строчки для изменения."));
        changes_rows.Clear();
        change_cells.Clear();
        return false;
    }

    changes_rows.Clear();
    change_cells.Clear();
    return true;
}

void Change::GetFillingVector()
{
    wxTextFile get_change_file(get_change);
    get_change_file.Open();
    wxString line_file = get_change_file.GetFirstLine();
    set_change = line_file;
    line_file = get_change_file.GetNextLine();
    while(!get_change_file.Eof())
    {
        wxString part_of_line = line_file.Left(1);
        ChangeInfo info_change;
        info_change.number_col = wxAtoi(part_of_line);

        part_of_line = line_file.SubString(1, 1);
        if(part_of_line.Cmp(wxT("t")) == 0)
        {
            info_change.ischange_arbitrarily = true;
        }
        else
        {
            info_change.ischange_arbitrarily = false;
        }

        int size_part_of_line = line_file.Len() - 3;
        part_of_line = line_file.Right(size_part_of_line);
        info_change.get_sql_file = part_of_line;

        vector_change.push_back(info_change);

        line_file = get_change_file.GetNextLine();
    }
    get_change_file.Close();
}

bool Change::GetSelectFromSQL(wxArrayString* result, wxString name_sql_file)
{
    PGresult* res;
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();
    
    const char* ch_sql = sql.mb_str(wxConvUTF8);
    res = bd->request(ch_sql);

    if(PQresultStatus(res) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        PQclear(res);
        return false;
    }

    for (int i = 0; i < PQntuples(res); i++)
    {
        wxString res_col_string(PQgetvalue(res, i, 0), wxConvUTF8);
        result->Add(res_col_string);
    }

    PQclear(res);
    return true;
}

void Change::AddNameCols()
{
    for(int i = 0; i < vector_change.size(); i++)
    {
        wxString name_col = change_table->GetColLabelValue(vector_change[i].number_col);
        grid_sizer->Add(new wxStaticText(scrolled, wxID_ANY, name_col), 0, wxALIGN_CENTER | wxALL);
    }
}

bool Change::FillingDialog()
{
    if(name_table.Cmp(wxT("staff")) == 0)
    {
        if( !GetSelectFromSQL(&read_result, wxT("sql/staff/change/get/get_boss.sql")) )
        {
            ErrorSQL(wxT("Попробуйте снова немного позже."));
            return false;
        }
        
    }
    wxArrayInt changes_rows = change_table->GetSelectedRows();
    int rows = changes_rows.GetCount() - 1;
    int number_count_rows = 0;
    int number_real_count_rows = 0;
    wxGridCellCoordsArray change_cells = change_table->GetSelectedCells();
    int cells = change_cells.Count() + rows;
    int number_count_cells = rows + 1;
    int number_cells = rows + 1;
    int number_real_count_cells = 0;
    int check_row = -1;
    
    while(number_count_rows <= rows)
    {
        int number_row = changes_rows[number_real_count_rows];
        number_rows.Add(number_row);
        FillingDialogMainPart(&rows, number_count_rows, &number_real_count_rows, number_row);

        number_count_rows++;
        number_real_count_rows++;
    }

    while(number_count_cells <= cells)
    {
        int number_row = change_cells[number_real_count_cells].GetRow();
        if(check_row != number_row)
        {
            check_row = number_row;
            number_rows.Add(number_row);
            FillingDialogMainPart(&cells, number_cells, &number_real_count_cells, number_row);
            number_cells++;
        }
        number_count_cells++;
        number_real_count_cells++;    
    }

    scrolled->SetSizer(grid_sizer);
    scrolled->FitInside();
    scrolled->SetScrollRate(10, 10);  

    changes_rows.Clear();
    change_cells.Clear();

    return true;
}

void Change::FillingDialogMainPart(int* rows, int number_count_rows, int* number_real_count_rows, int number_row)
{
    if(name_table.Cmp(wxT("default")) == 0)
        DefaultFillingLine(number_count_rows, number_row);
    if(name_table.Cmp(wxT("departments")) == 0)
        DefaultFillingLine(number_count_rows, number_row);
    if(name_table.Cmp(wxT("staff")) == 0)
    {
        if(change_table->GetCellValue(number_row, 6).Cmp(wxT("f")) == 0)
        {
            ErrorSQL(wxT("Изменять информацию у уволенных сотрудников\nЗАПРЕЩЕНО"));
            *rows--;
            *number_real_count_rows++;
            return;
        }
        StaffFillingLine(number_count_rows, number_row);
    }
    if(name_table.Cmp(wxT("equipment_table")) == 0)
    {
        if( change_table->GetCellValue(number_row, 2).Cmp(wxT("f")) == 0 &&
            change_table->GetCellValue(number_row, 3).IsNull() )
        {
            ErrorSQL(wxT("Информацию об отсутствующем оборудовании нельзя менять"));
            *rows--;
            *number_real_count_rows++;
            return;
        }
        DefaultFillingLine(number_count_rows, number_row);
    }
    if(name_table.Cmp(wxT("customer")) == 0)
        ContractFillingLine(number_count_rows, number_row, 3);

    if(name_table.Cmp(wxT("project")) == 0)
    {
        DeleteCurrencySymbol(number_row, 4);
        ContractFillingLine(number_count_rows, number_row, 0);
    }
    
    if(name_table.Cmp(wxT("subcontr_org")) == 0)
    {
        DeleteCurrencySymbol(number_row, 9);
        DefaultFillingLine(number_count_rows, number_row);
    }
}

void Change::DefaultFillingLine(int number_count_rows, int number_row)
{
    int number_combobox_result = 0;
    int number_vector_combobox = number_count_rows * vector_change.size();
    for(int line_number_col = 0; line_number_col < vector_change.size(); line_number_col++)
    {
        wxString cell_value = change_table->GetCellValue(number_row, vector_change[line_number_col].number_col);
        FillingLineAdd(cell_value, line_number_col, number_vector_combobox + line_number_col, &number_combobox_result);
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }
}

void Change::StaffFillingLine(int number_count_rows, int number_row)
{
    int number_combobox_result = 0;
    int number_vector_combobox = number_count_rows * vector_change.size();
    for(int line_number_col = 0; line_number_col < vector_change.size(); line_number_col++)
    {
        wxString cell_value = change_table->GetCellValue(number_row, vector_change[line_number_col].number_col);
        if(line_number_col != 1)
        {
            FillingLineAdd(cell_value, line_number_col, number_vector_combobox + line_number_col, &number_combobox_result);
        }
        else
        {
            bool isboss = false;
            wxString check_value = change_table->GetCellValue(number_row, 0);
            for(int number_read_result = 0; number_read_result < read_result.GetCount(); number_read_result++)
            {
                if(check_value.Cmp(read_result[number_read_result]) == 0)
                {
                    isboss = true;
                }
            }
            if (isboss)
            {
                vector_combobox.push_back( new wxComboBox(scrolled, wxID_ANY, cell_value, wxDefaultPosition, wxSize(300, -1),
                    0, NULL, wxCB_READONLY) );
                vector_combobox[number_vector_combobox + line_number_col]->Append(cell_value);
                vector_combobox[number_vector_combobox + line_number_col]->SetValue(cell_value);
            }
            else
            {
                FillingLineAdd(cell_value, line_number_col, number_vector_combobox + line_number_col, &number_combobox_result);
            }
            
        }
        
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }
}

void Change::ContractFillingLine(int number_count_rows, int number_row, int dont_change)
{
    int number_combobox_result = 0;
    int number_vector_combobox = number_count_rows * vector_change.size();
    for(int line_number_col = 0; line_number_col < vector_change.size(); line_number_col++)
    {
        wxString cell_value = change_table->GetCellValue(number_row, vector_change[line_number_col].number_col);
        if(line_number_col == dont_change)
        {
            vector_combobox.push_back( new wxComboBox(scrolled, wxID_ANY, cell_value, wxDefaultPosition, wxSize(300, -1),
                0, NULL, wxCB_READONLY) );
            vector_combobox[number_vector_combobox + line_number_col]->Append(cell_value);
            vector_combobox[number_vector_combobox + line_number_col]->SetValue(cell_value);
            grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
            continue;
        }
        FillingLineAdd(cell_value, line_number_col, number_vector_combobox + line_number_col, &number_combobox_result);
        grid_sizer->Add(vector_combobox[number_vector_combobox + line_number_col], 0, wxALL | wxALIGN_CENTER);
    }
}

void Change::FillingLineAdd(wxString cell_value, int line_number_col, int index_vector, int* number_combobox_result)
{
    if(vector_change[line_number_col].get_sql_file.Cmp(wxT("0")) != 0)
    {
        if(change_table->is_read_only)
        {
            vector_combobox.push_back( new wxComboBox(scrolled, wxID_ANY, cell_value, wxDefaultPosition, wxSize(300, -1), 
                    0, NULL, 0 | wxCB_READONLY) );
        }
        else
        {
            vector_combobox.push_back( new wxComboBox(scrolled, wxID_ANY, cell_value, wxDefaultPosition, wxSize(300, -1)) );
        }
        
        vector_combobox[index_vector]->Append(combobox_result[*number_combobox_result]);
        vector_combobox[index_vector]->AutoComplete(combobox_result[*number_combobox_result]);
        vector_combobox[index_vector]->SetValue(cell_value);
        *number_combobox_result++;
    }
    else
    {
        vector_combobox.push_back( new wxComboBox(scrolled, wxID_ANY, cell_value, wxDefaultPosition, wxSize(300, -1)) );
    }
    
}

void Change::DeleteCurrencySymbol(int number_row, int number_col)
{
    wxString get_name = change_table->GetCellValue(number_row, number_col);
    int without_symbol = get_name.Len() - 2;
    wxString new_name = get_name.Left(without_symbol);
    change_table->SetCellValue(number_row, number_col, new_name);
}

void Change::OnOk(wxCommandEvent &event)
{
    SetFillingVector();

    int i = 0;
    int size_vector_combobox = vector_combobox.size();
    int size_vector_change = vector_change.size();
    int score_number_row = 0;
    bool isequipment = false;

    if(name_table.Cmp(wxT("departments")) == 0)
    {
        if(!DepartmentOk())
            return;
        Close();
        return;
    }

    if(name_table.Cmp(wxT("equipment_table")) == 0)
        isequipment = true;
    
    
    while(i < size_vector_combobox)
    {
        int number_combobox_result = 0;
        for(int j = 0; j < size_vector_change; j++)
        {
            if(!vector_change[j].ischange_arbitrarily)
            {
                if(!CheckInput(i, number_combobox_result, combobox_result, vector_combobox))
                {
                    ErrorSQL(wxT("Неправильный ввод данных"));
                    vector_combobox[i]->SetSelection(-1, -1);
                    int y_scroll = ((i / size_vector_change) * 3) + 2;
                    scrolled->Scroll(0, y_scroll);
                    return;
                }
                number_combobox_result++;
            }

            wxString where_string = change_table->GetCellValue(number_rows[score_number_row], vector_change[j].number_col);
            wxString new_result = vector_combobox[i]->GetValue();
            if(isequipment && j == 1)
            {
                if(!EquipmentSQL(new_result, where_string))
                    return;
                i++;
                continue;
            }
            if(!SetSQL(vector_change[j].set_sql_file, new_result, where_string))
            {
                ErrorSQL(wxT("Не удалось сохранить изменения"));
                return;
            }

            i++;
        }

        score_number_row++;
    }
    Close();
}

void Change::SetFillingVector()
{
    wxTextFile set_change_file(set_change);
    set_change_file.Open();
    wxString line_file = set_change_file.GetFirstLine();
    int number_vector = 0;
    while(!set_change_file.Eof())
    {
        wxString part_of_line = line_file.Left(1);
        vector_change[number_vector].number_col = wxAtoi(part_of_line);

        int size_part_of_line = line_file.Len() - 2;
        part_of_line = line_file.Right(size_part_of_line);
        vector_change[number_vector].set_sql_file = part_of_line;

        number_vector++;
        line_file = set_change_file.GetNextLine();
    }
    set_change_file.Close();
}

bool Change::SetSQL(wxString name_sql_file, wxString new_result, wxString where_string)
{
    PGresult* res_sql;
    wxString select_sql = wxT(" ");
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("SET"));
    if(pos != wxString::npos)
    {
        SetTextSQL(&select_sql, pos, &sql, new_result);
    }

    select_sql += sql + wxT("\0");

    pos = select_sql.Find(wxT("WHERE"));
    wxString full_select_sql = wxT(" ");
    if(pos != wxString::npos)
    {
        SetTextSQL(&full_select_sql, pos, &select_sql, where_string);
    }
    full_select_sql += select_sql;
    
    const char* ch_sql = full_select_sql.mb_str(wxConvUTF8);

    res_sql = bd->request(ch_sql);

    if(PQresultStatus(res_sql) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res_sql), wxConvUTF8);
        ErrorSQL(str);
        res_sql = bd->request("ROLLBACK;");
        PQclear(res_sql);
        return false;
    }

    return true;
}

void Change::SetTextSQL(wxString* select_sql, int pos, wxString* sql, wxString input_text)
{
    wxString s1;
    wxString s2;
    wxString s3;
    if (pos != wxString::npos)
    {
        s1 = sql->SubString(0, pos-1);
        int len_s = sql->Len() - s1.Len();
        s2 = sql->Right(len_s);
    }

    pos = s2.Find(wxT("="));

    if (pos != wxString::npos)
    {
        s3 = s2.SubString(0, pos);
        int len_s = s2.Len() - s3.Len();
        *sql = s2.Right(len_s);
        s2 = s3;
    }

    *select_sql += s1 + s2 + wxT("'") + input_text + wxT("'");
}

bool Change::CheckChangeDepartment(int number_combobox, int score_number_row)
{
    wxString result_change = vector_combobox[number_combobox]->GetValue();

    wxArrayString result_name_dep;
    if(!GetSelectFromSQL(&result_name_dep, wxT("sql/departments/change/set/get_dep_name.sql")))
    {
        return false;
    }
    int number_reseult = result_name_dep.GetCount();
    for(int i = 0; i < number_reseult; i++)
    {   
        if(result_change.Cmp(result_name_dep[i]) == 0)
        {
            int int_old_dep_name = number_rows[score_number_row];
            wxString old_dep_name = change_table->GetCellValue(int_old_dep_name, 1);
            if(result_change.Cmp(old_dep_name) == 0)
                continue;
            return false;
        }
    }

    return true;    
}

bool Change::DepartmentOk()
{
    int size_vector_combobox = vector_combobox.size();
    wxArrayString id_old_boss;
    wxArrayString id_new_boss;
    wxString name_file_get_id_boss = wxT("sql/departments/change/set/get_id_new_boss.sql");

    //Помещаю в id_old_boss все изначальные uuid боссов, которые были выбранны для изменения
    int index_number_rows = 0;
    for(int i = 0; i < size_vector_combobox; i+=2)
    {
        wxString old_id_boss = change_table->GetCellValue(number_rows[index_number_rows], 2);
        id_old_boss.Add(old_id_boss);
        index_number_rows++;
    }

    //получаю все uuid боссов, которые сейчас введины столбце Начальник
    for(int i = 1; i < size_vector_combobox; i+=2)
    {
        wxString new_name_boss = vector_combobox[i]->GetValue();
        if(!GetWhereSQL(name_file_get_id_boss, &id_new_boss, new_name_boss, bd, true))
        {
            ErrorSQL(wxT("Повторите ошибку позже"));
            return true;
        }
    }

    //Добавляю в список начальников, которых выбрал для изменения
    index_number_rows = 0;
    wxArrayString add_in_vector = combobox_result[0];
    for(int i = 0; i < size_vector_combobox; i+=2)
    {
        wxString old_name_boss = change_table->GetCellValue(number_rows[index_number_rows], 3);
        add_in_vector.Add(old_name_boss);
        index_number_rows++;
    }
    combobox_result.pop_back();
    combobox_result.push_back(add_in_vector);

    //Проверяю, совпадает ли новое имя отдела с именами существующих отделов
    //Если название отдела не было изменено, то совпадение пропускается
    int score_number_row = 0;
    for(int i = 0; i < size_vector_combobox; i+=2)
    {
        if(!CheckChangeDepartment(i, score_number_row))
        {
            ErrorSQL(wxT("Неправильный ввод данных"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 2) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            return false;
        }
        score_number_row++;
    }

    //Проверяю, повтораются ли введенные названия отделов
    int count_lines = size_vector_combobox / 2;
    if(!CheckRepeatProject(count_lines, 0, 2, vector_combobox, scrolled))
    {
        ErrorSQL(wxT("Данное название отдела уже выбранно"));
        return false;
    }


    //Проверяю, данный человек числится ли в списке начальников
    for(int i = 1; i < size_vector_combobox; i+=2)
    {
        if(!CheckInput(i, 0, combobox_result, vector_combobox))
        {
            ErrorSQL(wxT("Неправильно введено имя начальника"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 2) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            return false;
        }
    }

    //Проверяю, не ввели ли одного и того же начальника в несколько строчек
    if(!CheckRepeatProject(count_lines, 1, 2, vector_combobox, scrolled))
    {
        ErrorSQL(wxT("Данный человек уже выбран"));
        return false;
    }

    //Начинаю транзакцию
    PGresult* res = bd->request("BEGIN;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }

    PQclear(res);

    wxString name_file = wxT("sql/departments/change/set/update_dep_in_staff.sql");
    wxString name_file_dep = wxT("sql/departments/change/set/update_name_department.sql");

    //У всех сотрудников одного отдела обновляю название отдела
    //+ обновляю название в таблице Отдел
    //И так в цикле для каждого изменяемого отдела 
    index_number_rows = 0;
    for(int i = 0; i < size_vector_combobox; i+=2)
    {
        wxString new_name_department = vector_combobox[i]->GetValue();
        wxString oldname_department = change_table->GetCellValue(number_rows[index_number_rows], 1);
        if(!UpdateSQL(name_file, new_name_department, oldname_department, bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }

        if(!UpdateSQL(name_file_dep, new_name_department, oldname_department, bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }
        index_number_rows++;
    }

    //Собираю список боссов, которых нужно поменять
    //Собираю список uuid отделов этих боссов
    int index_vector_combobox = 0;
    std::vector<wxString> vector_change_boss;
    wxArrayString change_id_dep;
    wxArrayString delete_id_boss;
    wxArrayString new_name_dep_in_staff;
    for(int i = 0; i < id_old_boss.GetCount(); i++)
    {
        if(id_old_boss[i].Cmp(id_new_boss[i]) != 0)
        {
            vector_change_boss.push_back(id_new_boss[i]);
            wxString string_change_id_dep = change_table->GetCellValue(number_rows[i], 0);
            change_id_dep.Add(string_change_id_dep);
            delete_id_boss.Add(id_old_boss[i]);
            wxString new_name_department = vector_combobox[index_vector_combobox]->GetValue();
            new_name_dep_in_staff.Add(new_name_department);
        }
        index_vector_combobox += 2;
    }

    int count_change_boss = vector_change_boss.size();
    
    //Вставляю uuid новых боссов в таблицу НачОтд
    if(count_change_boss > 0)
    {
        name_file = wxT("sql/departments/change/set/insert_idbos.sql");
        if(!InsertOnlyUUIDSQL(vector_change_boss, 1, name_file, bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }
    }

    //Обновляю uuid начальников, которых изменили в таблице Отдел
    name_file = wxT("sql/departments/change/set/update_id_boss_in_dep.sql");
    for(int i = 0; i < count_change_boss; i++)
    {
        if(!UpdateSQL(name_file, vector_change_boss[i], change_id_dep[i], bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }
    }

    //Удаляю из таблицы НачОтд uuid начальников, которых поменяли
    name_file = wxT("sql/departments/change/set/delete_id_in_idboss.sql");
    for(int i = 0; i < count_change_boss; i++)
    {
        if(!GetWhereSQL(name_file, &change_id_dep, delete_id_boss[i], bd, false))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }
    }

    //Обновляю название отдела у начальника в таблице Сотрудник
    name_file = wxT("sql/departments/change/set/update_dep_staff.sql");
    for(int i = 0; i < count_change_boss; i++)
    {
        if(!UpdateSQL(name_file, new_name_dep_in_staff[i], vector_change_boss[i], bd))
        {
            ErrorSQL(wxT("Повторите попытку позже"));
            return true;
        }
    }

    //Конец транзакции
    PGresult* res1 = bd->request("COMMIT;");
    if(PQresultStatus(res1) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res1), wxConvUTF8);
        ErrorSQL(str);
        res1 = bd->request("ROLLBACK;");
        PQclear(res1);
    }

    PQclear(res1);

    return true;
}

bool Change::GetDepartmentSQL(wxString name_sql_file, wxString* result, wxString where_string)
{
    PGresult* res_sql;
    wxString select_sql = wxT(" ");
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    int pos = sql.Find(wxT("WHERE"));
    while(pos != wxString::npos)
    {
        wxString s1;
        wxString s2;
        wxString s3;
        if (pos != wxString::npos)
        {
            s1 = sql.SubString(0, pos-1);
            int len_s = sql.Len() - s1.Len();
            s2 = sql.Right(len_s);
        }

        pos = s2.Find(wxT("="));
        if (pos != wxString::npos)
        {
            s3 = s2.SubString(0, pos);
            int len_s = s2.Len() - s3.Len();
            sql = s2.Right(len_s);
            s2 = s3;
        }

        select_sql += s1 + s2 + wxT("'") + where_string + wxT("'");
        pos = sql.Find(wxT("WHERE"));
    }

    select_sql += sql + wxT("\0");
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    res_sql = bd->request(ch_sql);

    if(PQresultStatus(res_sql) != PGRES_TUPLES_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res_sql), wxConvUTF8);
        ErrorSQL(str);
        PQclear(res_sql);
        return false;
    }

    wxString res_col_string(PQgetvalue(res_sql, 0, 0), wxConvUTF8);
    *result = res_col_string;
    return true;
}

bool Change::ChangeInsertSQL(wxString name_sql_file, wxString insert_string)
{
    PGresult* res_sql;
    wxString select_sql;
    wxTextFile sql_file(name_sql_file);
    sql_file.Open();
    int count_lines = sql_file.GetLineCount();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        sql += wxT("\n");
        sql += sql_file.GetNextLine();
    }
    sql_file.Close();

    select_sql = sql + wxT("'") + insert_string + wxT("');");
    
    const char* ch_sql = select_sql.mb_str(wxConvUTF8);

    res_sql = bd->request(ch_sql);

    if(PQresultStatus(res_sql) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res_sql), wxConvUTF8);
        ErrorSQL(str);
        res_sql = bd->request("ROLLBACK;");
        PQclear(res_sql);
        return false;
    }

    return true;
}

bool Change::EquipmentSQL(wxString new_result, wxString where_string)
{
    wxString id_department;
    if(!GetDepartmentSQL(wxT("sql/equipment_table/change/set/get_id_department.sql"), &id_department, new_result))
    {
        ErrorSQL(wxT("Не удалось сохранить изменения"));
        return false;
    }
    if(!SetSQL(vector_change[1].set_sql_file, id_department, where_string))
    {
        ErrorSQL(wxT("Не удалось сохранить изменения"));
        return false;
    }
    return true;
}