#include "DepartmentOkAdd.h"

DepartmentOkAdd::DepartmentOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
    std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled)
{
    set_sql = set_sql_main;
    table = add_table_main;
    bd = bd_main;
    vector_combobox = vector_combobox_main;
    is_ok = true;

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Попробуйте снова попозже"));
        return;
    }
    select_result.push_back(result);
    result.Clear();

    for(int i = 0; i < vector_combobox.size(); i+=2)
    {
        if(CheckInput(i, 0, select_result, vector_combobox))
        {
            ErrorSQL(wxT("Неправильный ввод"));
            vector_combobox[i]->SetSelection(-1, -1);
            int y_scroll = ((i / 2) * 3) + 2;
            scrolled->Scroll(0, y_scroll);
            is_ok = false;
            return;
        }
    }

    for(int i = 1; i < vector_combobox.size(); i+=2)
    {
        wxString combobox_fio = vector_combobox[i]->GetValue();
        if(!GetWhereSQL(set_sql[1], &result, combobox_fio, bd, true))
        {
            ErrorSQL(wxT("Попробуйте снова попозже"));
            is_ok = true;
            return;
        }
    }
    
    int j = 1;
    for(int i = 0; i < result.GetCount(); i++)
    {
        vector_uuid_boss.push_back(result[i]);
        vector_combobox[j]->SetValue(result[i]);
        j+= 2;
    }
    result.Clear();

    int number_uuid = vector_combobox.size() / 2;
    GetAndCheckUuid(table, 0, vector_uuid_dep, number_uuid);

    std::vector<int> insert_uuid;
    insert_uuid.push_back(0);

    if(!InsertOnlyUUIDSQL(vector_uuid_boss, 1, set_sql[2], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }

    if(!InsertSQL(vector_uuid_dep, insert_uuid, vector_combobox, 3, set_sql[3], bd) )
    {
        ErrorSQL(wxT("Повторите попытку попозже"));
        return;
    }

    j = 0;
    for(int i = 0; i < vector_uuid_boss.size(); i++)
    {
        wxString new_dep_name = vector_combobox[j]->GetValue();
        if(!UpdateSQL(set_sql[4], new_dep_name, vector_uuid_boss[i], bd) )
        {
            ErrorSQL(wxT("Повторите попытку попозже"));
            return;
        }
        j+= 2;
    }

    PGresult* res = bd->request("COMMIT;");
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        wxString str = wxString(PQresultErrorMessage(res), wxConvUTF8);
        ErrorSQL(str);
        res = bd->request("ROLLBACK;");
        PQclear(res);
    }

    if(!GetSQL(set_sql[0], &result, bd))
    {
        ErrorSQL(wxT("Ошибка обновления фильтра"));
        return;
    }

    UpdateFilter();
}

void DepartmentOkAdd::UpdateFilter()
{
    wxString name_table = wxT("staff");
    int len_name_table = name_table.Len();
    int what_table = 0;
    wxTextFile sql_file(wxT("sql/settings.txt"));
    sql_file.Open();
    wxString sql = sql_file.GetFirstLine();
    while(!sql_file.Eof())
    {
        wxString name_col_setting = sql.Left(len_name_table);
        if(name_table.Cmp(name_col_setting) == 0)
        {
            int count_cols = table->GetNumberCols();
            for(int i = 0; i < count_cols; i++)
            {
                sql = sql_file.GetNextLine();
            }

            int count_departments = result.GetCount();
            for(int i = 0; i < count_departments; i++)
            {
                sql = sql_file.GetNextLine();
                int size_name_dep = sql.Len() - 2;
                wxString name_dep = sql.Left(size_name_dep);
                if(name_dep.Cmp(result[i]) != 0)
                {
                    int number_line = sql_file.GetCurrentLine();
                    wxString insert_dep = result[i] + wxT(" t");
                    sql_file.InsertLine(insert_dep, number_line);
                    sql_file.GoToLine(number_line);
                }
            }
            if(what_table == 0)
            {
                name_table = wxT("departments");
                len_name_table = name_table.Len();
                what_table++;
            }
            else
            {
                name_table = wxT("equipment_table");
                len_name_table = name_table.Len();
            }
            
        }
        sql = sql_file.GetNextLine();
    }
    sql_file.Write();
    sql_file.Close();
    return;
}