#include "Exel.h"
#include "id.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class WGroupOkAdd
{
public:
    WGroupOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled_main);

    void CheckAndPlusWork(std::vector<wxComboBox*> &vector_wgroup, wxString start_work);
    bool CheckRepeatProjectWithContract(wxArrayString string_projects);
    bool AllProjectsOfContract();

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;
    wxScrolledWindow* scrolled;

    bool is_ok;
    wxArrayString result;
    wxArrayString change_all_contact;

    std::vector<wxComboBox*> vector_combobox;

    std::vector<wxString> vector_project;
    std::vector<wxString> vector_contract;

    std::vector<wxString> vector_all_uuid;
    std::vector<wxString> vector_uuid_wgroup;
    std::vector<wxString> vector_uuid_conpr;

    std::vector<wxArrayString> select_result;
    std::vector<wxArrayString> vector_projects_from_contract;
    std::vector<wxArrayString> vector_name_contract;
};