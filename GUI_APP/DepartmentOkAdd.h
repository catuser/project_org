#include "Exel.h"
#include "Myuuid.h"
#include "WorkWithSQL.h"
#include "WorkWithInput.h"

class DepartmentOkAdd
{
public:
    DepartmentOkAdd(std::vector<wxString> set_sql_main, Exel* add_table_main, BD* bd_main, 
        std::vector<wxComboBox*> vector_combobox_main, wxScrolledWindow* scrolled);
    void UpdateFilter();

    std::vector<wxString> set_sql;
    Exel* table;
    BD* bd;

    bool is_ok;
    wxArrayString result;

    std::vector<wxComboBox*> vector_combobox;
    std::vector<wxArrayString> select_result;
    std::vector<wxString> vector_uuid_boss;
    std::vector<wxString> vector_uuid_dep;
};