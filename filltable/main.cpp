#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <vector>
#include <fstream>
#include <libpq-fe.h>

using namespace std;

void request(PGconn* conn, const char* sql);
char* fill_char(ifstream &read_file);

static void exit_nicely(PGconn *conn)
{
    PQfinish(conn);
    exit(1);
}

int main()
{
    char * buffer_create;
    char * buffer_fill;
    ifstream is1 ("create_tables.txt", ifstream::binary);

    buffer_create = fill_char(is1);

    ifstream is2 ("fill_tables.txt", ifstream::binary);
    buffer_fill = fill_char(is2);


    PGconn* conn = PQconnectdb("user=postgres password=mysecretpassword host=localhost port=54325 dbname=project_org");
    if (PQstatus(conn) != CONNECTION_OK)
    {
        fprintf(stderr, "Connection to database failedd: %s",
                PQerrorMessage(conn));
        exit_nicely(conn);
    }
    else
        cout << "CONNECTION_OK" << endl;

    request(conn, buffer_create);
    request(conn, buffer_fill);

    PQfinish(conn);

    //Функции для исполнения команд

    delete[] buffer_create;
    delete[] buffer_fill;

    return 0;
}

void request(PGconn* conn, const char* sql)
{
    PGresult* res = NULL;
    res = PQexec(conn, sql);
    if(PQresultStatus(res) != PGRES_COMMAND_OK)
    {
        fprintf(stderr, "SET failed: %s", PQerrorMessage(conn));
        PQclear(res);
        exit_nicely(conn);
    }
    else
    {
        cout << "RES OK" << endl;
    }
    
    PQclear(res);
}

char* fill_char(ifstream &read_file)
{
    char* write_buffer;
    if (read_file) 
    {
        // get length of file:
        read_file.seekg (0, read_file.end);
        int length = read_file.tellg();
        read_file.seekg (0, read_file.beg);

        write_buffer = new char [length];

        std::cout << "start read file: create_tables.txt" << endl;
        // read data as a block:
        read_file.read (write_buffer,length);

        if (read_file)
          std::cout << "all characters read successfully." << endl;
        else
          std::cout << "error: only " << read_file.gcount() << " could be read" << endl;
        read_file.close();
    }
    return write_buffer;
}

//g++ -o connect main.cpp $(pkg-config --cflags --libs libpq)